﻿# Introduction 
ArchiTECH is an ST&I tool that aims to support technical managers for formula stability risk assessment. 
It’s also a learning tool that provides chemical understanding of stability phenomenon that may occur and enables expertise transmission.

# Some informations
I started working on this project since March 2020 with the help of Alexis Blanchet and Fallou MBAYE. 
I am actually working on the site of Neuilly sur Seine. During the period confinement, I was working at home.

# Progression
11 functions on 12 are working actually. 
The application is operational and can be used by the managers.
The code is indented and commented.
The app.py file is commented at 100%
The functions.py is not commented at 50%.
The assets files are not commented yet.

# Need to DO
Improove some functions, change some titles and the color of some results.
Simplify the code, I mean remove all the boucle "for" to be replace by one line of code (if it's possible).
Make a documentation of all the variable used in the program.

# Installation
To run the application, please install these packages in the Anaconda console :
```bash
pip install base64
pip install io
pip install json
pip install os
pip install dash
pip install pandas
pip install math
pip install numpy
pip install dash_core_components
pip install dash.dependencies
pip install dash_html_components
pip instal dash_table 
pip instal dash_daq 
pip instal dash.exceptions 
pip instal flask 
pip instal bs4 
pip instal decimal 
pip instal plotly.graph_objs 
pip instal datetime 
pip instal datetime 
pip instal time 
pip instal docx 
```

#How to launch ArchiTECH on the server 
1) Install winSCP on your computer.
2) Launch winSCP
3) Create new session with 
	- File protocol : SFTP
	- Host name : 10.35.19.16
	- User name : centos
Then click on Advanced / SSH / Authentification, and choose private key file as dev-architech.ppk file.
4) Login
5) Open session in PUTTY
6) write : "cd ../..", 
then write : "cd etc/nginx",
then write : "sudo service nginx restart"
then write : "cd ../..",
then write : "cd home/centos/ArchiTechApp",
the write : gunicorn --bind 0.0.0.0:8050 app:server --timeout 500 --daemon
7) Close the software winSCP and the Console
8) The application ArchiTECH is now working on the IFF server


# Presentation of the application (ArchiTECH)
The application takes as parameters a formula of raw materials and springs in output several results of stability of the perfume.
There are 12 types of stability analysis :
```bash
- Formula risk assessment
- Antioxidants origin
- Natural / Synthetic RMs decomposition
- Solvent decomposition
- Problematic RMs tracking
- P20/P80
- Comparator
- Data display
- RMs initial color and fundamentals
- Schiff Base
- Esters hydrolyses (not coded yet)
- Hydro Alcoholic solution formula
```

# Structuration of the code
There are 2 principal files and 1 folder to make the code run :
- app.py
- functions.py
- assets

In app.py is stored all the code that make the bond between the client and the program. It means all the parameter put in the application.
In functions.py is stored all the code in the background which deals with the analysis calculation of the functions.
In the folder assets is stored all the css files that make the design of the application.

# Functionnement of the application
First, you need to import a formula from your computer. The formula need to be in a specific format.
Then you can choose an analysis function of your choice, and the application will display the result based on the analysis function chosen.

