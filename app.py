###########################################################################
######################### Import PACKAGES #################################
###########################################################################
import base64
import io
import json
import os, sys, subprocess
import dash
import hashlib
import re
import uuid
import pandas as pd
import math
import numpy as np
import dash_core_components as dcc
from dash.dependencies import Input, Output, State, MATCH, ALL
import dash_html_components as html
import dash_table as dt
import dash_daq as daq
from dash.exceptions import PreventUpdate
from flask import Flask, send_from_directory,redirect, url_for,request,session,render_template,jsonify
from bs4 import BeautifulSoup
from decimal import Decimal
import plotly.graph_objs as go
from datetime import datetime
from datetime import date
from time import strftime
from docx import Document
from docx.shared import Inches
from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.shared import Pt
from docx.enum.text import WD_LINE_SPACING
from docx.enum.style import WD_STYLE_TYPE
from docx.shared import RGBColor

import requests
try:
    from layout_helper import run_standalone_app
except ModuleNotFoundError:
    from .layout_helper import run_standalone_app
from functions import *



###########################################################################
######################### DASH DEPLOYMENT #################################
###########################################################################
client_id = "0oa1j49m36mapcbn30h8"
okta_domain = "https://iff.okta.com"


secret_key_ = base64.urlsafe_b64encode(os.urandom(42)).decode('utf-8')
secret_key_ = re.sub('[^a-zA-Z0-9]+', '', secret_key_)

server = Flask(__name__)
server.secret_key = secret_key_

@server.route("/")
def home():
    print('going throw /')
    if 'id' in session:
        pass
    else:
        session['id'] = 'init'

    c = request.base_url
    #########################################################################
    #### ///// FOR DEPLOYEMENT ONLY - REROUTING TO SECURE https ADRESS \\\\\\ #####
    #########################################################################
    if ("10.35.19.16" in c):
       c = "https://architech-dev.iff.com/"
    elif ("127.0.0.1" in c):
       c = "https://architech-dev.iff.com/"
    #########################################################################
    nonce = uuid.uuid4().hex
    state = str(uuid.uuid4())
    r = okta_domain+"/oauth2/v1/authorize?client_id="+\
        client_id+"&response_type=token&scope=openid profile email&redirect_uri="+c+"login"+\
        "&state="+state+"&nonce="+nonce     #&code_challenge_method=S256&code_challenge="+code_challenge
    try:
        return redirect(r)
    except:
        return 'error'

@server.route('/login')
def index():
    print('going throw /login')
    # JS injection to get Fragments Client Side (Flask being server side)
    return('''  <script type="text/javascript">
                    try {
                    var token = window.location.href.split('access_token=')[1];
                    }
                    catch{error}{
                    var token = window.location.href.split('#')[1];
                    }
                    window.location = "/decode_answer/" + token;
                </script> ''')
    #return redirect(url_for('/dashapp/'))

@server.route('/decode_answer/<token>/', methods=['GET'])
def app_response_token(token):
    print('going throw /token')
    if('error' in token):
        return render_template("unauthorized.html")
    else:
        if 'id' in session:
            pass
        else:
            session['id'] = 'init'
        token = token.split('&')[0].split('=')[-1]
        #print(token)
        session['token'] = token
        headers = {'Authorization': 'Bearer '+token}
        m = okta_domain+"/oauth2/v1/userinfo"
        m = requests.get(m,headers=headers).json()

        session['name'] = m['name']
        session['email'] = m['email']
        #info list: 'sub','name','family_name','given_name','locale','preferred_username','zoneinfo','updated_at','email','email_verified'

        req1 = requests.get( "https://ion-qas.iff.com/api/v1/users?emailIds="+session['email']).json()
        session['globaluserid'] = req1['users'][0]['globaluserid']
        # with open('../logs/'+str(session['globaluserid'])+'_data.txt', 'w') as f:
        #     for key in ['name','email']:
        #         f.write(key+':'+str(m[key])+'\n')
        #     for key in ['globaluserid','deptname','position','jobfamily','divisionname','countrycode','countryname','employeecatcode','regioncode','regionname','orgunitname']:
        #         f.write(key+':'+str(req1['users'][0][key])+'\n')
        # print(session['globaluserid'])
        # Report('connexion of user: '+str(session['globaluserid']),user_id=str(session['globaluserid']))
        session['id'] = 'authorize'
        return redirect(url_for('/home/'))

@server.route('/home/')
def rerouting():
    print('going throw /home')
    if 'id' in session:
        if(session['id'] ==  'authorize'):
            return app.index()
    return redirect(request.url_root)

###########################################################################
######################### Import DATABASE #################################
###########################################################################
# testing formula (coming from phoenix if possible)
df = pd.read_excel('data/formule.xlsx')

# Ingredient DataBase - handled by ST&I
df_ing = pd.read_excel('data/dbing.xlsx')
df_ing['IPC'] = df_ing['IPC'].apply(make_ipc)
df_ing['Neat IPC'] = df_ing['Neat IPC'].apply(make_ipc)
df_ing['Name'] = df_ing['Name'].apply(lambda x: x.replace('"', ''))

# Database for anti oxydant
df_aox = pd.read_excel('data/AOX.xlsx')
df_aox['IPC'] = df_aox['IPC'].apply(make_ipc)
df_aox['Name'] = df_aox['Name'].apply(lambda x: x.replace('"', ''))

# Database for solvant natural or synthetic
df_solvent_nat_synt = pd.read_excel('data/indirectdbsol.xlsx')
df_solvent_nat_synt['IPC'] = df_solvent_nat_synt['IPC'].apply(make_ipc)
df_solvent_nat_synt['Name'] = df_solvent_nat_synt['Name'].apply(lambda x: x.replace('"', ''))

# Database for p20 p80 functions
df_p20_p80 = pd.read_excel('data/databasep20p80.xlsx')
df_p20_p80['IPC'] = df_p20_p80['IPC'].apply(make_ipc)
df_p20_p80['Name'] = df_p20_p80['Name'].apply(lambda x: x.replace('"', ''))

# Database for RMs coloration
df_RMs_color = pd.read_excel('data/dataRMsColor.xlsx')
df_RMs_color['IPC'] = df_RMs_color['IPC'].apply(make_ipc)
df_RMs_color['Name'] = df_RMs_color['Name'].apply(lambda x: x.replace('"', ''))

# Database for risk assessment
df_RMs_warning = pd.read_excel('data/datawarning.xlsx')
df_RMs_warning['IPC'] = df_RMs_warning['IPC'].apply(make_ipc)
df_RMs_warning['Name'] = df_RMs_warning['Name'].apply(lambda x: x.replace('"', ''))

# Database for natural explosion
df_explosion_nat = pd.read_excel('data/explosionNAT.xlsx')
df_explosion_nat['IPC'] = df_explosion_nat['IPC'].apply(make_ipc)
df_explosion_nat['Name'] = df_explosion_nat['NAME'].apply(lambda x: x.replace('"', ''))

# Database for synthetic explosion
df_explosion_synt = pd.read_excel('data/explosionSYNT.xlsx')
df_explosion_synt['IPC'] = df_explosion_synt['IPC'].apply(make_ipc)
df_explosion_synt['Name'] = df_explosion_synt['NAME'].apply(lambda x: x.replace('"', ''))

# Database for identification RMs tracking
df_indole = pd.read_excel('data/identification_indole.xlsx')
df_indole['IPC'] = df_indole['IPC'].apply(make_ipc)
df_indole['Name'] = df_indole['NAME'].apply(lambda x: x.replace('"', ''))

df_linalyl = pd.read_excel('data/identification_linalyl.xlsx')
df_linalyl['IPC'] = df_linalyl['IPC'].apply(make_ipc)
df_linalyl['Name'] = df_linalyl['NAME'].apply(lambda x: x.replace('"', ''))

df_methyl = pd.read_excel('data/identification_methyl.xlsx')
df_methyl['IPC'] = df_methyl['IPC'].apply(make_ipc)
df_methyl['Name'] = df_methyl['NAME'].apply(lambda x: x.replace('"', ''))

df_vanillin = pd.read_excel('data/identification_vanillin.xlsx')
df_vanillin['IPC'] = df_vanillin['IPC'].apply(make_ipc)
df_vanillin['Name'] = df_vanillin['NAME'].apply(lambda x: x.replace('"', ''))

# Database for identification of Methyl Anthranilate in direct or indirect
df_methyl_direct = pd.read_excel('data/identification_methyl_direct.xlsx')
df_methyl_direct['IPC'] = df_methyl_direct['IPC'].apply(make_ipc)
df_methyl_direct['Name'] = df_methyl_direct['NAME'].apply(lambda x: x.replace('"', ''))

df_methyl_indirect = pd.read_excel('data/identification_methyl_indirect.xlsx')
df_methyl_indirect['IPC'] = df_methyl_indirect['IPC'].apply(make_ipc)
df_methyl_indirect['Name'] = df_methyl_indirect['NAME'].apply(lambda x: x.replace('"', ''))

# Database for indirect solvant
list_solvent = pd.read_excel('data/indirectdbsol.xlsx')
df_indirect_solv = pd.read_excel('data/indirectdbsol.xlsx')
list_solvent['Valeur'] = list_solvent['Solvent'].apply(solvent_simple)
list_solvent = list_solvent[list_solvent.Valeur == True]
list_solvent = list_solvent['Solvent']

# Database for
df_BDS = pd.read_excel('data/dataBDS.xlsx')
df_BDS['IPC'] = df_BDS['IPC'].apply(make_ipc)
df_BDS['Name'] = df_BDS['NAME'].apply(lambda x: x.replace('"', ''))

df_BDS_natural = pd.read_excel('data/dataBDSnatural.xlsx')
df_BDS_natural['IPC ald'] = df_BDS_natural['IPC ald'].apply(make_ipc)
df_BDS_natural['NAME'] = df_BDS_natural['NAME'].apply(lambda x: x.replace('"', ''))

df_ester = pd.read_excel('data/dataEster.xlsx')
df_ester['IPC'] = df_ester['IPC'].apply(make_ipc)
df_ester['Name'] = df_ester['NAME'].apply(lambda x: x.replace('"', ''))

df_list_ester = pd.read_excel('data/dataListEster.xlsx')
df_list_ester['IPC'] = df_list_ester['IPC'].apply(make_ipc)
df_list_ester['Name'] = df_list_ester['NAME'].apply(lambda x: x.replace('"', ''))

df_ester_half_life = pd.read_excel('data/esterdataHalfLife.xlsx')
df_ester_half_life['IPC'] = df_ester_half_life['IPC'].apply(make_ipc)
df_ester_half_life['Name'] = df_ester_half_life['NAME'].apply(lambda x: x.replace('"', ''))

df_fullData = pd.read_excel('data/fullDatabase.xlsx')
df_fullData['IPC'] = df_fullData['IPC'].apply(make_ipc)
df_fullData['Name'] = df_fullData['Name'].apply(lambda x: x.replace('"', ''))

###########################################################################
############################### Classes ###################################
###########################################################################

class DashCallbackVariables:
    """Class to store information useful to callbacks"""

    def __init__(self):
        self.n_clicks = {1: 0, 2: 0}

    def update_n_clicks(self, nclicks, bt_num):
        self.n_clicks[bt_num] = nclicks


callbacks_vars = DashCallbackVariables()

###########################################################################
############################### Dicts #####################################
###########################################################################


rename_dict_for_data_upload = {
    'INGREDIENT': 'BOMItemName',
    'IPC': 'BOMItemNumber',
    'PARTS': 'Parts',
    "INGREDIENTS": 'BOMItemName',
    "INGREDIENT NAME": 'BOMItemName',
    "DOSAGES": 'Parts',
    "dosage":'Parts',

}

################### Preload a formula to avoid errors ####################

new_formula = df.drop([c for c in df if c is None], axis=1)
new_formula = new_formula.dropna(axis=1, how='all')
new_formula = new_formula.dropna()
new_formula = new_formula.rename(columns=rename_dict_for_data_upload)
if ('BOMItemNumber' in new_formula):
    new_formula = new_formula[['BOMItemNumber', 'Parts']]
else:
    if (len(new_formula.columns) == 2):
        new_formula.columns = ['IPC', 'dosage']
    elif (len(new_formula.columns) == 3):
        new_formula.columns = ['name', 'IPC', 'dosage']
        new_formula = new_formula.drop('name', axis=1)
    elif (len(new_formula.columns) == 4):
        new_formula.columns = ['name of formula', 'name', 'IPC', 'dosage']
        new_formula = new_formula.drop(['name of formula', 'name'], axis=1)
new_formula.columns = ['IPC', 'dosage']
new_formula['IPC'] = new_formula['IPC'].apply(make_ipc)
new_formula['dosage'] = new_formula['dosage'].apply(lambda x: str(x).replace(',', '.')).astype(float)
new_formula['dosage'] = new_formula.groupby(['IPC'])['dosage'].transform('sum')
new_formula['parts'] = new_formula['dosage'].copy()
new_formula['dosage'] = new_formula['dosage'] * 100 / sum(new_formula['dosage'])
new_formula = new_formula[new_formula['dosage'] > 0]

df = new_formula
df = df.set_index('IPC').join(df_ing.set_index('IPC'))
df['Name'] = df['Name'].fillna(df.index.to_series()).fillna('Not Found')

df_aox_data = new_formula
df_aox_data = df_aox_data.set_index('IPC').join(df_aox.set_index('IPC'))
df_aox_data['Name'] = df_aox_data['Name'].fillna(df_aox_data.index.to_series()).fillna('Not Found')
df_aox_data = df_aox_data.fillna("")
df_aox_data = df_aox_data.drop(['parts', 'Dilution', 'Solvant'], axis=1)
df_aox_data = df_aox_data.reset_index()
new_order = [2, 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
df_aox_data = df_aox_data[df_aox_data.columns[new_order]]
aox_columns = list(set(df_aox_data) - set(['Name', 'IPC', 'dosage']))
sr = df_aox_data['dosage'] / 100
df_aox_data[aox_columns] = df_aox_data[aox_columns].apply(pd.to_numeric, errors='coerce')
df_aox_data[aox_columns] = df_aox_data[aox_columns].mul(sr, fill_value=None, axis=0)
df_aox_data[aox_columns] = df_aox_data[aox_columns].replace(np.nan, "")
df_aox_data[aox_columns] = round(df_aox_data[aox_columns], 4)
df_aox_data['AOX'] = df_aox_data[aox_columns].apply(
    lambda x: ";".join([str(f) + ' : ' + str(round(x[f], 4)) for f in df_aox_data[aox_columns] if x[f] != ""]), axis=1)
df_aox_data['dosage'] = round(df_aox_data['dosage'], 4)

df['name of formula'] = 'default Formula'


###########################################################################
######################### BODY OF THE APPLICATION #########################
###########################################################################
def layout():
    """
    All the datas store in dcc.store :
    - current formula : test formula to test the functions
    - stash_upload_phoenix : formula from phoenix (not working)
    - name : name of formulas
    - stash_upload_computer : formulas loaded from Computer
    - formulas_name_comparator : formulas name loaded from Computer for function comparator
    - stash_upload_computer_comparator :  formulas loaded from computer for function comparator
    - aox_data / aox_MP / aox_direct : datas for function AOX
    - solvent_MP / solvent_direct : data for function solvent decomposition
    - df_data : formula data to process
    - df_data_comparator : formula datas to process for function comparator_risk_assessment
    - df_data_color / df_data_warn / df_data_identification : data for function risk assessment
    """
    return html.Div(
        [
            dcc.Store(id='current_formula', data=df.to_dict(), storage_type='local'),
            dcc.Store(id='stash_upload_phoenix', data={}),
            dcc.Store(id='missing_IPC', data={}),
            dcc.Store(id='name', data={}),
            dcc.Store(id='stash_upload_computer', data={}),
            dcc.Store(id='formulas_name_comparator', data={}),
            dcc.Store(id='stash_upload_computer_comparator', data={}),
            dcc.Store(id='aox_data', data={}),
            dcc.Store(id='aox_MP', data={}),
            dcc.Store(id='aox_direct', data={}),
            dcc.Store(id='solvent_MP', data={}),
            dcc.Store(id='solvent_direct', data={}),
            dcc.Store(id='df_data', data={}),
            dcc.Store(id='df_data_comparator', data={}),
            dcc.Store(id='df_data_color', data={}),
            dcc.Store(id='df_data_warn', data={}),
            dcc.Store(id='df_data_identification', data={}),
            html.Div(id='architect-body', className='app-body', children=[
                html.Div(id='architect-control-tabs', className='control-tabs', children=[
                    dcc.Tabs(id='architect-tabs', value='what-is', children=[
                        # about
                        dcc.Tab(
                            label='About',
                            value='what-is',
                            children=html.Div(className='control-tab', children=[
                                html.H4(className='what-is', children="What is ArchiTECH ?"),

                                html.Div(style={'textAlign': 'center'}, children=[html.Img(
                                    src='data:image/png;base64,{}'.format(
                                        base64.b64encode(
                                            open(
                                                './assets/plotly-dash-bio-logo.png', 'rb'
                                            ).read()
                                        ).decode()
                                    )
                                )]),
                                html.Br(),
                                html.P(
                                    'ArchiTECH is an ST&I tool that aims to support technical managers for formula stability risk assessment. It’s also a learning tool that provides chemical understanding of stability phenomenon that may occur and enables expertise transmission.'),
                                html.P(
                                    'NB: this tool does not exempt from the critical view of the expert, it only aims to help him in the risk assessment and to avoid repetitive tasks to save time.'),
                                html.P(
                                    'Databases used in this tool to provide stability risk assessment in fine fragrances are based on fifteen years of learnings acquired by Neuilly ST&I managers. It groups databases on ingredients physicochemical attributes, intrinsic color, additives, solvents but also on their stability both in neat oil and hydro-alcoholic solution. Databases and technical guidelines have been build and implemented on troubleshooting learnings and proactive studies.'),
                                html.P(
                                    'Many risk assessment functionalities are available is this tool as formula risk assessment for color, odor and aspect issues. The software highlights raw materials that can generate issues in the formula. Chemical explanations on the phenomenon are also given to the user.'),

                                html.Div([
                                    html.P(
                                        'For complete explanation on ArchiTECH and its functionalities or for any other question, please contact global ST&I super-user : '),
                                    html.A('Stéphanie WOIMANT', href='mailto:stephanie.woimant@iff.com')

                                ]),

                                html.Br()
                            ])
                        ),

                        # uplaod formula
                        dcc.Tab(
                            label='Upload Formula',
                            value='data',
                            children=html.Div(className='control-tab', children=[
                                html.Div(className='app-controls-block', children=[
                                    html.Div(className='app-controls-name', children='Data source'),
                                    dcc.Dropdown(
                                        id='architect-preloaded-uploaded',
                                        options=[
                                            {'label': 'Phoenix', 'value': 'phoenix'},
                                            {'label': 'Computer', 'value': 'computer'}
                                        ],
                                        value='computer'
                                    )
                                ]),
                                html.Hr(),
                                html.A(
                                    html.Button(
                                        id='architect-download-button',
                                        className='control-download',
                                        children="Download Formulas"
                                    ),
                                    href=os.path.join('assets', 'sample_data', 'sample_data.rar'),
                                    download="circos_sample_data.rar",
                                ),

                                html.Div(id='architect-uploaded-data', children=[
                                    dcc.Upload(
                                        id="upload-data",
                                        className='control-upload',
                                        children=html.Div(
                                            [
                                                "Drag and Drop or "
                                                "click to import "
                                                "the file here!"
                                            ]
                                        ),
                                        multiple=False,
                                    ),

                                    html.Hr(),
                                    html.Div(id='loaded', className='app-controls-name', style={'width': '15vw'})
                                ]),
                            ])

                        ),
                        # analyse type
                        dcc.Tab(
                            label='Analysis',
                            value='analysis',
                            children=html.Div(className='control-tab', children=[
                                html.Div(className='app-controls-block', children=[
                                    html.Div(className='app-controls-name', children='Analysis Type'),
                                    dcc.Dropdown(
                                        id='action-type',
                                        persistence=False,
                                        options=[
                                            {'label': 'Formula risk assessment', 'value': 'data1'},
                                            {'label': 'Antioxidants origin', 'value': 'data2'},
                                            {'label': 'Natural / Synthetic RMs decomposition', 'value': 'data3'},
                                            {'label': 'Solvent decomposition', 'value': 'data4'},
                                            {'label': 'Problematic RMs tracking', 'value': 'data5'},
                                            {'label': 'P20/P80', 'value': 'data6'},
                                            {'label': 'Comparator', 'value': 'data7'},
                                            {'label': 'Data display', 'value': 'data8'},
                                            {'label': 'RMs initial color and fundamentals', 'value': 'data9'},
                                            {'label': 'Schiff Base', 'value': 'data10'},
                                            {'label': 'Esters hydrolyses', 'value': 'data11'},
                                            {'label': 'Hydro Alcoholic solution formula', 'value': 'data12'},
                                            {'label': 'Raw materials Research', 'value': 'data13'}
                                        ],
                                        placeholder="Select"
                                    )
                                ]),
                                html.Hr(),
                                html.Div(className='app-controls-block', id='parameters-for-action', children=[]),

                            ])
                        ),
                        dcc.Tab(
                            label='Formula',
                            value='formula',
                            children=html.Div(className='control-tab', children=[
                                html.Div(className='app-controls-block', children=[

                                    html.Div(id='architect-table-container', children=[])

                                ]
                                         )
                            ])
                        )

                    ])
                ]),
                html.Div(id='results-stability', style={'display': 'none'}, children=[]),
                html.Div(id='results-nat-synt', style={'display': 'none'}, children=[]),
                html.Div(id='results-comparator', style={'display': 'none'}, children=[]),
                html.Div(id='results-RMscolor', style={'display': 'none'}, children=[]),
                html.Div(id='results-data-show', style={'display': 'none'}, children=[]),
                html.Div(id='results-risk', style={'display': 'none'}, children=[]),
                html.Div(id='results-RMs-tracking', style={'display': 'none'}, children=[]),
                html.Div(id='results-hydro', style={'display': 'none'}, children=[]),
                html.Div(id='results-schiff', style={'display': 'none'}, children=[]),
                html.Div(id='results-ester', style={'display': 'none'}, children=[]),
                html.Div(id='generate-file', style={'display': 'none'}, children=[dcc.ConfirmDialog(
                    id='confirm',
                    message='There is no methyl antranilate in your formula !',
                )])

            ])
        ])


###########################################################################
################## Call Back & User interactions ##########################
###########################################################################


"""
Upload a formula from computer (operational) or from phoenix (not operational).
A formula need to be in the right format. If not, the callback will not be call.
"""


def callbacks(_app):
    # Callback to display computer upload or phoenix upload
    @_app.callback([Output('architect-uploaded-data', 'style'), Output('architect-download-button', 'style')],
                   [Input('architect-preloaded-uploaded', 'value')])
    def show_hide_uploaded(pre_up):
        if pre_up == 'phoenix':
            return {'display': 'none'}, {'display': 'inline-block'}
        elif pre_up == 'computer':
            return {'display': 'block'}, {'display': 'none'}
        else:
            return {'display': 'none'}, {'display': 'none'}

    # Callback to upload data from computer
    @_app.callback([Output('stash_upload_computer', 'data'), Output('df_data', 'data'), Output('loaded', 'children'),Output("missing_IPC",'data')],
                   [Input('upload-data', 'contents')],
                   [State('upload-data', 'filename'), State('stash_upload_computer', 'data')])
    # allow the upload of a personnal file (file type can be CSV (english), CSV (french), XML, XLSX)
    # Only on formula in each file.
    def update_output(contents, filename, current):
        # user_id = session['globaluserid']
        if (contents == None):
            raise PreventUpdate
        else:
            content_type, content_string = contents.split(',')
            decoded = base64.b64decode(content_string)
            #### So! We have the file as a string. But people tend to make a mess of the files they have.
            #### We'll need to adapt the code to guess the format and extract the infos: IPC,Dosage and name.
            if 'csv' in filename:
                # Assume that the user uploaded a CSV file
                try:
                    new_formula = pd.read_csv(io.StringIO(decoded.decode('utf-8')))
                except:
                    new_formula = pd.read_csv(io.StringIO(decoded.decode('utf-8')), sep=';').replace(',', '.',
                                                                                                     regex=True).fillna(
                        0).apply(pd.to_numeric)
                name = filename.strip('.csv')
            elif 'xls' in filename:
                # Assume that the user uploaded an excel file
                new_formula = pd.read_excel(io.BytesIO(decoded))
                name = filename.strip('.xlsx')
            elif 'xml' in filename:
                new_formula, name = read_excel_xml(io.StringIO(decoded.decode('utf-8')))
                if (name == ''):
                    name = filename.strip('.xml')

                # elif(use_name_of_file):
                # name = filename.strip('.xml')

            else:
                return current, '', '', 'Something went wrong! Please Upload Only XML XLS or CSV files',[]

        new_formula = new_formula.drop([c for c in new_formula if c is None], axis=1)
        new_formula = new_formula.dropna(axis=1, how='all')
        new_formula = new_formula.dropna()
        new_formula = new_formula.rename(columns=rename_dict_for_data_upload)
        if ('BOMItemNumber' in new_formula):
            new_formula = new_formula[['BOMItemName','BOMItemNumber', 'Parts']]
        else:
            if (len(new_formula.columns) == 2):
                new_formula.columns = ['IPC', 'dosage']
            elif (len(new_formula.columns) == 3):
                new_formula.columns = ['name', 'IPC', 'dosage']
                #new_formula = new_formula.drop('name', axis=1)
            elif (len(new_formula.columns) == 4):
                new_formula.columns = ['name of formula', 'name', 'IPC', 'dosage']
                new_formula = new_formula.drop(['name of formula', 'name'], axis=1)
        new_formula.columns = ['name','IPC', 'dosage']
        new_formula['IPC'] = new_formula['IPC'].apply(make_ipc)
        missing = list(set(new_formula['IPC'].unique()) - set(df_ing['IPC'].unique()))
        new_formula['dosage'] = new_formula['dosage'].apply(lambda x: str(x).replace(',', '.')).astype(float)
        new_formula['dosage'] = new_formula.groupby(['IPC'])['dosage'].transform('sum')
        new_formula['parts'] = new_formula['dosage'].copy()
        new_formula['dosage'] = new_formula['dosage'] * 100 / sum(new_formula['dosage'])
        new_formula = new_formula[new_formula['dosage'] > 0]

        new_formula = new_formula.set_index('IPC').join(df_ing.set_index('IPC'))
        new_formula = new_formula.drop('Name', axis=1)
        new_formula = new_formula.rename(columns={'name':'Name'})
        #new_formula['Name'] = new_formula['Name'].fillna(new_formula.index.to_series()).fillna('Not Found')
        new_formula['name of formula'] = name

        return new_formula.to_dict(), new_formula.to_dict(), [html.Div('Formula Loaded: ' + filename), html.Br(),
                                                              html.Div("Name: " + name), html.Br(), html.Div(
                [html.Div('Missing IPC: ')] + [html.Div(m) for m in missing])],missing

    # Display in Formula page on ArchiTECH
    @_app.callback(
        [Output('current_formula', 'data'), Output('architect-table-container', "children"), Output('name', 'data')],
        [Input('stash_upload_phoenix', 'data'), Input('stash_upload_computer', 'data')],
        [State('architect-preloaded-uploaded', 'value'), State('current_formula', 'data')])
    def up_form(Phoenix, computer, choice, current):
        if (choice == 'computer'):
            df = pd.DataFrame.from_dict(computer)
            if df.shape[0] == 0:
                df = pd.DataFrame.from_dict(current)
                if df['name of formula'][0] == "default Formula":
                    # doesn't return waiting for .... bcz not in children
                    return current, [], "Waiting for formula import"
                return current, [html.P(df['name of formula'][0]), df_to_table(df[['Name', 'parts']])], \
                       df['name of formula'][0]
            return computer, [html.P(df['name of formula'][0]), df_to_table(df[['Name', 'parts']])], \
                   df['name of formula'][0]
        elif (choice == 'phoenix'):
            df = pd.DataFrame.from_dict(Phoenix)
            if df.shape[0] == 0:
                df = pd.DataFrame.from_dict(current)
                return current, [html.P(df['name of formula'][0]), df_to_table(df[['Name', 'parts']])], \
                       df['name of formula'][0]
            return Phoenix, [html.P(df['name of formula'][0]), df_to_table(df[['Name', 'parts']])], \
                   df['name of formula'][0]
        else:
            df = pd.DataFrame.from_dict(current)
            return current, [html.P(df['name of formula'][0]), df_to_table(df[['Name', 'parts']])], \
                   df['name of formula'][0]

    # Choose an analysis function

    """
    Output style is showing or hiding depend of the analysis function chosen.
    Some datas are preloaded in order to be treated.
    """

    @_app.callback(
        [
            Output('parameters-for-action', 'children'),
            Output('results-stability', 'children'),
            Output('results-stability', 'style'),
            Output('results-nat-synt', 'style'),
            Output('results-comparator', 'style'),
            Output('results-RMscolor', 'style'),
            Output('results-data-show', 'style'),
            Output('results-risk', 'style'),
            Output('results-RMs-tracking', 'style'),
            Output('results-hydro', 'children'),
            Output('results-hydro', 'style'),
            Output('results-schiff', 'children'),
            Output('results-schiff', 'style'),
            Output('results-ester', 'style'),
            Output('aox_MP', 'data'),
            Output('aox_direct', 'data'),
            Output('df_data_color', 'data'),
            Output('solvent_MP', 'data'),
            Output('solvent_direct', 'data')
        ],
        [Input('action-type', 'value')],
        [
            State('df_data', 'data'),
            State('missing_IPC', 'data')
        ]
    )
    def update_action_options(value, df_data,missing):

        """
        --------------------------------------------------- Description of all the analysis function ---------------------------------------------------



        -------------------------------------------------------- Function 1 : Risk assessment ----------------------------------------------------------
        The software gives a formula risk assessment. Raw materials that can generate issues are highlighted in both non exploed and exploed formulas.
        To obtain exploded formula, the software splits naturals bases into their chemical components (based on Gold Standart analyses).
        This function also sorts a table with warning flags (coloration, dyes, offnote, metal traces, methanol, solubility potention issues).
        A red flag indicates a potentail strong issue.

        Output Used :
        - 'parameters-for-action', 'children'
        - 'results-risk', 'style'



        -------------------------------------------------------- Function 2 : AOX ----------------------------------------------------------------------
        This function gathers antioxidants origins in your formula (direct and indirect additions).

        Output Used :
        - 'results-stability', 'children'
        - 'results-stability', 'style'
        - 'aox_MP', 'data'
        - 'aox_direct', 'data'


        -------------------------------------------------------- Function 3 : Natural / Synthetic Decomposition ----------------------------------------
        This function gathers natural and synthetic raw materials partitions in your formula.

        Output Used :
        - 'parameters-for-action', 'children'
        - 'results-nat-synt', 'style'


        -------------------------------------------------------- Function 4 : Solvent Decomposition ----------------------------------------------------
        This function gathers solvents partitions in your formula (direct and indirect sources).

        Output Used :
        - 'results-stability', 'children'
        - 'results-stability', 'style'
        - 'solvent_MP', 'data'
        - 'solvent_direct', 'data'


        -------------------------------------------------------- Function 5 : Problematic RMs tracking -------------------------------------------------
        Methyl anthranilate, indole, linalyl acetate, vanillin and ethyl vanillin are common source of issues in FF.
        This function helps to identify sources of these raw materials in your formula or in all natural ingredients.

        Output Used :
        - 'parameters-for-action', 'children'
        - 'results-RMs-tracking', 'style'


        -------------------------------------------------------- Function 6 : P20 / P80 ----------------------------------------------------------------
        This function generates p20/p80 sub-formulas.

        Output Used :
        - 'results-stability', 'children'
        - 'results-stability', 'style'


        -------------------------------------------------------- Function 7 : Comparator ---------------------------------------------------------------
        This function allows to compare different formulas.

        Output Used :
        - 'parameters-for-action', 'children'
        - 'results-comparator', 'style'


        -------------------------------------------------------- Function 8 : Data Display -------------------------------------------------------------
        This function allows to compare different columns or analysis function for one formula.

        Output Used :
        - 'parameters-for-action', 'children'
        - 'results-data-show', 'style'


        -------------------------------------------------------- Function 9 : RMs initial color and fundamentals ---------------------------------------
        This function gives ingredients initial color in Neat Oil and fundamentals data on RMs in your formula.

        Output Used :
        - 'results-RMscolor', 'style'
        - 'df_data_color', 'data'


        -------------------------------------------------------- Function 10 : Schiff Base -------------------------------------------------------------
        This function gathers potential sources of Schiff bases and aldehydes classifications.
        Primary aldehydes react with methyl anthranilate to form Schiff bases. Use only secondary aldehydes when pre-forming Schiff bases.
        For crystal clear formulation, methyl anthranilate should be separated from any aldehyde (p20/p80 or SMA version).

        Output Used :
        - 'results-schiff', 'children'
        - 'results-schiff', 'style'


        -------------------------------------------------------- Function 12 : Hydro alcoholic solution formula ----------------------------------------
        This function gathers alcoholic formula report for customers (classic formula, SMA or P20/P80 cases).

        Output Used :
        - 'results-hydro', 'children'
        - 'results-hydro', 'style'


        """

        ###############################################################################################################################################
        ###################################################### FUNCTION 1 : Risk Assesment ############################################################
        ###############################################################################################################################################

        if (value == 'data1'):
            if df_data == {}:
                raise PreventUpdate
            else:
                return [
                           html.Div(className='app-controls-block', children=[
                               html.P(style={'height': '1vw'}),
                               html.Div(className='app-controls-name', children='Data to show'),
                               html.Div(id='data-show', children=[dcc.Dropdown(id='data-risk',
                                                                               options=[
                                                                                   {
                                                                                       'label': 'Non exploded formula risk assessment ',
                                                                                       'value': 'Non exploded formula risk assessment '},
                                                                                   {
                                                                                       'label': 'Exploded formula risk assessment',
                                                                                       'value': 'Exploded formula risk assessment'},
                                                                                   {'label': 'Warning flags summary',
                                                                                    'value': 'Warning flags summary'}
                                                                               ],
                                                                               value=[
                                                                                   'Non exploded formula risk assessment ',
                                                                                   'Exploded formula risk assessment'],
                                                                               persistence=True,
                                                                               multi=True

                                                                               ), ],
                                        )
                           ])
                       ], [], {'display': 'None'}, {'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {
                           'display': 'none'}, {'display': 'block', 'max-witdh': '30%', 'margin-left': '35%',
                                                'margin-right': '6%'}, {'display': 'none'}, [], {
                           'display': 'none'}, [], {'display': 'none'}, {'display': 'none'}, (), (), (), (), ()


        ###############################################################################################################################################
        ###################################################### FUNCTION 2 : AOX #######################################################################
        ###############################################################################################################################################

        elif (value == 'data2'):
            if df_data == {}:
                raise PreventUpdate
            else:
                df_aox_data = pd.DataFrame(df_data, columns=['dosage'])
                aox_columns = ['Name', 'IPC', 'Quantity %', 'AOX']
                columns_direct = ['AOX', '% DIRECT', '% INDIRECT', '% TOTAL']

                df_aox_data, df_direct, df_direct_bis, aox_in_direct = treatment_AOX(df_aox_data, df_aox, [], [], "AOX")

                return [], [
                    html.Hr(),
                    html.H4(className='what-is', children='Part of Antioxidant in Raw Materials'),
                    html.Hr(),
                    html.Div(style={'padding': '2vw'}, children=[dt.DataTable(
                        columns=[{"name": i, "id": i} for i in df_aox_data[aox_columns]],
                        data=df_aox_data.to_dict('records'),
                        export_columns='visible',
                        export_format='xlsx',
                        export_headers='display',
                        filter_action="native",
                         sort_action='native',
                         sort_mode='multi',
                        style_data_conditional=(data_bars_for_AOX(df_aox_data, 'Quantity %', aox_in_direct,missing)),
                        style_header={
                            'backgroundColor': '#0075cf',
                            'fontWeight': 'bold',
                            'fontSize': '12pt',
                            'whiteSpace': 'normal',
                        }, style_data={
                            'font-family': 'Arial',
                            'backgroundColor': '#262B3D',
                            'fontSize': '9pt',
                            'whiteSpace': 'normal',
                        },
                        style_cell={
                            'font-family': 'Arial',
                            'whiteSpace': 'normal',
                            'textAlign': 'left',
                            'height': '25px',
                            'backgroundColor': '#262B3D'
                        }
                    )]),
                    html.Hr(),
                    html.H4(className='what-is', children='Antioxidant direct or indirect'),
                    html.Hr(),
                    html.Div(style={'padding': '2vw'}, children=[dt.DataTable(
                        columns=[{"name": i, "id": i} for i in df_direct[columns_direct]],
                        data=df_direct.to_dict('records'),
                        export_columns='visible',
                        export_format='xlsx',
                        export_headers='display',
                        style_data_conditional=[
                            {
                                'if': {
                                    'column_id': '% DIRECT',
                                    'filter_query': '{% DIRECT} != 0'
                                },
                                'backgroundColor': '#d97b09',
                                'color': 'white',
                            },
                            {
                                'if': {
                                    'column_id': '% INDIRECT',
                                },
                                'backgroundColor': '#232838',
                                'color': 'white',
                            },
                            {
                                'if': {
                                    'column_id': '% INDIRECT',
                                    'filter_query': '{% INDIRECT} != 0'
                                },
                                'backgroundColor': '#d97b09',
                                'color': 'white',
                            },
                            {
                                'if': {
                                    'column_id': '% TOTAL',
                                    'filter_query': '{% TOTAL} != 0'
                                },
                                'backgroundColor': '#d97b09',
                                'color': 'white',
                            }
                        ],
                        style_header={
                            'backgroundColor': '#0075cf',
                            'fontWeight': 'bold',
                            'fontSize': '12pt',
                            'whiteSpace': 'normal',
                        }, style_data={
                            'font-family': 'Arial',
                            'backgroundColor': '#262B3D',
                            'fontSize': '9pt',
                            'whiteSpace': 'normal',
                        },
                        style_cell={
                            'font-family': 'Arial',
                            'whiteSpace': 'normal',
                            'textAlign': 'left'
                        }
                    )]),
                    html.Hr(),
                    html.H4(className='what-is', children='Antioxidant presence in Raw Materials'),
                    html.Hr(),
                    html.H6("Choose an AOX : ", style={"textAlign": 'center'}),
                    dcc.Dropdown(id='action-type-aox',
                                 # persistence=True,
                                 options=[
                                     {'label': 'AOXs partition in formula', 'value': 'AOXs partition in formula'},
                                     {'label': 'BHA', 'value': 'BHA'},
                                     {'label': 'BHT', 'value': 'BHT'},
                                     {'label': 'TBHQ', 'value': 'TBHQ'},
                                     {'label': 'TOCOPHEROL ALPHA', 'value': 'TOCOPHEROL ALPHA'},
                                     {'label': 'TOCOPHEROLS NATURAL', 'value': 'TOCOPHEROLS NATURAL'},
                                     {'label': 'STABILIFF', 'value': 'STABILIFF'},
                                     {'label': 'TINOGARD TT', 'value': 'TINOGARD TT'},
                                     {'label': 'BUTYL PHENOL,2,6,DITERT', 'value': 'BUTYL PHENOL,2,6,DITERT'},
                                     {'label': 'A-5237', 'value': 'A-5237'},
                                     {'label': 'CITRIC ACID', 'value': 'CITRIC ACID'},
                                 ],
                                 value='AOXs partition in formula',
                                 clearable=False,
                                 style={'max-width': '100%', 'fontSize': '10pt', 'textAlign': 'center'},
                                 ),
                    # html.Br(),
                    html.Div([
                        dcc.Graph(id='aox_diagram')
                    ]),
                    html.Br(),
                    html.Br(),
                ], {'display': 'block', 'max-witdh': '30%', 'margin-left': '35%', 'margin-right': '6%'}, {
                           'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {
                           'display': 'none'}, {'display': 'none'}, [], {'display': 'none'}, [], {'display': 'none'}, {
                           'display': 'none'}, df_aox_data.to_dict(), df_direct_bis.to_dict(), (), (), ()

        ###############################################################################################################################################
        ###################################################### FUNCTION 3 : Natural / Synthetic RMs Decomposition #####################################
        ###############################################################################################################################################

        elif (value == 'data3'):
            if df_data == {}:
                raise PreventUpdate
            else:
                return [
                           html.Div(className='app-controls-block', children=[
                               html.P(style={'height': '1vw'}),
                               html.Div(className='app-controls-name', children='Solvent as a group'),
                               daq.BooleanSwitch(id='solvent-as-a-group', on=True, color="#9B51E0",
                                                 style={'#float': 'right', 'display': 'inline-block', 'width': '7.35vw',
                                                        'position': 'relative'}),
                           ])
                       ], [], {'display': 'None'}, {'display': 'block', 'max-witdh': '30%', 'margin-left': '35%',
                                                    'margin-right': '6%'}, {'display': 'none'}, {'display': 'none'}, {
                           'display': 'none'}, {'display': 'none'}, {'display': 'none'}, [], {'display': 'none'}, [], {
                           'display': 'none'}, {'display': 'none'}, (), (), (), (), ()

        ###############################################################################################################################################
        ###################################################### FUNCTION 4 : Solvent Decomposition #####################################################
        ###############################################################################################################################################

        elif (value == 'data4'):
            if df_data == {}:
                raise PreventUpdate
            else:
                df_data_name=pd.DataFrame(df_data, columns=['Name'])
                df_data_dt = pd.DataFrame(df_data, columns=['dosage'])
                solvent_columns = ['Name', 'IPC', 'Quantity %', 'Dilution (%)', 'Solvent']
                solvent_direct = ['SOLVENT', '% DIRECT', '% INDIRECT', '% TOTAL']

                df_data_dt, df_solv_direct, df_solv_direct_bis, df_solvent_data = treatment_solvent(df_data_dt, df_ing)
                df_data_dt=IPC_not_found(df_data_dt,df_data_name)
                return [], [
                    html.Hr(),
                    html.H4(className='what-is', children='Parts of solvent in RM'),
                    html.Hr(),
                    html.Div(style={'padding': '2vw'}, children=[dt.DataTable(
                        columns=[{"name": i, "id": i} for i in df_data_dt[solvent_columns]],
                        data=df_data_dt.to_dict('records'),
                        export_columns='visible',
                        export_format='xlsx',
                        export_headers='display',
                        filter_action="native",
                         sort_action='native',
                         sort_mode='multi',
                        style_data_conditional=(data_bars(df_data_dt, 'Quantity %', 'Solvent',missing)),
                        style_header={
                            'backgroundColor': '#0075cf',
                            'fontWeight': 'bold',
                            'fontSize': '12pt',
                            'whiteSpace': 'normal',
                        }, style_data={
                            'font-family': 'Arial',
                            'backgroundColor': '#262B3D',
                            'fontSize': '9pt',
                            'whiteSpace': 'normal',
                        },
                        style_cell={
                            'font-family': 'Arial',
                            'whiteSpace': 'normal',
                            'textAlign': 'left',
                            'height': '25px',
                            'backgroundColor': '#262B3D'
                        }
                    )]),
                    html.Hr(),
                    html.H4(className='what-is', children='Solvent direct or indirect'),
                    html.Hr(),
                    html.Div(style={'padding': '2vw'}, children=[dt.DataTable(
                        columns=[{"name": i, "id": i} for i in df_solv_direct[solvent_direct]],
                        data=df_solv_direct.to_dict('records'),
                        export_columns='visible',
                        export_format='xlsx',
                        export_headers='display',
                        style_data_conditional=[
                            {
                                'if': {
                                    'column_id': '% DIRECT',
                                    'filter_query': '{% DIRECT} != 0'
                                },
                                'backgroundColor': '#d97b09',
                                'color': 'white',
                            },
                            {
                                'if': {
                                    'column_id': '% INDIRECT',
                                },
                                'backgroundColor': '#232838',
                                'color': 'white',
                            },
                            {
                                'if': {
                                    'column_id': '% INDIRECT',
                                    'filter_query': '{% INDIRECT} != 0'
                                },
                                'backgroundColor': '#d97b09',
                                'color': 'white',
                            },
                            {
                                'if': {
                                    'column_id': '% TOTAL',
                                    'filter_query': '{% TOTAL} != 0'
                                },
                                'backgroundColor': '#d97b09',
                                'color': 'white',
                            }
                        ],
                        style_header={
                            'backgroundColor': '#0075cf',
                            'fontWeight': 'bold',
                            'fontSize': '12pt',
                            'whiteSpace': 'normal',
                        }, style_data={
                            'font-family': 'Arial',
                            'backgroundColor': '#262B3D',
                            'fontSize': '9pt',
                            'whiteSpace': 'normal',
                        },
                        style_cell={
                            'font-family': 'Arial',
                            'whiteSpace': 'normal',
                            'textAlign': 'left'
                        }
                    )]),
                    html.Hr(),
                    html.H4(className='what-is', children='Solvent presence in RM'),
                    html.Hr(),
                    html.H6("Choose a solvent : ", style={"textAlign": 'center'}),
                    dcc.Dropdown(id='action-type-solvent',
                                 # persistence=True,
                                 options=[{'label': 'Solvents partition in formula',
                                           'value': 'Solvents partition in formula'}] + [{'label': i, 'value': i} for i
                                                                                         in df_solvent_data.columns if
                                                                                         i not in ['Name']],
                                 value='Solvents partition in formula',
                                 clearable=False,
                                 style={'max-width': '100%', 'fontSize': '10pt', 'textAlign': 'center'},
                                 ),
                    # html.Br(),
                    html.Div([
                        dcc.Graph(id='solvent_diagram')
                    ]),
                    html.Br(),
                    html.Br(),
                ], {'display': 'block', 'max-witdh': '30%', 'margin-left': '35%', 'margin-right': '6%'}, {
                           'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {
                           'display': 'none'}, {'display': 'none'}, [], {'display': 'none'}, [], {'display': 'none'}, {
                           'display': 'none'}, (), (), (), df_solvent_data.to_dict(), df_solv_direct_bis.to_dict()

        ###############################################################################################################################################
        ###################################################### FUNCTION 5 : Problematic RMs tracking ##################################################
        ###############################################################################################################################################

        elif (value == 'data5'):
            if df_data == {}:
                raise PreventUpdate
            else:
                return [
                           html.Div(className='app-controls-block', children=[
                               html.P(style={'height': '1vw'}),
                               html.Div(className='app-controls-name', children='Display all formula'),
                               daq.BooleanSwitch(id='display_all_formula', on=False, color="#9B51E0",
                                                 style={'#float': 'right', 'display': 'inline-block', 'width': '7.35vw',
                                                        'position': 'relative'}),
                           ])
                       ], [], {'display': 'None'}, {'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {
                           'display': 'none'}, {'display': 'none'}, {'display': 'block', 'max-witdh': '30%',
                                                                     'margin-left': '35%', 'margin-right': '6%'}, [], {
                           'display': 'none'}, [], {'display': 'none'}, {'display': 'none'}, (), (), (), (), ()


        ###############################################################################################################################################
        ###################################################### FUNCTION 6 : P20 / P80 #################################################################
        ###############################################################################################################################################

        elif value == 'data6':
            if df_data == {}:
                raise PreventUpdate
            else:

                df_data_dt = pd.DataFrame(df_data, columns=['parts'])
                df_p80 = pd.DataFrame(df_data, columns=['parts', 'Name'])

                df_p20, df_p80, P20_somme, P80_somme, P20_somme_2, P80_somme_2, perc, condition, methyl = treatment_P20_P80(
                    df_data_dt, df_p80, df_p20_p80,df_methyl_direct)

                if methyl == True:
                    # if <20% p20
                    if condition == False:
                        return [], [
                            html.Hr(),
                            html.H4(className='what-is', children='Parts in P20 - P80'),
                            html.Hr(),
                            html.Div(style={'textAlign': 'center'}, children=[
                                html.Div(style={'width': '150px', 'display': 'inline-block', 'textAlign': 'center'},
                                         children=[
                                             html.H4(children='P20 part'),
                                             html.H4(style={"border": "2px red solid"}, children=P20_somme_2)
                                         ]),
                                html.Div(style={'margin-left': '5%', 'display': 'inline-block', 'width': '150px',
                                                'textAlign': 'center'}, children=[
                                    html.H4(children='P80 part'),
                                    html.H4(style={"border": "2px red solid"}, children=P80_somme_2)
                                ]),
                                html.Div(style={'margin-left': '5%', 'display': 'inline-block', 'width': '150px',
                                                'textAlign': 'center'}, children=[
                                    html.H4(children='Total part'),
                                    html.H4(style={"border": "2px white solid"}, children=P20_somme_2+P80_somme_2)
                                ])
                            ]),
                            html.H5(children='P20 is completed at ' + str(perc) + ' %.',
                                    style={'textAlign': 'center', 'color': 'red'}),
                            html.Hr(),
                            html.H4(className='what-is', children='P20 formula'),
                            html.Hr(),
                            html.Div(style={'padding': '2vw'}, children=[dt.DataTable(
                                columns=[{"name": i, "id": i} for i in df_p20.columns],
                                data=df_p20.to_dict('records'),
                                export_columns='visible',
                                export_format='xlsx',
                                export_headers='display',
                                style_data_conditional=(data_bars(df_p20, 'parts', 'p20_true',missing)),

                                style_header={
                                    'backgroundColor': '#0075cf',
                                    'fontWeight': 'bold',
                                    'fontSize': '12pt',
                                    'whiteSpace': 'normal',
                                }, style_data={
                                    'font-family': 'Arial',
                                    'backgroundColor': '#262B3D',
                                    'fontSize': '9pt',
                                    'whiteSpace': 'normal',
                                },
                                style_cell={
                                    'font-family': 'Arial',
                                    'whiteSpace': 'normal',
                                    'textAlign': 'left'
                                }
                            )]),
                            html.Hr(),
                            html.H4(className='what-is', children='P80 formula'),
                            html.Hr(),
                            html.Div(style={'padding': '2vw'}, children=[dt.DataTable(
                                columns=[{"name": i, "id": i} for i in df_p80.columns],
                                data=df_p80.to_dict('records'),
                                export_columns='visible',
                                export_format='xlsx',
                                export_headers='display',
                                style_data_conditional=(data_bars(df_p80, 'parts', 'p80_true',missing)),
                                style_header={
                                    'backgroundColor': '#0075cf',
                                    'fontWeight': 'bold',
                                    'fontSize': '12pt',
                                    'whiteSpace': 'normal',
                                },
                                style_data={
                                    'font-family': 'Arial',
                                    'backgroundColor': '#262B3D',
                                    'fontSize': '9pt',
                                    'whiteSpace': 'normal',
                                },
                                style_cell={
                                    'font-family': 'Arial',
                                    'whiteSpace': 'normal',
                                    'textAlign': 'left'
                                }
                            )]),
                        ], {'display': 'block', 'max-witdh': '30%', 'margin-left': '35%', 'margin-right': '6%'}, {
                                   'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {
                                   'display': 'none'}, {'display': 'none'}, [], {'display': 'none'}, [], {
                                   'display': 'none'}, {'display': 'none'}, (), (), (), (), ()


                    # 20% p20
                    elif condition == True:
                        return [], [
                            html.Hr(),
                            html.H4(className='what-is', children='Parts in P20 - P80'),
                            html.Hr(),
                            html.Div(style={'textAlign': 'center'}, children=[
                                html.Div(style={'width': '150px', 'display': 'inline-block', 'textAlign': 'center'},
                                         children=[
                                             html.H4(children='P20 part'),
                                             html.H4(style={"border": "2px white solid"}, children=P20_somme)
                                         ]),
                                html.Div(style={'margin-left': '5%', 'display': 'inline-block', 'width': '150px',
                                                'textAlign': 'center'}, children=[
                                    html.H4(children='P80 part'),
                                    html.H4(style={"border": "2px white solid"}, children=P80_somme)
                                ]),
                                html.Div(style={'margin-left': '5%', 'display': 'inline-block', 'width': '150px',
                                                'textAlign': 'center'}, children=[
                                    html.H4(children='Total part'),
                                    html.H4(style={"border": "2px white solid"}, children=P20_somme+P80_somme)
                                ])
                            ]),
                            html.Hr(),
                            html.H4(className='what-is', children='P20 formula'),
                            html.Hr(),
                            html.Div(style={'padding': '2vw'}, children=[dt.DataTable(
                                columns=[{"name": i, "id": i} for i in df_p20.columns],
                                data=df_p20.to_dict('records'),
                                export_columns='visible',
                                export_format='xlsx',
                                export_headers='display',
                                style_data_conditional=(
                                    data_bars(df_p20, 'parts', 'p20_false',missing)
                                ),
                                # {
                                #     'if':{
                                #     'column_id':'Name',
                                #     'filter_query':'{AOX} contains {dosage}'
                                #     },
                                #     'backgroundColor': '#3D9970',
                                #     'color': 'white',
                                # }
                                # ],
                                style_header={
                                    'backgroundColor': '#0075cf',
                                    'fontWeight': 'bold',
                                    'fontSize': '12pt',
                                    'whiteSpace': 'normal',
                                }, style_data={
                                    'font-family': 'Arial',
                                    'backgroundColor': '#262B3D',
                                    'fontSize': '9pt',
                                    'whiteSpace': 'normal',
                                },
                                style_cell={
                                    'font-family': 'Arial',
                                    'whiteSpace': 'normal',
                                    # 'minWidth': '1vw !important',
                                    # 'backgroundColor': '#262B3D',
                                    'textAlign': 'left'
                                }
                            )]),
                            html.Hr(),
                            html.H4(className='what-is', children='P80 formula'),
                            html.Hr(),
                            html.Div(style={'padding': '2vw'}, children=[dt.DataTable(
                                columns=[{"name": i, "id": i} for i in df_p80.columns],
                                data=df_p80.to_dict('records'),
                                export_columns='visible',
                                export_format='xlsx',
                                export_headers='display',
                                style_data_conditional=(
                                    data_bars(df_p80, 'parts', 'p80_false',missing)
                                ),
                                # {
                                #     'if':{
                                #     'column_id':'Name',
                                #     'filter_query':'{AOX} contains {dosage}'
                                #     },
                                #     'backgroundColor': '#3D9970',
                                #     'color': 'white',
                                # }
                                # ],
                                style_header={
                                    'backgroundColor': '#0075cf',
                                    'fontWeight': 'bold',
                                    'fontSize': '12pt',
                                    'whiteSpace': 'normal',
                                }, style_data={
                                    'font-family': 'Arial',
                                    'backgroundColor': '#262B3D',
                                    'fontSize': '9pt',
                                    'whiteSpace': 'normal',
                                },
                                style_cell={
                                    'font-family': 'Arial',
                                    'whiteSpace': 'normal',
                                    # 'minWidth': '1vw !important',
                                    # 'backgroundColor': '#262B3D',
                                    'textAlign': 'left'
                                }
                            )]),
                        ], {'display': 'block', 'max-witdh': '30%', 'margin-left': '35%', 'margin-right': '6%'}, {
                                   'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {
                                   'display': 'none'}, {'display': 'none'}, [], {'display': 'none'}, [], {
                                   'display': 'none'}, {'display': 'none'}, (), (), (), (), ()


                elif methyl == False:
                    return [], [
                        html.Hr(),
                        html.H4(className='what-is', children='P20 - P80'),
                        html.Hr(),
                        html.H4(
                            children="There is no methyl anthranilate in your formula either in direct of in indirect addition. There is no need to proceed p20 / p80.")
                    ], {'display': 'block', 'max-witdh': '30%', 'margin-left': '35%', 'margin-right': '6%'}, {
                               'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {
                               'display': 'none'}, {'display': 'none'}, [], {'display': 'none'}, [], {
                               'display': 'none'}, {'display': 'none'}, (), (), (), (), ()

        ###############################################################################################################################################
        ###################################################### FUNCTION 7 : Comparator ################################################################
        ###############################################################################################################################################

        elif (value == 'data7'):
            return [
                       html.Div(className='app-controls-block', children=[
                           html.Div(id='comparator-uploaded-data', children=[
                               dcc.Upload(
                                   id="upload-data-comparator",
                                   className='control-upload',
                                   children=html.Div(
                                       [
                                           "Drag and Drop or "
                                           "click to import "
                                           "the file here!"
                                       ]
                                   ),
                                   multiple=True,
                               ),

                               html.Hr(),
                               html.Div(className='app-controls-block', children=[
                                   #html.Div(className='app-controls-name', children='Solvent explosion'),
                                   daq.BooleanSwitch(id='solvent-explosion', on=False, color="#9B51E0",
                                                     style={'display': 'none'}),
                                   #style={'#float': 'right', 'display': 'inline-block','width': '7.35vw', 'position': 'relative'}),
                                   #html.Hr(),
                                   html.Div(id='loaded-comparator', className='app-controls-name',
                                            style={'width': '15vw'})
                               ])
                           ]),
                       ])
                   ], [], {'display': 'None'}, {'display': 'none'}, {'display': 'block', 'max-witdh': '30%',
                                                                     'margin-left': '35%', 'margin-right': '6%'}, {
                       'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {'display': 'none'}, [], {
                       'display': 'none'}, [], {'display': 'none'}, {'display': 'none'}, (), (), (), (), ()


        ###############################################################################################################################################
        ###################################################### FUNCTION 8 : Data Display ##############################################################
        ###############################################################################################################################################

        elif value == 'data8':
            if df_data == {}:
                raise PreventUpdate
            else:

                return [
                           html.Div(className='app-controls-block', children=[
                               html.P(style={'height': '1vw'}),
                               html.Div(className='app-controls-name', children='Data to show'),
                               html.Div(id='data-show', children=[dcc.Dropdown(id='data-to-show',
                                                                               options=[{'label': 'PURE QUANTITY %',
                                                                                         'value': 'PURE QUANTITY %'}] + [
                                                                                           {'label': i, 'value': i} for
                                                                                           i in df_ing.columns if
                                                                                           i not in ['IPC', 'Name',
                                                                                                     'Neat IPC']],
                                                                               value=[],
                                                                               persistence=True,
                                                                               multi=True

                                                                               ), ],
                                        # style={'#float':'right','display':'inline-block','width': '7.35vw',  'position': 'relative'}
                                        )
                           ])
                       ], [], {'display': 'None'}, {'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {
                           'display': 'block', 'max-witdh': '30%', 'margin-left': '35%', 'margin-right': '6%'}, {
                           'display': 'none'}, {'display': 'none'}, [], {'display': 'none'}, [], {'display': 'none'}, {
                           'display': 'none'}, (), (), (), (), ()

        ###############################################################################################################################################
        ###################################################### FUNCTION 9 : RMs initial color and fundamentals ########################################
        ###############################################################################################################################################

        elif value == 'data9':
            if df_data == {}:
                raise PreventUpdate
            else:
                df_data_dt = pd.DataFrame(df_data, columns=['dosage'])

                df_data_dt = df_data_dt.join(df_RMs_color.set_index('IPC'))
                df_data_dt = df_data_dt.drop('Synthetic/Natural', axis=1)

                return [], [], {'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {'display': 'block',
                                                                                               'max-witdh': '30%',
                                                                                               'margin-left': '35%',
                                                                                               'margin-right': '6%'}, {
                           'display': 'none'}, {'display': 'none'}, {'display': 'none'}, [], {'display': 'none'}, [], {
                           'display': 'none'}, {'display': 'none'}, (), (), df_data_dt.to_dict(), (), ()

        ###############################################################################################################################################
        ###################################################### FUNCTION 10 : Schiff Base ##############################################################
        ###############################################################################################################################################

        elif value == 'data10':
            if df_data == {}:
                raise PreventUpdate
            else:

                df_data_final = pd.DataFrame(df_data, columns=['dosage'])

                df_data_final, indirect_aldehydes_class1, direct_aldehydes_class1, indirect_aldehydes_class2, direct_aldehydes_class2, indirect_aldehydes_class3, direct_aldehydes_class3, IPC_indirect_methyl, IPC_direct_methyl, total_aldehyde, total_methyl, primary_aldehyde, secondary_aldehyde, tertiary_aldehyde, direct_methyl, indirect_methyl = treatment_schiff_base(
                    df_data_final, df_ing, df_explosion_nat, df_BDS_natural, df_BDS, df_methyl_indirect,
                    df_methyl_direct)

                return [], [], {'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {
                    'display': 'none'}, {'display': 'none'}, {'display': 'none'}, [], {'display': 'none'}, [
                           html.Hr(),
                           html.H4(className='what-is', children='Schiff Base'),
                           html.Hr(),
                           html.Div(style={'textAlign': 'center'}, children=[
                               html.Div(style={'width': '150px', 'display': 'inline-block', 'textAlign': 'center'},
                                        children=[
                                            html.H4(children='Primary Aldehyde'),
                                            html.H4(style={"border": "2px #DC800C solid"}, children=primary_aldehyde)
                                        ]),
                               html.Div(style={'margin-left': '5%', 'display': 'inline-block', 'width': '150px',
                                               'textAlign': 'center'}, children=[
                                   html.H4(children='Secondary Aldehydes'),
                                   html.H4(style={"border": "2px #D3BE11 solid"}, children=secondary_aldehyde)
                               ]),
                               html.Div(style={'margin-left': '5%', 'display': 'inline-block', 'width': '150px',
                                               'textAlign': 'center'}, children=[
                                   html.H4(children='Tertiary Aldehydes'),
                                   html.H4(style={"border": "2px #11D34C solid"}, children=tertiary_aldehyde)
                               ]),
                               html.Div(style={'margin-left': '5%', 'display': 'inline-block', 'width': '150px',
                                               'textAlign': 'center'}, children=[
                                   html.H4(children='Direct Methyl Anthranilate'),
                                   html.H4(style={"border": "2px #D311AA solid"}, children=direct_methyl)
                               ]),
                               html.Div(style={'margin-left': '5%', 'display': 'inline-block', 'width': '150px',
                                               'textAlign': 'center'}, children=[
                                   html.H4(children='Indirect Methyl Anthranilate'),
                                   html.H4(style={"border": "2px #1175D3 solid"}, children=indirect_methyl)
                               ])
                           ]),
                           html.Br(),
                           html.Div(style={'textAlign': 'center'}, children=[
                               html.Div(style={'width': '350px', 'display': 'inline-block', 'textAlign': 'center'},
                                        children=[
                                            html.H4(children='Total Primary + Secondary Aldehydes'),
                                            html.H4(style={"border": "2px #DC800C solid"}, children=total_aldehyde)
                                        ]),
                               html.Div(style={'margin-left': '5%', 'display': 'inline-block', 'width': '350px',
                                               'textAlign': 'center'}, children=[
                                   html.H4(children='Total Methyl Anthranilate'),
                                   html.H4(style={"border": "2px #D311AA solid"}, children=total_methyl)
                               ])
                           ]),
                           html.Br(),
                           html.Div(style={'padding': '2vw'}, children=[dt.DataTable(
                               columns=[{"name": i, "id": i} for i in df_data_final.columns],
                               data=df_data_final.to_dict('records'),
                               export_columns='visible',
                               export_format='xlsx',
                               export_headers='display',
                                filter_action="native",
                             sort_action='native',
                             sort_mode='multi',
                               style_data_conditional=(
                                   data_schiff_base(indirect_aldehydes_class1, direct_aldehydes_class1,
                                                    indirect_aldehydes_class2, direct_aldehydes_class2,
                                                    indirect_aldehydes_class3, direct_aldehydes_class3,
                                                    IPC_indirect_methyl, IPC_direct_methyl,missing)
                               ),
                               style_header={
                                   'backgroundColor': '#0075cf',
                                   'fontWeight': 'bold',
                                   'fontSize': '12pt',
                                   'whiteSpace': 'normal',
                               }, style_data={
                                   'font-family': 'Arial',
                                   'backgroundColor': '#262B3D',
                                   'fontSize': '9pt',
                                   'whiteSpace': 'normal',
                               },
                               style_cell={
                                   'font-family': 'Arial',
                                   'whiteSpace': 'normal',
                                   # 'minWidth': '1vw !important',
                                   # 'backgroundColor': '#262B3D',
                                   'textAlign': 'left',
                                   'height': '25px',
                                   'backgroundColor': '#262B3D'
                               }
                           )])], {'display': 'block', 'max-witdh': '30%', 'margin-left': '35%', 'margin-right': '6%'}, {
                           'display': 'none'}, (), (), (), (), ()

        ###############################################################################################################################################
        ###################################################### FUNCTION 11 : Ester Hydrolyses #########################################################
        ###############################################################################################################################################

        elif value == 'data11':
            if df_data == {}:
                raise PreventUpdate
            else:
                df_data = pd.DataFrame(df_data, columns=['dosage'])
                df_list = treatment_List_Ester(df_data, df_list_ester)
                list_ester = list(df_list['Name'])

                return [
                           html.Div(className='app-controls-ester', children=[
                               html.Div(children='Choose the Ester Component :',
                                        style={'textAlign': 'center', 'fontSize': '12pt'}),
                               html.Div(style={"textAlign": 'center', "max-width": '100%'},
                                        children=dcc.Dropdown(id='list-ester',
                                                              options=[{'label': i, 'value': i} for i in list_ester],
                                                              style={'max-width': '100%', 'fontSize': '10pt'},
                                                              ),
                                        ),
                               html.P(
                                   'You can calculate residual and deteriorate ester % among esters from the list. Only esters with associated kinetics data can be chosen in this list. To have a global overview of ester classes and related stabilities, please click on Esters classes',
                                   style={'font-style': 'italic'}),
                               html.Div(style={'textAlign': 'center'}, children=[
                                   html.Div(style={'width': '100px', 'display': 'inline-block', 'textAlign': 'center'},
                                            children=[
                                                html.H4(style={'fontSize': '10pt'}, children='Temperature (°C) :'),
                                                dcc.Dropdown(id='ester-temp',
                                                             options=[{'label': i, 'value': i} for i in
                                                                      [10, 20, 25, 30, 40, 50, 60]],
                                                             value=20
                                                             )
                                            ]),
                                   html.Div(style={'margin-left': '30%', 'display': 'inline-block', 'width': '100px',
                                                   'textAlign': 'center'}, children=[
                                       html.H4(style={'fontSize': '10pt'}, children='time (days) :'),
                                       dcc.Dropdown(id='ester-time',
                                                    options=[{'label': i, 'value': i} for i in range(1, 1201)],
                                                    value=100
                                                    )
                                   ])
                               ]),
                               html.Div(style={'textAlign': 'center'}, children=[
                                   html.Div(style={'width': '100px', 'display': 'inline-block', 'textAlign': 'center'},
                                            children=[
                                                html.H4(style={'fontSize': '10pt'}, children='pH :'),
                                                dcc.Dropdown(id='ester-ph',
                                                             options=[{'label': i, 'value': i} for i in
                                                                      [3, 4, 5, 6, 7, 8, 9, 10]],
                                                             value=3
                                                             )
                                            ]),
                                   html.Div(style={'margin-left': '30%', 'display': 'inline-block', 'width': '100px',
                                                   'textAlign': 'center'}, children=[
                                       html.H4(style={'fontSize': '10pt'}, children='% EDT :'),
                                       dcc.Dropdown(id='ester-edt',
                                                    options=[{'label': i, 'value': i} for i in range(0, 41)],
                                                    value=25
                                                    )
                                   ])
                               ]),
                               html.Br(),
                               html.Div(style={'textAlign': 'center'}, children=[
                                   html.Button("Calculate", id="ester-calculate", n_clicks=0,
                                               style={'width': '180px', 'display': 'inline-block',
                                                      'textAlign': 'center'}),
                                   html.Button("Esters Classes", id="ester-classes", n_clicks=0,
                                               style={'margin-left': '10%', 'display': 'inline-block', 'width': '180px',
                                                      'textAlign': 'center'}),
                               ]),
                           ])], [], {'display': 'None'}, {'display': 'None'}, {'display': 'none'}, {
                           'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {'display': 'none'}, [], {
                           'display': 'none'}, [], {'display': 'none'}, {'display': 'block', 'max-witdh': '30%',
                                                                         'margin-left': '35%',
                                                                         'margin-right': '6%'}, (), (), (), (), ()


        ###############################################################################################################################################
        ###################################################### FUNCTION 12 : Hydro alcoholic solution formula #########################################
        ###############################################################################################################################################

        elif value == 'data12':
            if df_data == {}:
                raise PreventUpdate
            else:
                df_data_dt = pd.DataFrame(df_data, columns=['dosage'])
                today = date.today()
                d1 = today.strftime("%d/%m/%Y")
                formulaire = [
                    html.Hr(),
                    html.H4(className='what-is', children='Alcoholic Solution Form'),
                    html.Hr(),
                    html.Div(style={'border-width': '2px', 'border-style': 'double', 'border-color': 'whilte'},
                             children=[
                                 html.Br(),
                                 dcc.Dropdown(id='action-hydro',
                                              # persistence=True,
                                              options=[
                                                  {'label': i, 'value': i} for i in
                                                  ['Alcoholic solution', 'Alcoholic solution P20/P80',
                                                   'Alcoholic solution SMA']
                                              ],
                                              value=[],
                                              # placeholder="Select a type reference",
                                              clearable=False,
                                              style={'max-width': '100%', 'fontSize': '10pt', 'textAlign': 'center'},
                                              ),
                                 html.Br(),
                                 html.Div(style={'textAlign': 'center'}, children=[
                                     html.H6("Client / Project :",
                                             style={'width': '25%', 'display': 'inline-block', "textAlign": "right"}),
                                     html.H6("",
                                             style={'width': '2%', 'display': 'inline-block', 'margin-right': '1%'}),
                                     dcc.Input(id="input1", type="text", placeholder="",
                                               style={'width': '20%', 'display': 'inline-block'})
                                 ]),
                                 html.Br(),
                                 html.Div(style={'textAlign': 'center'}, children=[
                                     html.H6("Formula name :",
                                             style={'width': '25%', 'display': 'inline-block', "textAlign": "right"}),
                                     html.H6("",
                                             style={'width': '2%', 'display': 'inline-block', 'margin-right': '1%'}),
                                     dcc.Input(id="input2", type="text", placeholder="",
                                               style={'width': '20%', 'display': 'inline-block'})
                                 ]),
                                 html.Br(),
                                 html.Div(style={'textAlign': 'center'}, children=[
                                     html.H6("Fragrance % in EDT :",
                                             style={'width': '25%', 'display': 'inline-block', "textAlign": "right"}),
                                     html.H6("",
                                             style={'width': '2%', 'display': 'inline-block', 'margin-right': '1%'}),
                                     dcc.Input(id="input3", type="number", placeholder="",
                                               style={'width': '20%', 'display': 'inline-block'})
                                 ]),
                                 html.Br(),
                                 html.Div(style={'textAlign': 'center'}, children=[
                                     html.H6("Ethanol % in EDT :",
                                             style={'width': '25%', 'display': 'inline-block', "textAlign": "right"}),
                                     html.H6("",
                                             style={'width': '2%', 'display': 'inline-block', 'margin-right': '1%'}),
                                     dcc.Input(id="input4", type="number", placeholder="",
                                               style={'width': '20%', 'display': 'inline-block'})
                                 ]),
                                 html.Br(),
                                 # additive
                                 html.Div(style={'textAlign': 'center'}, children=[
                                     html.H6("Additive:",
                                             style={'width': '25%', 'display': 'inline-block', "textAlign": "right"}),
                                     html.H6("",
                                             style={'width': '2%', 'display': 'inline-block', 'margin-right': '1%'}),
                                     html.Div(style={'width': '40%', 'display': 'inline-block'}, children=[
                                         dcc.Dropdown(id="input8", options=[{'label': i, 'value': i} for i in
                                                                            ['BHT', 'Covabsorb', 'Parsol 1789',
                                                                             'Tinogard Q']], multi=True, value=[],
                                                      style={'width': '100%', 'display': 'inline-block',
                                                             'position': 'relative', 'color': 'black'})
                                     ])
                                 ]),

                                 html.Div(id="additif1", style={'textAlign': 'center', 'display': 'none'}, children=[

                                     html.H6("BHT:",
                                             style={'width': '25%', 'display': 'inline-block', "textAlign": "right"}),
                                     html.H6("",
                                             style={'width': '2%', 'display': 'inline-block', 'margin-right': '1%'}),
                                     dcc.Input(id="input9", type="number", placeholder="",
                                               style={'width': '20%', 'display': 'inline-block'})
                                 ]),

                                 html.Div(id="additif2", style={'textAlign': 'center', 'display': 'none'}, children=[

                                     html.H6("Covabsorb:",
                                             style={'width': '25%', 'display': 'inline-block', "textAlign": "right"}),
                                     html.H6("",
                                             style={'width': '2%', 'display': 'inline-block', 'margin-right': '1%'}),
                                     dcc.Input(id="input10", type="number", placeholder="",
                                               style={'width': '20%', 'display': 'inline-block'})
                                 ]),

                                 html.Div(id="additif3", style={'textAlign': 'center', 'display': 'none'}, children=[

                                     html.H6("Parsol 1789:",
                                             style={'width': '25%', 'display': 'inline-block', "textAlign": "right"}),
                                     html.H6("",
                                             style={'width': '2%', 'display': 'inline-block', 'margin-right': '1%'}),
                                     dcc.Input(id="input11", type="number", placeholder="",
                                               style={'width': '20%', 'display': 'inline-block'})
                                 ]),

                                 html.Div(id="additif4", style={'textAlign': 'center', 'display': 'none'}, children=[

                                     html.H6("Tinogard Q:",
                                             style={'width': '25%', 'display': 'inline-block', "textAlign": "right"}),
                                     html.H6("",
                                             style={'width': '2%', 'display': 'inline-block', 'margin-right': '1%'}),
                                     dcc.Input(id="input12", type="number", placeholder="",
                                               style={'width': '20%', 'display': 'inline-block'})
                                 ]),
                                 html.Br(),
                                 html.Div(style={'textAlign': 'center'}, children=[
                                     html.H6("Maturation Time (weeks):",
                                             style={'width': '25%', 'display': 'inline-block', "textAlign": "right"}),
                                     html.H6("",
                                             style={'width': '2%', 'display': 'inline-block', 'margin-right': '1%'}),
                                     dcc.Input(id="input5", type="text", placeholder="",
                                               style={'width': '20%', 'display': 'inline-block'})
                                 ]),
                                 html.Br(),
                                 html.Div(style={'textAlign': 'center'}, children=[
                                     html.H6("Maceration Time (weeks):",
                                             style={'width': '25%', 'display': 'inline-block', "textAlign": "right"}),
                                     html.H6("",
                                             style={'width': '2%', 'display': 'inline-block', 'margin-right': '1%'}),
                                     dcc.Input(id="input6", type="text", placeholder="",
                                               style={'width': '20%', 'display': 'inline-block'})
                                 ]),
                                 html.Br(),
                                 html.Div(style={'textAlign': 'center'}, children=[
                                     html.H6("Date :",
                                             style={'width': '25%', 'display': 'inline-block', "textAlign": "right"}),
                                     html.H6("",
                                             style={'width': '2%', 'display': 'inline-block', 'margin-right': '1%'}),
                                     dcc.Input(id="input7", type="text", value=str(d1),
                                               style={'width': '20%', 'display': 'inline-block'})
                                 ]),
                                 html.Br(),
                                 html.Div(style={'textAlign': 'center'}, children=[
                                     html.Button(children='Submit', id='submit-hydro', n_clicks=None)
                                 ]),
                                 html.Br(),
                             ])]
                return [], [], {'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {
                    'display': 'none'}, {'display': 'none'}, {'display': 'none'}, formulaire, {'display': 'block',
                                                                                               'max-witdh': '30%',
                                                                                               'margin-left': '35%',
                                                                                               'margin-right': '6%'}, [], {
                           'display': 'none'}, {'display': 'none'}, (), (), (), (), ()

        ###############################################################################################################################################
        ###################################################### FUNCTION 13 : Raw materials Research ###################################################
        ###############################################################################################################################################

        elif value == 'data13':
            return [], [
                    html.Hr(),
                    html.H4(className='what-is', children='Choose criterias'),
                    html.Hr(),
                    html.Div(id='data_RMs_search',children=[
                        dcc.Dropdown(id='data_search_criteria',
                                   options=[
                                      {'label': i, 'value': i} for
                                       i in df_fullData.columns if
                                       i not in ['IPC', 'Name',
                                                 'Neat IPC']],
                                   value=[],
                                   persistence=True,
                                   multi=True
                        )]
                    ),
                    html.Br(),
                    html.Div(id="fill_criteria",children=[]),
                    html.Hr(),
                    html.H4(className='what-is', children='Choose display columns'),
                    html.Hr(),
                    html.Div(id='data_RMs_search',children=[
                        dcc.Dropdown(id='data_display_columns',
                                   options=[
                                      {'label': i, 'value': i} for
                                       i in df_fullData.columns if
                                       i not in ['IPC', 'Name']],
                                   value=[],
                                   persistence=True,
                                   multi=True
                        )]
                    ),
                    html.Div(id="Research_result",children=[])
                ], {'display': 'block', 'max-witdh': '30%', 'margin-left': '35%', 'margin-right': '6%'}, {'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {
                    'display': 'none'}, {'display': 'none'}, {'display': 'none'}, [], {'display': 'none'}, [], {
                           'display': 'none'}, {'display': 'none'}, (), (), (), (), ()


        else:
            return [], [], {'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {'display': 'none'}, {
                'display': 'none'}, {'display': 'none'}, {'display': 'none'}, [], {'display': 'none'}, [], {
                       'display': 'none'}, {'display': 'none'}, (), (), (), (), ()


    ### Part of function 13 : rms research ###
    @_app.callback([Output('fill_criteria', 'children'),Output('fill_criteria', 'style')],[Input('data_search_criteria', 'value')])

    def criteria_search(value):
        """
        """
        children = []
        if value==[]:
            return [],{'display': 'none'}
        else:
            for i, j in enumerate(value):
                new_dropdown = html.Div(children=[
                    html.P(j),
                    dcc.Dropdown(
                        id={'type': 'action-criteria', 'index': i},
                        options=[{'label': t, 'value': t} for t in list(df_fullData[j].fillna('Not Found').unique())],
                        value=[],
                        style = {"color":"black"}
                    )])
                children.append(new_dropdown)



            return children,{'display': 'block', 'max-witdh': '30%', 'margin-left': '15%', 'margin-right': '15%'}

    @_app.callback(
        [
            Output('Research_result', 'children'),
            Output('Research_result', 'style'),
        ],
        [
            Input({'type': 'action-criteria', 'index': ALL}, 'value'),
            Input('data_display_columns', 'value')
        ],
        [
            State('data_search_criteria', 'value')
        ]
    )

    def display_results_search(value,columns,col):
        df_result=df_fullData
        for i,valeur in enumerate(col):
            if value[i]!=[]:
                print(value[i])
                df_result=df_result[df_result[valeur]==value[i]]
            else:
                continue
        cols = ['IPC', 'Name'] + [i for i in columns]
        print(cols)
        return  [html.Div(style={'padding': '2vw'}, children=[dt.DataTable(
             columns=[{"name": i, "id": i} for i in cols],
             data=df_result.to_dict('records'),
             export_columns='visible',
             export_format='xlsx',
             export_headers='display',
           filter_action="native",
           sort_action='native',
           sort_mode='multi',
             style_header={
                 'backgroundColor': '#0075cf',
                 'fontWeight': 'bold',
                 'fontSize': '12pt',
                 'whiteSpace': 'normal',
             }, style_data={
                 'font-family': 'Arial',
                 'backgroundColor': '#262B3D',
                 'fontSize': '9pt',
                 'whiteSpace': 'normal',
             },
             style_cell={
                 'font-family': 'Arial',
                 'whiteSpace': 'normal',
                 # 'minWidth': '1vw !important',
                 # 'backgroundColor': '#262B3D',
                 'textAlign': 'left',
                 'height': '25px',
                 'backgroundColor': '#262B3D'
             }
         )])], {'display':'block'}

    ### Part of the function 2 : AOX ###
    @_app.callback(Output('aox_diagram', 'figure'),
                   [Input('action-type-aox', 'value'),
                    Input('aox_MP', 'data'),
                    Input('aox_direct', 'data')])
    def aox_diagram(value, aox_MP, aox_direct):
        """
        This function display a diagram graph of anti-oxydant in the formula.
        The argument of the function are the type of AOX chosen (Input of action-type-aox, named value in the function) and datas of aox present in formula.

        In this function, we use many if to display results in function of the AOX chosen.
        The diagram graph depend of the parameter action-type-aox, aox_MP and aox_direct.

        Input :
        - aox data
        - type of aox of the diagram graph displays

        Output :
        - diagram graph

        Note : if value equal to "AOX partition in formula", we use aox_direct data. Else if value equal to some AOX, we use aox_MP data.
        """
        if value == "AOXs partition in formula":

            aox_direct_dt = pd.DataFrame(aox_direct)
            aox_direct_dt = aox_direct_dt.replace(0, np.nan).dropna(axis=1, how="all")

            if aox_direct_dt.empty == False:

                data = {
                    'values': aox_direct_dt['% TOTAL'],
                    'labels': aox_direct_dt['AOX'],
                    'type': 'pie',
                    'marker': {'line': {'width': '1', 'color': 'rgb(255, 255, 255)'}},
                    'textfont': {'color': 'rgb(255, 255, 255)'}
                }
                layout = {
                    'height': '200px',
                    'max-width': '50%',
                    'margin-left': '25%',
                    'paper_bgcolor': 'rgba(31,33,50,1)',
                    'plot_bgcolor': 'rgba(31,33,50,1)',
                    'legend': {'font':
                                   {'color': 'rgb(255, 255, 255)'},
                               'borderwidth': '1',
                               'bordercolor': 'rgb(255, 255, 255)'
                               },

                }

                return {
                    'data': [data],
                    'layout': layout
                }
            else:
                raise PreventUpdate

        elif value == "BHA":
            aox_MP_dt = pd.DataFrame(aox_MP)
            aox_MP_dt = aox_MP_dt.replace('', np.nan)

            aox_MP_dt['BHA'] = aox_MP_dt['BHA'].dropna(axis=0, how="all")
            if aox_MP_dt.empty == False:
                data = {
                    'values': aox_MP_dt['BHA'],
                    'labels': aox_MP_dt['Name'],
                    'type': 'pie',
                    'marker': {'line': {'width': '1', 'color': 'rgb(255, 255, 255)'}},
                    'textfont': {'color': 'rgb(255, 255, 255)'}
                }
                layout = {
                    'height': '200px',
                    'max-width': '80%',
                    'margin-left': '10%',
                    'paper_bgcolor': 'rgba(31,33,50,1)',
                    'plot_bgcolor': 'rgba(31,33,50,1)',
                    'legend': {'font':
                                   {'color': 'rgb(255, 255, 255)'},
                               'borderwidth': '1',
                               'bordercolor': 'rgb(255, 255, 255)'
                               },
                }
                return {
                    'data': [data],
                    'layout': layout
                }

            else:
                raise PreventUpdate

        elif value == "BHT":
            aox_MP_dt = pd.DataFrame(aox_MP)
            aox_MP_dt = aox_MP_dt.replace('', np.nan)

            aox_MP_dt['BHT'] = aox_MP_dt['BHT'].dropna(axis=0, how="all")
            if aox_MP_dt.empty == False:
                data = {
                    'values': aox_MP_dt['BHT'],
                    'labels': aox_MP_dt['Name'],
                    'type': 'pie',
                    'marker': {'line': {'width': '1', 'color': 'rgb(255, 255, 255)'}},
                    'textfont': {'color': 'rgb(255, 255, 255)'}
                }
                layout = {
                    'height': '200px',
                    'max-width': '80%',
                    'margin-left': '10%',
                    'paper_bgcolor': 'rgba(31,33,50,1)',
                    'plot_bgcolor': 'rgba(31,33,50,1)',
                    'legend': {'font':
                                   {'color': 'rgb(255, 255, 255)'},
                               'borderwidth': '1',
                               'bordercolor': 'rgb(255, 255, 255)'
                               },
                }
                return {
                    'data': [data],
                    'layout': layout
                }

            else:
                raise PreventUpdate

        elif value == "TBHQ":
            aox_MP_dt = pd.DataFrame(aox_MP)
            aox_MP_dt = aox_MP_dt.replace('', np.nan)

            aox_MP_dt['TBHQ'] = aox_MP_dt['TBHQ'].dropna(axis=0, how="all")
            if aox_MP_dt.empty == False:
                data = {
                    'values': aox_MP_dt['TBHQ'],
                    'labels': aox_MP_dt['Name'],
                    'type': 'pie',
                    'marker': {'line': {'width': '1', 'color': 'rgb(255, 255, 255)'}},
                    'textfont': {'color': 'rgb(255, 255, 255)'}
                }
                layout = {
                    'height': '200px',
                    'max-width': '80%',
                    'margin-left': '10%',
                    'paper_bgcolor': 'rgba(31,33,50,1)',
                    'plot_bgcolor': 'rgba(31,33,50,1)',
                    'legend': {'font':
                                   {'color': 'rgb(255, 255, 255)'},
                               'borderwidth': '1',
                               'bordercolor': 'rgb(255, 255, 255)'
                               },
                }
                return {
                    'data': [data],
                    'layout': layout
                }

            else:
                raise PreventUpdate

        elif value == "TOCOPHEROL ALPHA":
            aox_MP_dt = pd.DataFrame(aox_MP)
            aox_MP_dt = aox_MP_dt.replace('', np.nan)

            aox_MP_dt['TOCOPHEROL ALPHA'] = aox_MP_dt['TOCOPHEROL ALPHA'].dropna(axis=0, how="all")
            if aox_MP_dt.empty == False:
                data = {
                    'values': aox_MP_dt['TOCOPHEROL ALPHA'],
                    'labels': aox_MP_dt['Name'],
                    'type': 'pie',
                    'marker': {'line': {'width': '1', 'color': 'rgb(255, 255, 255)'}},
                    'textfont': {'color': 'rgb(255, 255, 255)'}
                }
                layout = {
                    'height': '200px',
                    'max-width': '80%',
                    'margin-left': '10%',
                    'paper_bgcolor': 'rgba(31,33,50,1)',
                    'plot_bgcolor': 'rgba(31,33,50,1)',
                    'legend': {'font':
                                   {'color': 'rgb(255, 255, 255)'},
                               'borderwidth': '1',
                               'bordercolor': 'rgb(255, 255, 255)'
                               },
                }
                return {
                    'data': [data],
                    'layout': layout
                }

            else:
                raise PreventUpdate

        elif value == "TOCOPHEROLS NATURAL":
            aox_MP_dt = pd.DataFrame(aox_MP)
            aox_MP_dt = aox_MP_dt.replace('', np.nan)

            aox_MP_dt['TOCOPHEROLS NATURAL'] = aox_MP_dt['TOCOPHEROLS NATURAL'].dropna(axis=0, how="all")
            if aox_MP_dt.empty == False:
                data = {
                    'values': aox_MP_dt['TOCOPHEROLS NATURAL'],
                    'labels': aox_MP_dt['Name'],
                    'type': 'pie',
                    'marker': {'line': {'width': '1', 'color': 'rgb(255, 255, 255)'}},
                    'textfont': {'color': 'rgb(255, 255, 255)'}
                }
                layout = {
                    'height': '200px',
                    'max-width': '80%',
                    'margin-left': '10%',
                    'paper_bgcolor': 'rgba(31,33,50,1)',
                    'plot_bgcolor': 'rgba(31,33,50,1)',
                    'legend': {'font':
                                   {'color': 'rgb(255, 255, 255)'},
                               'borderwidth': '1',
                               'bordercolor': 'rgb(255, 255, 255)'
                               },
                }
                return {
                    'data': [data],
                    'layout': layout
                }

            else:
                raise PreventUpdate

        elif value == "STABILIFF":
            aox_MP_dt = pd.DataFrame(aox_MP)
            aox_MP_dt = aox_MP_dt.replace('', np.nan)

            aox_MP_dt['STABILIFF'] = aox_MP_dt['STABILIFF'].dropna(axis=0, how="all")
            if aox_MP_dt.empty == False:
                data = {
                    'values': aox_MP_dt['STABILIFF'],
                    'labels': aox_MP_dt['Name'],
                    'type': 'pie',
                    'marker': {'line': {'width': '1', 'color': 'rgb(255, 255, 255)'}},
                    'textfont': {'color': 'rgb(255, 255, 255)'}
                }
                layout = {
                    'height': '200px',
                    'max-width': '80%',
                    'margin-left': '10%',
                    'paper_bgcolor': 'rgba(31,33,50,1)',
                    'plot_bgcolor': 'rgba(31,33,50,1)',
                    'legend': {'font':
                                   {'color': 'rgb(255, 255, 255)'},
                               'borderwidth': '1',
                               'bordercolor': 'rgb(255, 255, 255)'
                               },
                }
                return {
                    'data': [data],
                    'layout': layout
                }

            else:
                raise PreventUpdate

        elif value == "TINOGARD TT":
            aox_MP_dt = pd.DataFrame(aox_MP)
            aox_MP_dt = aox_MP_dt.replace('', np.nan)

            aox_MP_dt['TINOGARD TT'] = aox_MP_dt['TINOGARD TT'].dropna(axis=0, how="all")
            if aox_MP_dt.empty == False:
                data = {
                    'values': aox_MP_dt['TINOGARD TT'],
                    'labels': aox_MP_dt['Name'],
                    'type': 'pie',
                    'marker': {'line': {'width': '1', 'color': 'rgb(255, 255, 255)'}},
                    'textfont': {'color': 'rgb(255, 255, 255)'}
                }
                layout = {
                    'height': '200px',
                    'max-width': '80%',
                    'margin-left': '10%',
                    'paper_bgcolor': 'rgba(31,33,50,1)',
                    'plot_bgcolor': 'rgba(31,33,50,1)',
                    'legend': {'font':
                                   {'color': 'rgb(255, 255, 255)'},
                               'borderwidth': '1',
                               'bordercolor': 'rgb(255, 255, 255)'
                               },
                }
                return {
                    'data': [data],
                    'layout': layout
                }

            else:
                raise PreventUpdate

        elif value == "BUTYL PHENOL,2,6,DITERT":
            aox_MP_dt = pd.DataFrame(aox_MP)
            aox_MP_dt = aox_MP_dt.replace('', np.nan)

            aox_MP_dt['BUTYL PHENOL,2,6,DITERT'] = aox_MP_dt['BUTYL PHENOL,2,6,DITERT'].dropna(axis=0, how="all")
            if aox_MP_dt.empty == False:
                data = {
                    'values': aox_MP_dt['BUTYL PHENOL,2,6,DITERT'],
                    'labels': aox_MP_dt['Name'],
                    'type': 'pie',
                    'marker': {'line': {'width': '1', 'color': 'rgb(255, 255, 255)'}},
                    'textfont': {'color': 'rgb(255, 255, 255)'}
                }
                layout = {
                    'height': '200px',
                    'max-width': '80%',
                    'margin-left': '10%',
                    'paper_bgcolor': 'rgba(31,33,50,1)',
                    'plot_bgcolor': 'rgba(31,33,50,1)',
                    'legend': {'font':
                                   {'color': 'rgb(255, 255, 255)'},
                               'borderwidth': '1',
                               'bordercolor': 'rgb(255, 255, 255)'
                               },
                }
                return {
                    'data': [data],
                    'layout': layout
                }

            else:
                raise PreventUpdate

        elif value == "A-5237":
            aox_MP_dt = pd.DataFrame(aox_MP)
            aox_MP_dt = aox_MP_dt.replace('', np.nan)

            aox_MP_dt['A-5237'] = aox_MP_dt['A-5237'].dropna(axis=0, how="all")
            if aox_MP_dt.empty == False:
                data = {
                    'values': aox_MP_dt['A-5237'],
                    'labels': aox_MP_dt['Name'],
                    'type': 'pie',
                    'marker': {'line': {'width': '1', 'color': 'rgb(255, 255, 255)'}},
                    'textfont': {'color': 'rgb(255, 255, 255)'}
                }
                layout = {
                    'height': '200px',
                    'max-width': '80%',
                    'margin-left': '10%',
                    'paper_bgcolor': 'rgba(31,33,50,1)',
                    'plot_bgcolor': 'rgba(31,33,50,1)',
                    'legend': {'font':
                                   {'color': 'rgb(255, 255, 255)'},
                               'borderwidth': '1',
                               'bordercolor': 'rgb(255, 255, 255)'
                               },
                }
                return {
                    'data': [data],
                    'layout': layout
                }

            else:
                raise PreventUpdate

        elif value == "CITRIC ACID":
            aox_MP_dt = pd.DataFrame(aox_MP)
            aox_MP_dt = aox_MP_dt.replace('', np.nan)

            aox_MP_dt['CITRIC ACID'] = aox_MP_dt['CITRIC ACID'].dropna(axis=0, how="all")
            if aox_MP_dt.empty == False:
                data = {
                    'values': aox_MP_dt['CITRIC ACID'],
                    'labels': aox_MP_dt['Name'],
                    'type': 'pie',
                    'marker': {'line': {'width': '1', 'color': 'rgb(255, 255, 255)'}},
                    'textfont': {'color': 'rgb(255, 255, 255)'}
                }
                layout = {
                    'height': '200px',
                    'max-width': '80%',
                    'margin-left': '10%',
                    'paper_bgcolor': 'rgba(31,33,50,1)',
                    'plot_bgcolor': 'rgba(31,33,50,1)',
                    'legend': {'font':
                                   {'color': 'rgb(255, 255, 255)'},
                               'borderwidth': '1',
                               'bordercolor': 'rgb(255, 255, 255)'
                               },
                }
                return {
                    'data': [data],
                    'layout': layout
                }

            else:
                raise PreventUpdate

        else:
            raise PreventUpdate

    ### Part of the function 3 : Natural / synthetic decomposition ###
    @_app.callback(Output('results-nat-synt', 'children'),
                   [Input('solvent-as-a-group', 'on')],
                   [State('df_data', 'data'),State('missing_IPC', 'data')])
    def update_nat_synt(value, df_data,missing):
        """
        Input :
        - boolean if solvent considerate as group or no
        - data of formula

        Output :
        - datatable of results

        Note : if the boolean is True, we display all the group. If the boolean is False, we display all the group except the solvant.
        """
        if df_data == {}:
            raise PreventUpdate
        else:
            nat_syn_columns = ['Name', 'parts', 'dosage', 'Nat/Synth', 'Dilution (%)', 'Solvent']
            df_nat_synt = pd.DataFrame(df_data, columns=nat_syn_columns)

            column_datatable = ['Name', 'IPC', 'parts', 'Quantity %', 'Nat/Synth']
            if value == True:
                df_nat_synt, natural, synthetic, solvant, NF = treatment_nat_synt(df_nat_synt, df_solvent_nat_synt,
                                                                                    value)

                return [
                    html.Hr(),
                    html.H4(className='what-is', children='Natural Synthetic Decomposition'),
                    html.Hr(),
                    html.Div(style={'textAlign': 'center'}, children=[
                        html.Div(style={'width': '150px', 'display': 'inline-block', 'textAlign': 'center'}, children=[
                            html.H4(children='Natural %'),
                            html.H4(style={"border": "2px green solid"}, children=natural)
                        ]),
                        html.Div(style={'margin-left': '5%', 'display': 'inline-block', 'width': '150px',
                                        'textAlign': 'center'}, children=[
                            html.H4(children='Synthetic %'),
                            html.H4(style={"border": "2px #d97b09 solid"}, children=synthetic)
                        ]),
                        html.Div(style={'margin-left': '5%', 'display': 'inline-block', 'width': '150px',
                                        'textAlign': 'center'}, children=[
                            html.H4(children='Solvant %'),
                            html.H4(style={"border": "2px purple solid"}, children=solvant)
                        ]),
                        html.Div(style={'margin-left': '5%', 'display': 'inline-block', 'width': '150px',
                                        'textAlign': 'center'}, children=[
                            html.H4(children='Not Found %'),
                            html.H4(style={"border": "2px white solid"}, children=NF)
                        ])
                    ]),
                    html.Hr(),
                    html.H4(className='what-is', children='Details'),
                    html.Hr(),
                    html.Div(style={'padding': '2vw'}, children=[dt.DataTable(
                        columns=[{"name": i, "id": i} for i in df_nat_synt[column_datatable]],
                        data=df_nat_synt.to_dict('records'),
                        export_columns='visible',
                        export_format='xlsx',
                        export_headers='display',
                        filter_action="native",
                         sort_action='native',
                         sort_mode='multi',
                        style_data_conditional=(
                                data_bars(df_nat_synt, 'parts', 'nat/synt',missing) +
                                data_bars(df_nat_synt, 'Quantity %', '',[])),
                        style_header={
                            'backgroundColor': '#0075cf',
                            'fontWeight': 'bold',
                            'fontSize': '12pt',
                            'whiteSpace': 'normal',
                        }, style_data={
                            'font-family': 'Arial',
                            'backgroundColor': '#262B3D',
                            'fontSize': '9pt',
                            'whiteSpace': 'normal',
                        },
                        style_cell={
                            'font-family': 'Arial',
                            'whiteSpace': 'normal',
                            'textAlign': 'left',
                            'height': '25px',
                            'backgroundColor': '#262B3D'
                        }
                    )]),
                    html.Br(),
                    html.Br(),
                ]
            elif value == False:

                df_nat_synt, natural, synthetic, solvant, NF = treatment_nat_synt(df_nat_synt, df_solvent_nat_synt,
                                                                                  value)

                return [
                    html.Hr(),
                    html.H4(className='what-is', children='Natural Synthetic Decomposition'),
                    html.Hr(),
                    html.Div(style={'textAlign': 'center'}, children=[
                        html.Div(style={'width': '150px', 'display': 'inline-block', 'textAlign': 'center'}, children=[
                            html.H4(children='Natural %'),
                            html.H4(style={"border": "2px green solid"}, children=natural)
                        ]),
                        html.Div(style={'margin-left': '5%', 'display': 'inline-block', 'width': '150px',
                                        'textAlign': 'center'}, children=[
                            html.H4(children='Synthetic %'),
                            html.H4(style={"border": "2px #d97b09 solid"}, children=synthetic)
                        ]),
                        html.Div(style={'margin-left': '5%', 'display': 'inline-block', 'width': '150px',
                                        'textAlign': 'center'}, children=[
                            html.H4(children='Not Found %'),
                            html.H4(style={"border": "2px white solid"}, children=NF)
                        ])
                    ]),
                    html.Hr(),
                    html.H4(className='what-is', children='Details'),
                    html.Hr(),
                    html.Div(style={'padding': '2vw'}, children=[dt.DataTable(
                        columns=[{"name": i, "id": i} for i in df_nat_synt[column_datatable]],
                        data=df_nat_synt.to_dict('records'),
                        export_columns='visible',
                        export_format='xlsx',
                        export_headers='display',
                        filter_action="native",
                         sort_action='native',
                         sort_mode='multi',
                        style_data_conditional=(
                                data_bars(df_nat_synt, 'parts', 'nat/synt',missing) +
                                data_bars(df_nat_synt, 'Quantity %', '',missing)),
                        style_header={
                            'backgroundColor': '#0075cf',
                            'fontWeight': 'bold',
                            'fontSize': '12pt',
                            'whiteSpace': 'normal',
                        }, style_data={
                            'font-family': 'Arial',
                            'backgroundColor': '#262B3D',
                            'fontSize': '9pt',
                            'whiteSpace': 'normal',
                        },
                        style_cell={
                            'font-family': 'Arial',
                            'whiteSpace': 'normal',
                            'textAlign': 'left',
                            'height': '25px',
                            'backgroundColor': '#262B3D'
                        }
                    )]),
                    html.Br(),
                    html.Br(),
                ]


    ### Part of the function 7 : Comparator upload formulas ###
    @_app.callback(
        [
            Output('stash_upload_computer_comparator', 'data'),
            Output('formulas_name_comparator', 'data'),
            Output('df_data_comparator', 'data'),
            Output('loaded-comparator', 'children')
        ],
        [
            Input('upload-data-comparator', 'contents')
        ],
        [
            State('upload-data-comparator', 'filename'),
            State('stash_upload_computer_comparator', 'data')
        ])
    def update_output_comparator(contents, filenames, current):
        """
        This function allow to import many formulas in the application and then can be compared in function of many criterias.

        Input :
        - formulas data with their content and formula name*

        Output :
        - formula data
        - data of all formulas together

        """
        if (contents == None):
            raise PreventUpdate
        else:
            formulas = []
            df_all_dataframe = pd.DataFrame()
            i = 0
            for content, filename in zip(contents, filenames):

                new_formula = []
                content_type, content_string = content.split(',')
                decoded = base64.b64decode(content_string)
                #### So! We have the file as a string. But people tend to make a mess of the files they have.
                #### We'll need to adapt the code to guess the format and extract the infos: IPC,Dosage and name.
                if 'csv' in filename:
                    # Assume that the user uploaded a CSV file
                    try:
                        new_formula = pd.read_csv(io.StringIO(decoded.decode('utf-8')))
                    except:
                        new_formula = pd.read_csv(io.StringIO(decoded.decode('utf-8')), sep=';').replace(',', '.',
                                                                                                         regex=True).fillna(
                            0).apply(pd.to_numeric)
                    name = filename.strip('.csv')
                    formulas.append(name)
                elif 'xls' in filename:
                    # Assume that the user uploaded an excel file
                    new_formula = pd.read_excel(io.BytesIO(decoded))
                    name = filename.strip('.xlsx')
                    formulas.append(name)
                elif 'xml' in filename:
                    new_formula, name = read_excel_xml(io.StringIO(decoded.decode('utf-8')))
                    if (name == ''):
                        name = filename.strip('.xml')
                    formulas.append(name)

                    # elif(use_name_of_file):
                    # name = filename.strip('.xml')

                else:
                    return current, '', '', 'Something went wrong! Please Upload Only XML XLS or CSV files'

                new_formula = new_formula.drop([c for c in new_formula if c is None], axis=1)
                new_formula = new_formula.dropna(axis=1, how='all')
                new_formula = new_formula.dropna()
                new_formula = new_formula.rename(columns=rename_dict_for_data_upload)
                if ('BOMItemNumber' in new_formula):
                    new_formula = new_formula[['BOMItemNumber', 'Parts']]
                else:
                    if (len(new_formula.columns) == 2):
                        new_formula.columns = ['IPC', 'dosage']
                    elif (len(new_formula.columns) == 3):
                        new_formula.columns = ['name', 'IPC', 'dosage']
                        new_formula = new_formula.drop('name', axis=1)
                    elif (len(new_formula.columns) == 4):
                        new_formula.columns = ['name of formula', 'name', 'IPC', 'dosage']
                        new_formula = new_formula.drop(['name of formula', 'name'], axis=1)
                new_formula.columns = ['IPC', 'dosage']
                new_formula['IPC'] = new_formula['IPC'].apply(make_ipc)
                missing = list(set(new_formula['IPC'].unique()) - set(df_ing['IPC'].unique()))
                new_formula['dosage'] = new_formula['dosage'].apply(lambda x: str(x).replace(',', '.')).astype(float)
                new_formula['dosage'] = new_formula.groupby(['IPC'])['dosage'].transform('sum')
                new_formula['parts'] = new_formula['dosage'].copy()
                new_formula['dosage'] = new_formula['dosage'] * 100 / sum(new_formula['dosage'])
                new_formula = new_formula[new_formula['dosage'] > 0]

                new_formula = new_formula.set_index('IPC').join(df_ing.set_index('IPC'))
                new_formula['Name'] = new_formula['Name'].fillna(new_formula.index.to_series()).fillna('Not Found')
                new_formula['name of formula'] = name
                new_formula['number'] = i
                i = i + 1
                if df_all_dataframe.empty == True:
                    df_all_dataframe = pd.DataFrame(columns=new_formula.columns)

                df_all_dataframe = pd.concat([df_all_dataframe, new_formula])

            df_all_dataframe = df_all_dataframe.rename_axis('IPC').reset_index()
            df_all_dataframe = df_all_dataframe.pivot(index='name of formula', columns='IPC', values='dosage')

            return df_all_dataframe.to_dict(), formulas, df_all_dataframe.to_dict(), [
                html.Div([html.Div('Formula Loaded: ')] + [html.Div("- "+m) for m in formulas])]

    ### Part of the function 7 : Comparator display results ###
    @_app.callback(

        Output('results-comparator', 'children'),

        [
            Input('solvent-explosion', 'on'),
            Input('df_data_comparator', 'data')
        ],
        [
            State('formulas_name_comparator', 'data')
        ]
    )
    def update_comparator(value, df_data_comparator, formulas):
        """
        This function displays the choice of the reference formula and the comparation formulas.
        It display also the criteria that you want to compare between the formulas chosen.

        Input :
        - boolean if explosion of solvent or not
        - data formula

        Output :
        - datatable of results

        """
        if df_data_comparator == {}:
            raise PreventUpdate
        else:
            if value == True:
                # A CODER solvent decomposition
                raise PreventUpdate
            elif value == False:

                return [
                    html.Hr(),
                    html.H4(className='what-is', children='Choose formula reference'),
                    html.Hr(),
                    dcc.Dropdown(id='action-type-ref',
                                 # persistence=True,
                                 options=[
                                     {'label': k, 'value': i} for i, k in enumerate(formulas)
                                 ],
                                 value=0,
                                 # placeholder="Select a type reference",
                                 clearable=False,
                                 style={'max-width': '100%', 'fontSize': '10pt', 'textAlign': 'center'},
                                 ),
                    html.Hr(),
                    html.H4(className='what-is', children='Choose formulas comparation'),
                    html.Hr(),
                    dcc.Checklist(id='action-comparator',
                                  options=[
                                      {'label': k, 'value': i} for i, k in enumerate(formulas)
                                  ],
                                  value=[],
                                  style={'max-width': '100%', 'fontSize': '10pt', 'textAlign': 'left',
                                         'margin-left': '20%'}
                                  ),
                    html.Hr(),
                    html.H4(className='what-is', children='Choose type comparation'),
                    html.Hr(),
                    dcc.Dropdown(id='action-type-comparator',
                                 # persistence=True,
                                 options=[
                                     {'label': k, 'value': k} for k in comparator_type
                                 ],
                                 # value=formulas[0],
                                 value=comparator_type[0],
                                 clearable=False,
                                 style={'max-width': '100%', 'fontSize': '10pt', 'textAlign': 'center'},
                                 ),
                    html.Div(id="edt-dosage", children=[], style={'display': 'none'}),
                    html.Div(id="RMs_color", children=[], style={'display': 'none'}),
                    html.Div([
                        html.Div(id='datatable-comparator')
                    ]),
                    html.Br(),
                    html.Br(),
                ]

    ### Part of the function 7 : Comparator options ###
    @_app.callback(

        Output('action-comparator', 'options')
        ,
        [
            Input('action-type-ref', 'value')
        ],
        [
            State('formulas_name_comparator', 'data')
        ]
    )
    def update_comparator_formulas(value, formulas):
        """
        This functions displays all the formulas that are chosen to be compared

        Input :
        - formula reference name
        - all the formulas selected

        Output :
        - Display the options of formula comparator

        """
        input_list = formulas.copy()
        input_list.remove(input_list[value])
        return [
            # {'label':formulas[value]}
            {'label': k, 'value': i} for i, k in enumerate(input_list)
        ]
        # ],value,True

    ### Part of the function 7 : Comparator display datatable ###
    @_app.callback(
        [
            Output('datatable-comparator', 'children'),
            Output('edt-dosage', 'style'),
            Output('RMs_color', 'style')
        ],
        [
            Input('action-type-ref', 'value'),
            Input('action-comparator', 'value'),
            Input('action-type-comparator', 'value'),
            Input({'type': 'filter-dropdown', 'index': ALL}, 'value'),
            Input({'type': 'action-color-comparator', 'index': ALL}, 'value')
        ],
        [
            State('df_data_comparator', 'data'),
            State('formulas_name_comparator', 'data')
        ]
    )
    def datatable_comparator(value_ref, value_comparator, value_type, value_edt, value_color, df_data_comparator,
                             formulas):
        """
        This function displays the datatable result.

        Input :
        - The formula reference
        - the formulas comparation
        - the type of comparation
        - the data formula
        - the formulas name
        If type of comparation = EDT :
        - the dosage for each formula

        Output :
        - datatable

        """
        if value_ref == None and value_comparator == []:

            raise PreventUpdate
        else:
            df_data = pd.DataFrame(df_data_comparator)
            df_data = df_data.T.rename_axis("IPC").reset_index()
            formulas2 = formulas.copy()
            formulas2.remove(formulas2[value_ref])
            columns = ['IPC'] + [formulas[value_ref]] + list(pd.Series(formulas2).iloc[value_comparator])
            df_output = pd.DataFrame(df_data, columns=columns)
            col = list(pd.Series(formulas2).iloc[value_comparator])

            if value_type == "OIL":
                df_output, custom = treatment_comparator(df_data, df_output, df_ing, df_aox, df_RMs_color,
                                                         df_RMs_warning, value_ref, value_type, value_edt, value_color,
                                                         value_comparator, formulas, formulas2)

                return [
                           html.Div(style={'padding': '2vw'}, children=[dt.DataTable(
                               columns=[{"name": i, "id": i} for i in df_output.columns],
                               data=df_output.to_dict('records'),
                               export_columns='visible',
                               export_format='xlsx',
                               export_headers='display',
                               filter_action="native",
                                sort_action='native',
                                sort_mode='multi',
                               style_table={
                                   'maxHeight': '500px',
                                   'overflowY': 'scroll',
                                   'overflowX': 'scroll'
                               },
                               style_data_conditional=(
                                   data_comparator(formulas[value_ref], col)
                               ),
                               style_header={
                                   'backgroundColor': '#0075cf',
                                   'fontWeight': 'bold',
                                   'fontSize': '12pt',
                                   'whiteSpace': 'normal',
                               }, style_data={
                                   'font-family': 'Arial',
                                   'backgroundColor': '#262B3D',
                                   'fontSize': '9pt',
                                   'whiteSpace': 'normal',
                               },
                               style_cell={
                                   'font-family': 'Arial',
                                   'whiteSpace': 'normal',
                                   # 'minWidth': '1vw !important',
                                   # 'backgroundColor': '#262B3D',
                                   'textAlign': 'left',
                                    'height': '25px',
                                    'backgroundColor': '#262B3D'
                               }
                           )]),
                           html.Br(),
                           html.Br(),
                       ], {'display': 'none'}, {'display': 'none'}

            elif value_type == "EDT":
                df_output, custom = treatment_comparator(df_data, df_output, df_ing, df_aox, df_RMs_color,
                                                         df_RMs_warning, value_ref, value_type, value_edt, value_color,
                                                         value_comparator, formulas, formulas2)
                return [
                           html.Div(style={'padding': '2vw'}, children=[dt.DataTable(
                               columns=[{"name": i, "id": i} for i in df_output.columns],
                               data=df_output.to_dict('records'),
                               export_columns='visible',
                               export_format='xlsx',
                               export_headers='display',
                                filter_action="native",
                                 sort_action='native',
                                 sort_mode='multi',
                               style_table={
                                   'maxHeight': '500px',
                                   'overflowY': 'scroll',
                                   'overflowX': 'scroll'
                               },
                               style_data_conditional=(
                                   data_comparator(formulas[value_ref], col)
                               ),
                               style_header={
                                   'backgroundColor': '#0075cf',
                                   'fontWeight': 'bold',
                                   'fontSize': '12pt',
                                   'whiteSpace': 'normal',
                               }, style_data={
                                   'font-family': 'Arial',
                                   'backgroundColor': '#262B3D',
                                   'fontSize': '9pt',
                                   'whiteSpace': 'normal',
                               },
                               style_cell={
                                   'font-family': 'Arial',
                                   'whiteSpace': 'normal',
                                   # 'minWidth': '1vw !important',
                                   # 'backgroundColor': '#262B3D',
                                   'textAlign': 'left',
                                    'height': '25px',
                                    'backgroundColor': '#262B3D'
                               }
                           )]),
                           html.Br(),
                           html.Br(),
                       ], {'max-width': '100%', 'fontSize': '10pt', 'textAlign': 'center'}, {'display': 'none'}

            elif value_type == "AOX":
                df_output, custom = treatment_comparator(df_data, df_output, df_ing, df_aox, df_RMs_color,
                                                         df_RMs_warning, value_ref, value_type, value_edt, value_color,
                                                         value_comparator, formulas, formulas2)

                return [
                           html.Div(style={'padding': '2vw'}, children=[dt.DataTable(
                               columns=[{"name": i, "id": i} for i in df_output.columns],
                               data=df_output.to_dict('records'),
                               export_columns='visible',
                               export_format='xlsx',
                               export_headers='display',
                                filter_action="native",
                                 sort_action='native',
                                 sort_mode='multi',
                               style_table={
                                   'maxHeight': '500px',
                                   'overflowY': 'scroll',
                                   'overflowX': 'scroll'
                               },
                               style_data_conditional=(
                                   data_comparator(formulas[value_ref], col)
                               ),
                               style_header={
                                   'backgroundColor': '#0075cf',
                                   'fontWeight': 'bold',
                                   'fontSize': '12pt',
                                   'whiteSpace': 'normal',
                               }, style_data={
                                   'font-family': 'Arial',
                                   'backgroundColor': '#262B3D',
                                   'fontSize': '9pt',
                                   'whiteSpace': 'normal',
                               },
                               style_cell={
                                   'font-family': 'Arial',
                                   'whiteSpace': 'normal',
                                   # 'minWidth': '1vw !important',
                                   # 'backgroundColor': '#262B3D',
                                   'textAlign': 'left',
                                    'height': '25px',
                                    'backgroundColor': '#262B3D'
                               }
                           )]),
                           html.Br(),
                           html.Br(),
                       ], {'display': 'none'}, {'display': 'none'}

            elif value_type == "RM color":

                df_output, custom = treatment_comparator(df_data, df_output, df_ing, df_aox, df_RMs_color,
                                                         df_RMs_warning, value_ref, value_type, value_edt, value_color,
                                                         value_comparator, formulas, formulas2)

                return [html.Div(style={'padding': '2vw'}, children=[dt.DataTable(
                    columns=[{"name": i, "id": i} for i in df_output.columns],
                    data=df_output.to_dict('records'),
                    export_columns='visible',
                    export_format='xlsx',
                    export_headers='display',
                     filter_action="native",
                      sort_action='native',
                      sort_mode='multi',
                    style_table={
                        'maxHeight': '500px',
                        'overflowY': 'scroll',
                        'overflowX': 'scroll'
                    },
                    style_data_conditional=custom,
                    style_header={
                        'backgroundColor': '#0075cf',
                        'fontWeight': 'bold',
                        'fontSize': '12pt',
                        'whiteSpace': 'normal',
                    }, style_data={
                        'font-family': 'Arial',
                        'backgroundColor': '#262B3D',
                        'fontSize': '9pt',
                        'whiteSpace': 'normal',
                    },
                    style_cell={
                        'font-family': 'Arial',
                        'whiteSpace': 'normal',
                        # 'minWidth': '1vw !important',
                        # 'backgroundColor': '#262B3D',
                        'textAlign': 'left',
                         'height': '25px',
                         'backgroundColor': '#262B3D'
                    }
                )]),
                        html.Br(),
                        html.Br(), ], {'display': 'none'}, {'max-width': '100%', 'fontSize': '10pt',
                                                            'textAlign': 'left', 'margin-left': '45%'}





            elif value_type == "Risk Assessment":

                df_output, custom = treatment_comparator(df_data, df_output, df_ing, df_aox, df_RMs_color,
                                                         df_RMs_warning, value_ref, value_type, value_edt, value_color,
                                                         value_comparator, formulas, formulas2)

                return [
                           html.Div(style={'padding': '2vw'}, children=[dt.DataTable(
                               columns=[{"name": i, "id": i} for i in df_output.columns],
                               data=df_output.to_dict('records'),
                               export_columns='visible',
                               export_format='xlsx',
                               export_headers='display',
                                filter_action="native",
                                 sort_action='native',
                                 sort_mode='multi',
                               style_table={
                                   'maxHeight': '500px',
                                   'overflowY': 'scroll',
                                   'overflowX': 'scroll'
                               },
                               style_data_conditional=(
                                   data_comparator(formulas[value_ref], col)
                               ),
                               style_header={
                                   'backgroundColor': '#0075cf',
                                   'fontWeight': 'bold',
                                   'fontSize': '12pt',
                                   'whiteSpace': 'normal',
                               }, style_data={
                                   'font-family': 'Arial',
                                   'backgroundColor': '#262B3D',
                                   'fontSize': '9pt',
                                   'whiteSpace': 'normal',
                               },
                               style_cell={
                                   'font-family': 'Arial',
                                   'whiteSpace': 'normal',
                                   # 'minWidth': '1vw !important',
                                   # 'backgroundColor': '#262B3D',
                                   'textAlign': 'left',
                                    'height': '25px',
                                    'backgroundColor': '#262B3D'
                               }
                           )]),
                           html.Br(),
                           html.Br(),
                       ], {'display': 'none'}, {'display': 'none'}

            for valeur in df_ing.columns:
                if value_type == valeur:
                    df_output, custom = treatment_comparator(df_data, df_output, df_ing, df_aox, df_RMs_color,
                                                             df_RMs_warning, value_ref, value_type, value_edt,
                                                             value_color, value_comparator, formulas, formulas2)

                    return [
                               html.Div(style={'padding': '2vw'}, children=[dt.DataTable(
                                   columns=[{"name": i, "id": i} for i in df_output.columns],
                                   data=df_output.to_dict('records'),
                                   export_columns='visible',
                                   export_format='xlsx',
                                   export_headers='display',
                                    filter_action="native",
                                     sort_action='native',
                                     sort_mode='multi',
                                   style_table={
                                       'maxHeight': '500px',
                                       'overflowY': 'scroll',
                                       'overflowX': 'scroll'
                                   },
                                   style_data_conditional=(
                                       data_comparator(formulas[value_ref], col)
                                   ),
                                   style_header={
                                       'backgroundColor': '#0075cf',
                                       'fontWeight': 'bold',
                                       'fontSize': '12pt',
                                       'whiteSpace': 'normal',
                                   }, style_data={
                                       'font-family': 'Arial',
                                       'backgroundColor': '#262B3D',
                                       'fontSize': '9pt',
                                       'whiteSpace': 'normal',
                                   },
                                   style_cell={
                                       'font-family': 'Arial',
                                       'whiteSpace': 'normal',
                                       # 'minWidth': '1vw !important',
                                       # 'backgroundColor': '#262B3D',
                                       'textAlign': 'left',
                                        'height': '25px',
                                        'backgroundColor': '#262B3D'
                                   }
                               )]),
                               html.Br(),
                               html.Br(),
                           ], {'display': 'none'}, {'display': 'none'}

        raise PreventUpdate

    ### Part of the function 7 in EDT options: Comparator display edt dosage ###
    @_app.callback(Output('edt-dosage', 'children'),
                   [Input('action-type-comparator', 'value'),
                    Input('action-type-ref', 'value'),
                    Input('action-comparator', 'value')],
                   [State('formulas_name_comparator', 'data')])
    def edt_dosage_update(value_type, value_ref, value_comparator, formulas):
        """
        This function display the dosage for the comparator type "EDT" of the function comparator

        Input :
        - comparator Type
        - formula reference
        - formulas comparation

        Output :
        - Dosage of each formula in EDT

        """
        if value_type == "EDT":

            formulas2 = formulas.copy()
            formulas2.remove(formulas2[value_ref])
            valeur = [formulas[value_ref]] + list(pd.Series(formulas2).iloc[value_comparator])

            children = []
            for i, j in enumerate(valeur):
                new_dropdown = html.Div(children=[
                    html.P(j),
                    dcc.Input(
                        id={'type': 'filter-dropdown', 'index': i},
                        value='10',
                        max='100',
                        min='0'
                    )])
                children.append(new_dropdown)

            return children
        else:

            return []

    ### Part of the function 7 in RMs color table: Comparator display choice data columns ###
    @_app.callback(Output('RMs_color', 'children'),
                   [Input('action-type-comparator', 'value'),
                    Input('action-type-ref', 'value'),
                    Input('action-comparator', 'value')],
                   [State('formulas_name_comparator', 'data')])

    def action_color_update(value_type, value_ref, value_comparator, formulas):
        """
        This function display the color datas for the comparator type "RM color" of the function comparator

        Input :
        - comparator Type
        - formula reference
        - formulas comparation

        Output :
        - Datatable of each formula selected in parameters of RM color information

        """
        if value_type == "RM color":
            children = []
            array = ['Neat oil colors', 'Stability data in edt', 'Color impact thresold', 'replacement']

            for i, j in enumerate(array):
                new_dropdown = html.Div(children=[
                    dcc.Checklist(
                        id={'type': 'action-color-comparator', 'index': i},
                        options=[{'label': j, 'value': i}]
                    )])
                children.append(new_dropdown)

            return children

        else:
            return []

    ### Part of the function 9 : Display RMs color result ###
    @_app.callback(
        Output('results-RMscolor', 'children'),
        [
            Input('df_data_color', 'data')
        ]

    )
    def update_output_RMscolor(df_data_color):
        if df_data_color == {} or df_data_color == []:

            raise PreventUpdate
        else:
            # traitement df_data_color
            df_data = pd.DataFrame(df_data_color)

            array = ['Neat oil colors', 'Stability data in edt', 'Color impact thresold', 'replacement']

            return [
                html.Hr(),
                html.H4(className='what-is', children='Choose array'),
                html.Hr(),
                dcc.Checklist(id='action-color',
                              options=[
                                  {'label': k, 'value': i} for i, k in enumerate(array)
                              ],
                              value=[0, 1, 2, 3],
                              style={'max-width': '100%', 'fontSize': '10pt', 'textAlign': 'left', 'margin-left': '45%'}
                              ),
                html.Hr(),
                html.H4(className='what-is', children='RMs color'),
                html.Hr(),
                html.Div([
                    html.Div(id='datatable-color')
                ]),
                html.Br(),
                html.Br(),
            ]

    ### Part of the function 9 : Display RMs color datatable result ###
    @_app.callback(
        Output('datatable-color', 'children'),
        [
            Input('action-color', 'value')
        ],
        [
            State('df_data_color', 'data')
        ]
    )
    def update_output_RMscolor(value, df_data_color):

        df_data = pd.DataFrame(df_data_color)

        df_data, custom = treatment_RMs_color(df_data, value, "RMs_color")

        return [
            html.Div(style={'padding': '2vw'}, children=[dt.DataTable(
                columns=[{"name": i, "id": i} for i in df_data.columns],
                data=df_data.to_dict('records'),
                export_columns='visible',
                export_format='xlsx',
                export_headers='display',
                filter_action="native",
                 sort_action='native',
                 sort_mode='multi',
                style_table={
                    'maxHeight': '1000px',
                    'overflowY': 'scroll',
                    'overflowX': 'scroll'
                },
                style_data_conditional=custom,
                style_header={
                    'backgroundColor': '#0075cf',
                    'fontWeight': 'bold',
                    'fontSize': '12pt',
                    'whiteSpace': 'normal',
                }, style_data={
                    'font-family': 'Arial',
                    'backgroundColor': '#262B3D',
                    'fontSize': '9pt',
                    'whiteSpace': 'normal',
                },
                style_cell={
                    'font-family': 'Arial',
                    'whiteSpace': 'normal',
                    # 'minWidth': '1vw !important',
                    # 'backgroundColor': '#262B3D',
                    'textAlign': 'left',
                    'height': '25px',
                    'backgroundColor': '#262B3D'
                }
            )]),
            html.Br(),
            html.Br(),
        ]
        # bouton export rms color
        # style data conditional : color = {RGB} et couleur texte pour colonne rgb meme que background

    ### Part of the function 8 : display result data show function ###
    @_app.callback(
        Output('results-data-show', 'children'),
        [
            Input('data-to-show', 'value')
        ],
        [
            State('df_data', 'data'),
            State('missing_IPC', 'data')
        ]
    )
    def update_data_show(cols, df_data,missing):
        """
        This function display some physical parameters or information of each raw material of a formula entered in parameter.

        Input :
        - data-to-show : type of data you want to display

        Output :
        - results-data-show : display datatable of the results of data show function
        """
        if df_data == {}:
            raise PreventUpdate
        else:
            df_data_name=pd.DataFrame(df_data, columns=['Name'])
            df_data_dt = pd.DataFrame(df_data, columns=['dosage'])
            df_data_dt = treatment_data_display(df_data_dt, df_ing)
            cols = ['IPC', 'Name', 'Quantity %'] + [i for i in cols if i in df_data_dt.columns.values]
            df_data_dt = df_data_dt.sort_values(by='Quantity %', ascending=False)
            df_data_dt=IPC_not_found(df_data_dt,df_data_name)
            return [
                html.Hr(),
                html.H4(className='what-is', children='DataBase of Ingredients'),
                html.Hr(),
                html.Div(style={'padding': '2vw'}, children=[dt.DataTable(
                    columns=[{"name": i, "id": i} for i in df_data_dt[cols].columns],
                    data=df_data_dt[cols].to_dict('records'),
                    export_columns='visible',
                    export_format='xlsx',
                    export_headers='display',
                    filter_action="native",
                     sort_action='native',
                     sort_mode='multi',
                    style_data_conditional=(data_bars(df_data_dt[cols], 'Quantity %', '',missing)),
                    style_header={
                        'backgroundColor': '#0075cf',
                        'fontWeight': 'bold',
                        'fontSize': '12pt',
                        'whiteSpace': 'normal',
                    }, style_data={
                        'font-family': 'Arial',
                        'backgroundColor': '#262B3D',
                        'fontSize': '9pt',
                        'whiteSpace': 'normal',
                    },
                    style_cell={
                        'font-family': 'Arial',
                        'whiteSpace': 'normal',
                        # 'minWidth': '1vw !important',
                        # 'backgroundColor': '#262B3D',
                        'textAlign': 'left',
                        'height': '25px',
                        'backgroundColor': '#262B3D'
                    }
                )])]

    ### Part of the function 1 : display result risk assessment ###
    @_app.callback(
        [
            Output('results-risk', 'children'),
            Output('df_data_warn', 'data')
        ],
        [
            Input('data-risk', 'value')
        ],
        [
            State('df_data', 'data'),
            State('missing_IPC', 'data')
        ]
    )
    def update_risk(value, df_data,missing):
        """
        This function display the risk assessment of the formula. We can choose which datatable you want to display.

        Input :
        - data-risk : display a dropdown where you can choose with datatable you want to display

        Output :
        - results-risk : display the datatable you have chosen
        - df_data_warn : data send to another procedure to display the datatable : Warning flags summary
        """
        if df_data == {}:
            raise PreventUpdate
        else:

            df_data_name=pd.DataFrame(df_data, columns=['Name'])
            df_data = pd.DataFrame(df_data, columns=['dosage'])
            df_data_2 = pd.DataFrame(df_data, columns=['dosage'])

            df_non_exploded, df_exploded, df_warning = treatment_risk_assessment(df_data, df_data_2, df_ing,
                                                                                 df_RMs_warning, df_solvent_nat_synt,
                                                                                 df_explosion_nat, df_explosion_synt,
                                                                                 "risk_assessment")

            df_non_exploded=IPC_not_found(df_non_exploded,df_data_name)


            datatable_1 = {'display': 'none'}
            datatable_2 = {'display': 'none'}
            datatable_3 = {'display': 'none'}
            style_display = {'display': 'none'}
            if 'Non exploded formula risk assessment ' in value:
                datatable_1 = {'display': 'inline'}
            if 'Exploded formula risk assessment' in value:
                datatable_2 = {'display': 'inline'}
            if 'Warning flags summary' in value:
                datatable_3 = {'display': 'inline'}
                style_display = {'display': 'inline'}

            return [
                       html.Div(id='data_1_title', style=datatable_1, children=[
                           html.Hr(),
                           html.H4(className='what-is', children='Non exploded formula risk assessment'),
                           html.Hr()
                       ]),
                       html.Div(id='data_1', style=datatable_1, children=[dt.DataTable(
                           columns=[{"name": i, "id": i} for i in df_non_exploded.columns],
                           data=df_non_exploded.to_dict('records'),
                           export_columns='visible',
                           export_format='xlsx',
                           export_headers='display',
                             filter_action="native",
                             sort_action='native',
                             sort_mode='multi',
                           style_data_conditional=(
                                   data_bars(df_non_exploded, 'Quantity %', 'risk assessment',missing) +
                                   data_bars(df_non_exploded, 'PURE QUANTITY %', '',[])
                           ),
                           style_header={
                               'backgroundColor': '#0075cf',
                               'fontWeight': 'bold',
                               'fontSize': '12pt',
                               'whiteSpace': 'normal',
                           }, style_data={
                               'font-family': 'Arial',
                               'backgroundColor': '#262B3D',
                               'fontSize': '9pt',
                               'whiteSpace': 'pre-line',
                           },
                           style_cell={
                               'font-family': 'Arial',
                               'whiteSpace': 'normal',
                               # 'minWidth': '1vw !important',
                               'backgroundColor': '#262B3D',
                               'textAlign': 'left',
                               'height': '25px',
                           }
                       )]),
                       html.Div(id='data_2_title', style=datatable_2, children=[
                           html.Hr(),
                           html.H4(className='what-is', children='Exploded formula risk assessment'),
                           html.Hr()
                       ]),
                       html.Div(id='data_2', style=datatable_2, children=[dt.DataTable(
                           columns=[{"name": i, "id": i} for i in df_exploded.columns],
                           data=df_exploded.to_dict('records'),
                           export_columns='visible',
                           export_format='xlsx',
                           export_headers='display',
                           filter_action="native",
                           sort_action='native',
                           sort_mode='multi',
                           style_data_conditional=(
                               data_bars(df_exploded, 'Quantity', 'Exploded',missing)+
                               line_odd('rgb(0, 79, 153)')
                           ),
                           style_header={
                               'backgroundColor': '#0075cf',
                               'fontWeight': 'bold',
                               'fontSize': '12pt',
                               'whiteSpace': 'normal',
                           }, style_data={
                               'font-family': 'Arial',
                               'backgroundColor': '#262B3D',
                               'fontSize': '9pt',
                               'whiteSpace': 'normal',
                           },
                           style_cell={
                               'font-family': 'Arial',
                               'whiteSpace': 'normal',
                               # 'minWidth': '1vw !important',
                                'backgroundColor': '#262B3D',
                               'textAlign': 'left',
                               'height': '25px',
                           }
                       ),
                           html.Br(),
                           html.Br(),
                           html.Br(),
                           html.Br(),
                           html.Br(),
                           html.Br()
                       ]),

                       html.Div(id='data_3_title', style=datatable_3, children=[
                           html.Hr(),
                           html.H4(className='what-is', children='Warning flags summary'),
                           html.Hr()
                       ]),
                       html.Div(id='data_cols', style=style_display, children=[dcc.Dropdown(id='data_cols_warn',
                                                                                            options=[
                                                                                                {'label': i, 'value': i}
                                                                                                for i in
                                                                                                df_warning.columns if
                                                                                                i not in ['IPC', 'Name',
                                                                                                          'dosage']],
                                                                                            value=[],
                                                                                            persistence=True,
                                                                                            multi=True,
                                                                                            placeholder="Please select the type of unstability to get more information"

                                                                                            ), ],

                                ),
                       html.Br(),
                       html.Div(id='data_3', style=datatable_3)
                   ], df_warning.to_dict()

    ### Part of the function 1 : display result datatable of warning flags ###
    @_app.callback(
        Output('data_3', 'children'),
        [
            Input('data_cols_warn', 'value')
        ],
        [
            State('df_data_warn', 'data'),
            State('missing_IPC', 'data')
        ]
    )
    def update_data_warning(value, df_data,missing):
        """
        This procedure display the datatable Warning flags summary with the risk selectionned in parameters (data_cols_warn).
        """
        if df_data == {}:
            raise PreventUpdate
        else:
            df_warning = pd.DataFrame(df_data)

            return [
                dt.DataTable(
                    columns=[{"name": i, "id": i} for i in df_warning[['IPC', 'Name', 'Quantity %'] + value]],
                    data=df_warning.to_dict('records'),
                    export_columns='visible',
                    export_format='xlsx',
                    export_headers='display',
                      filter_action="native",
                      sort_action='native',
                      sort_mode='multi',
                    style_data_conditional=(
                        data_bars(df_warning, 'Quantity %', 'warning',missing)
                    ),
                    style_header={
                        'backgroundColor': '#0075cf',
                        'fontWeight': 'bold',
                        'fontSize': '12pt',
                        'whiteSpace': 'normal',
                    }, style_data={
                        'font-family': 'Arial',
                        'backgroundColor': '#262B3D',
                        'fontSize': '9pt',
                        'whiteSpace': 'normal',
                    },
                    style_cell={
                        'font-family': 'Arial',
                        'whiteSpace': 'normal',
                        # 'minWidth': '1vw !important',
                         'backgroundColor': '#262B3D',
                        'textAlign': 'left',
                        'height': '25px'
                    }
                )]


    ### Part of the function 4 : display solvent diagram ###
    @_app.callback(

        Output('solvent_diagram', 'figure'),

        [
            Input('action-type-solvent', 'value'),
            Input('solvent_MP', 'data'),
            Input('solvent_direct', 'data')
        ]

    )
    def solvent_diagram(value, solvent_MP, solvent_direct):
        """
        This function display the diagram of the solvent decomposition function.

        Input :
        - action-type-solvent : solvent chosen for display diagram
        - solvent_MP : data used for datatable "Parts of solvent in RM"
        - solvent_direct : data used for datatable "Solvent direct or indirect"
        """
        if value == "Solvents partition in formula":

            solvent_direct_dt = pd.DataFrame(solvent_direct)
            solvent_direct_dt = solvent_direct_dt.replace(0, np.nan).dropna(axis=1, how="all")

            if solvent_direct_dt.empty == False:

                data = {
                    'values': solvent_direct_dt['% TOTAL'],
                    'labels': solvent_direct_dt['SOLVENT'],
                    'type': 'pie',
                    'marker': {'line': {'width': '1', 'color': 'rgb(255, 255, 255)'}},
                    'textfont': {'color': 'rgb(255, 255, 255)'}
                }
                layout = {
                    'height': '200px',
                    'max-width': '50%',
                    'margin-left': '25%',
                    'paper_bgcolor': 'rgba(31,33,50,1)',
                    'plot_bgcolor': 'rgba(31,33,50,1)',
                    'legend': {'font':
                                   {'color': 'rgb(255, 255, 255)'},
                               'borderwidth': '1',
                               'bordercolor': 'rgb(255, 255, 255)'
                               },

                }

                return {
                    'data': [data],
                    'layout': layout
                }
            else:
                raise PreventUpdate
        else:
            for solvent in solvent_MP:
                if value == solvent:
                    solvent_MP_dt = pd.DataFrame(solvent_MP)
                    solvent_MP_dt = solvent_MP_dt.replace('', np.nan)

                    solvent_MP_dt[solvent] = solvent_MP_dt[solvent].dropna(axis=0, how="all")

                    if solvent_MP_dt.empty == False:
                        data = {
                            'values': solvent_MP_dt[solvent],
                            'labels': solvent_MP_dt['Name'],
                            'type': 'pie',
                            'marker': {'line': {'width': '1', 'color': 'rgb(255, 255, 255)'}},
                            'textfont': {'color': 'rgb(255, 255, 255)'}
                        }
                        layout = {
                            'height': '200px',
                            'max-width': '80%',
                            'margin-left': '10%',
                            'paper_bgcolor': 'rgba(31,33,50,1)',
                            'plot_bgcolor': 'rgba(31,33,50,1)',
                            'legend': {'font':
                                           {'color': 'rgb(255, 255, 255)'},
                                       'borderwidth': '1',
                                       'bordercolor': 'rgb(255, 255, 255)'
                                       },
                        }
                        return {
                            'data': [data],
                            'layout': layout
                        }

                    else:
                        raise PreventUpdate

    ### Part of the function 5 : display result Rms tracking function ###
    @_app.callback(
        [
            Output('results-RMs-tracking', 'children'),
            Output('df_data_identification', 'data')
        ],
        [
            Input('display_all_formula', 'on')
        ],
        [
            State('df_data', 'data')
        ]

    )
    def update_RMs_tracking(value, df_data):
        """
        This prodecure display the dropdown of raw materials tracking.
        We can choose to display all raw materials of formula or only raw materials concerned.

        Input :
        - display_all_formula : Off to display only the raw materials concerned or On to display all the raw materials.

        Output :
        - The children of the function RMs Tracking
        - The formula data for function RMs Tracking
        """
        if df_data == {}:
            raise PreventUpdate
        else:
            df_data = pd.DataFrame(df_data, columns=["dosage"])

            df_data_identification = treatment_RMs_tracking(df_data, df_ing, df_indole, df_linalyl, df_methyl,
                                                            df_vanillin, value)

            return [
                       html.Hr(),
                       html.H4(className='what-is', children='Raw Materials tracking'),
                       html.Hr(),
                       html.Div(id='data_RMs', children=[dcc.Dropdown(id='data_RMs_choice',
                                                                      options=[{'label': i, 'value': i} for i in
                                                                               ['indole', 'linalyl acetate',
                                                                                'methyl anthranilate', 'vanillin']],
                                                                      value=['indole', 'linalyl acetate',
                                                                             'methyl anthranilate', 'vanillin'],
                                                                      persistence=True,
                                                                      multi=True

                                                                      ), ],

                                ),
                       html.Br(),
                       html.Div(id='data_idenfication', style={'padding': '2vw'})
                   ], df_data_identification.to_dict()

    ### Part of the function 5 : display result datatable ###
    @_app.callback(

        Output('data_idenfication', 'children'),

        [
            Input('data_RMs_choice', 'value'),
            Input('df_data_identification', 'data')
        ],
        [
            State('missing_IPC', 'data')
        ]

    )
    def update_data_identification(value, df_data,missing):
        """
        This procedure display the datatable of the function RMs tracking

        Input :
        - data_RMs_choice : The raw materials we want to track
        - df_data_identification : The formula data

        Output :
        - datatable of result
        """
        if df_data == {}:
            raise PreventUpdate
        else:
            df_identification = pd.DataFrame(df_data)

            df_id = []
            if value != []:
                for valeur in value:
                    df_id.append(df_identification[df_identification['Identification'] == valeur])
            df_id.append(df_identification[df_identification['Identification'].isnull()])
            df_id_2 = pd.concat(df_id, ignore_index=True)
            df_id_2 = df_id_2.sort_values(by='Quantity %', ascending=False)
            df_id_2['Quantity %'] = round(df_id_2['Quantity %'], 4)

            # TOTAUX
            indole_total = round(
                df_identification[df_identification['Identification'] == "indole"]["Dosage in raw material"].sum(), 4)
            linalyl_total = round(df_identification[df_identification['Identification'] == "linalyl acetate"][
                                      "Dosage in raw material"].sum(), 4)
            methyl_total = round(df_identification[df_identification['Identification'] == "methyl anthranilate"][
                                     "Dosage in raw material"].sum(), 4)
            vanillin_total = round(
                df_identification[df_identification['Identification'] == "vanillin"]["Dosage in raw material"].sum(), 4)

            # df_id_2=df_id_2.drop(columns=['NAME','% GC'],axis=1)
            return [
                html.Div(style={'textAlign': 'center'}, children=[
                    html.Div(style={'width': '200px', 'display': 'inline-block', 'textAlign': 'center'}, children=[
                        html.H4(children='Indole'),
                        html.H4(style={"border": "2px #D2CC18 solid"}, children=indole_total)
                    ]),
                    html.Div(
                        style={'margin-left': '5%', 'display': 'inline-block', 'width': '200px', 'textAlign': 'center'},
                        children=[
                            html.H4(children='Linalyl Acetate'),
                            html.H4(style={"border": "2px #39C214 solid"}, children=linalyl_total)
                        ]),
                    html.Div(
                        style={'margin-left': '5%', 'display': 'inline-block', 'width': '200px', 'textAlign': 'center'},
                        children=[
                            html.H4(children='Methyl Anthranilate'),
                            html.H4(style={"border": "2px #DD0F0C solid"}, children=methyl_total)
                        ]),
                    html.Div(
                        style={'margin-left': '5%', 'display': 'inline-block', 'width': '200px', 'textAlign': 'center'},
                        children=[
                            html.H4(children='Vanillin'),
                            html.H4(style={"border": "2px #19C9EC solid"}, children=vanillin_total)
                        ])
                ]),
                dt.DataTable(
                    columns=[{"name": i, "id": i} for i in df_id_2.columns],
                    data=df_id_2.to_dict('records'),
                    export_columns='visible',
                    export_format='xlsx',
                    export_headers='display',
                    filter_action="native",
                     sort_action='native',
                     sort_mode='multi',
                    style_data_conditional=(data_bars(df_id_2, 'Quantity %', 'identification',missing)),
                    style_header={
                        'backgroundColor': '#0075cf',
                        'fontWeight': 'bold',
                        'fontSize': '12pt',
                        'whiteSpace': 'normal',
                    }, style_data={
                        'font-family': 'Arial',
                        'backgroundColor': '#262B3D',
                        'fontSize': '9pt',
                        'whiteSpace': 'normal',
                    },
                    style_cell={
                        'font-family': 'Arial',
                        'whiteSpace': 'normal',
                        'textAlign': 'left',
                        'height': '25px',
                        'backgroundColor': '#262B3D'
                    }
                )]

    ### Part of the function 6 : display additif in function hydro alcoholic solution ###
    @_app.callback(
        [
            Output('additif1', 'style'),
            Output('additif2', 'style'),
            Output('additif3', 'style'),
            Output('additif4', 'style'),
        ],
        [
            Input('input8', 'value')
        ]
    )
    def additif(value):
        if 'BHT' in value:
            style1 = {'display': 'block', 'textAlign': 'center'}
        else:
            style1 = {"display": 'none'}

        if 'Covabsorb' in value:
            style2 = {'display': 'block', 'textAlign': 'center'}
        else:
            style2 = {"display": 'none'}

        if 'Parsol 1789' in value:
            style3 = {'display': 'block', 'textAlign': 'center'}
        else:
            style3 = {"display": 'none'}

        if 'Tinogard Q' in value:
            style4 = {'display': 'block', 'textAlign': 'center'}
        else:
            style4 = {"display": 'none'}

        return style1, style2, style3, style4

    ### Part of the function 6 : generate word file ###
    @_app.callback(
        [
            Output('generate-file', 'style'),
            Output('confirm', 'displayed')
        ],
        [
            Input('submit-hydro', 'n_clicks')
        ],
        [
            State('df_data', 'data'),
            State('action-hydro', 'value'),
            State('input1', 'value'),
            State('input2', 'value'),
            State('input3', 'value'),
            State('input4', 'value'),
            State('input5', 'value'),
            State('input6', 'value'),
            State('input7', 'value'),
            State('input8', 'value'),
            State('input9', 'value'),
            State('input10', 'value'),
            State('input11', 'value'),
            State('input12', 'value')
        ]

    )
    def update_hydro_alcoholic(n_clicks, df_data, value, value1, value2, value3, value4, value5, value6, value7, value8,
                               value9, value10, value11, value12):
        if df_data == {} or n_clicks == None:
            raise PreventUpdate
        else:
            # 'Alcoholic solution','Alcoholic solution P20/P80','Alcoholic solution SMA'
            style, display = treatment_hydro_alcoholic(df_data, df_methyl, df_methyl_direct, value, value1, value2,
                                                       value3, value4, value5, value6, value7, value8, value9, value10,
                                                       value11, value12)

            return style, display

    @_app.callback(
        Output('results-ester', 'children'),
        [
            Input('ester-calculate', 'n_clicks'),
            Input('ester-classes', 'n_clicks')
        ],
        [
            State('df_data', 'data'),
            State('list-ester', 'value'),
            State('ester-edt', 'value'),
            State('ester-temp', 'value'),
            State('ester-time', 'value'),
            State('ester-ph', 'value'),
            State('missing_IPC', 'data')
        ]
    )
    def ester_classes(calculate_click, classes_click, df_data, list_ester, edt_value, temp_value, time_value, ph_value,missing):
        if df_data == {} or (calculate_click == None and classes_click == None):
            raise PreventUpdate
        else:
            df_data = pd.DataFrame(df_data, columns=['dosage'])
            if int(calculate_click) != callbacks_vars.n_clicks[1] and (
                    list_ester != None and edt_value != None and temp_value != None and time_value != None and ph_value != None):
                callbacks_vars.update_n_clicks(calculate_click, 1)
                calculate_click = 0

                # treatment calculate
                residual_ester, deteriorate_ester = treatment_ester_calculate(df_data, df_ing, df_ester_half_life,
                                                                              list_ester, edt_value, temp_value,
                                                                              time_value, ph_value)

                return [
                    html.Hr(),
                    html.H4(className='what-is', children='Ester Hydrolysis'),
                    html.Hr(),
                    html.Div(style={'textAlign': 'center'}, children=[
                        html.Div(style={'width': '300px', 'display': 'inline-block', 'textAlign': 'center'}, children=[
                            html.H4(children='Residual Ester'),
                            html.H4(style={"border": "2px white solid"}, children=round(residual_ester, 2))
                        ]),
                        html.Div(style={'margin-left': '5%', 'display': 'inline-block', 'width': '300px',
                                        'textAlign': 'center'}, children=[
                            html.H4(children='Deteriorate Ester'),
                            html.H4(style={"border": "2px white solid"}, children=round(deteriorate_ester, 2))
                        ]),
                        html.Div(style={'margin-left': '5%', 'display': 'inline-block', 'width': '300px',
                                        'textAlign': 'center'}, children=[
                            html.H4(children='Total Ester'),
                            html.H4(style={"border": "2px white solid"},
                                    children=round(residual_ester + deteriorate_ester, 2))
                        ]),
                    ])
                ]
            elif int(classes_click) != callbacks_vars.n_clicks[2]:

                callbacks_vars.update_n_clicks(classes_click, 2)
                classes_click = 0

                # treatment ester classes

                df_ester_data, indirect_ester_class1, indirect_ester_class2, indirect_ester_class3, direct_ester_class1, direct_ester_class2, direct_ester_class3, ester_I, ester_II, ester_III, ester_I_II = treatment_Ester_Classes(
                    df_data, df_ing, df_ester, df_explosion_nat, edt_value)

                return [
                    html.Hr(),
                    html.H4(className='what-is', children='Ester Classes'),
                    html.Hr(),
                    html.Div(style={'textAlign': 'center'}, children=[
                        html.Div(style={'width': '150px', 'display': 'inline-block', 'textAlign': 'center'}, children=[
                            html.H4(children='Ester I'),
                            html.H4(style={"border": "2px #D01C1C solid"}, children=ester_I)
                        ]),
                        html.Div(style={'margin-left': '5%', 'display': 'inline-block', 'width': '150px',
                                        'textAlign': 'center'}, children=[
                            html.H4(children='Ester II'),
                            html.H4(style={"border": "2px #D09C1C solid"}, children=ester_II)
                        ]),
                        html.Div(style={'margin-left': '5%', 'display': 'inline-block', 'width': '150px',
                                        'textAlign': 'center'}, children=[
                            html.H4(children='Ester III'),
                            html.H4(style={"border": "2px #34C00B solid"}, children=ester_III)
                        ]),
                        html.Div(style={'margin-left': '5%', 'display': 'inline-block', 'width': '150px',
                                        'textAlign': 'center'}, children=[
                            html.H4(children='Ester I + II'),
                            html.H4(style={"border": "2px #DC7310 solid"}, children=ester_I_II)
                        ]),
                    ]),
                    html.Div(style={'padding': '2vw'}, children=[dt.DataTable(
                        columns=[{"name": i, "id": i} for i in df_ester_data.columns],
                        data=df_ester_data.to_dict('records'),
                        export_columns='visible',
                        export_format='xlsx',
                        export_headers='display',
                        filter_action="native",
                         sort_action='native',
                         sort_mode='multi',
                        style_data_conditional=(
                            data_ester_class(indirect_ester_class1, indirect_ester_class2, indirect_ester_class3,
                                             direct_ester_class1, direct_ester_class2, direct_ester_class3,missing)
                        ),
                        style_header={
                            'backgroundColor': '#0075cf',
                            'fontWeight': 'bold',
                            'fontSize': '12pt',
                            'whiteSpace': 'normal',
                        }, style_data={
                            'font-family': 'Arial',
                            'backgroundColor': '#262B3D',
                            'fontSize': '9pt',
                            'whiteSpace': 'normal',
                        },
                        style_cell={
                            'font-family': 'Arial',
                            'whiteSpace': 'normal',
                            'textAlign': 'left',
                           'height': '25px',
                           'backgroundColor': '#262B3D'
                        }
                    )])]

            else:
                raise PreventUpdate

    # Analyse type : return "" with uplaod file
    @_app.callback(
        Output('action-type', 'value'),
        [
            Input('stash_upload_computer', 'data')
        ]
    )
    def action_type_value(data):
        return ""


# Header colors
def header_colors():
    return {'bg_color': '#262B3D', 'font_color': '#FFF', 'light_logo': True}


# only declare app/server if the file is being run directly
if 'DEMO_STANDALONE' not in os.environ:
    app = run_standalone_app(server,layout, callbacks, header_colors, __file__)
    server = app.server

if __name__ == '__main__':
    app.run_server(debug=True, port=8050)
