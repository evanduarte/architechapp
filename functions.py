#####################################################################################################################################
######################################################### Import packages ###########################################################
#####################################################################################################################################
import base64
import io
import json
import os, sys, subprocess
import dash
import pandas as pd
import math
import numpy as np
import dash_core_components as dcc
from dash.dependencies import Input, Output, State, MATCH, ALL
import dash_html_components as html
import dash_table as dt
import dash_daq as daq
from dash.exceptions import PreventUpdate
from flask import Flask, send_from_directory
from bs4 import BeautifulSoup
from decimal import Decimal
import plotly.graph_objs as go
from datetime import datetime
from datetime import date
from time import strftime
from docx import Document
from docx.shared import Inches
from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.shared import Pt
from docx.enum.text import WD_LINE_SPACING
from docx.enum.style import WD_STYLE_TYPE
from docx.shared import RGBColor


#####################################################################################################################################
######################################################### Some functions ############################################################
#####################################################################################################################################


def IPC_not_found(df_result,df_data):

    df_data=df_data.rename_axis('IPC').reset_index()
    df_result['IPC']=df_result['IPC'].astype(object)

    df_result['Name']=df_result['Name'].fillna(df_data['Name'])


    return df_result

def open_file(filename):
    if sys.platform == "win32":
        os.startfile(filename)
    else:
        opener ="open" if sys.platform == "darwin" else "xdg-open"
        subprocess.call([opener, filename])

def df_to_table(df):
    df = df.round(3)
    return html.Table(
        # Header
        [html.Tr([html.Th(col + ' ') for col in df.columns])] +

        # Body
        [
            html.Tr(
                [
                    html.Td(str(df.iloc[i][col]) + '  ')
                    for col in df.columns
                ]
            )
            for i in range(len(df))
        ], style={'border-spacing': '1vw 0px'}
    )


def read_excel_xml(file):
    soup = BeautifulSoup(file, 'xml')
    workbook = []
    for sheet in soup.findAll('Worksheet'):
        sheet_as_list = []
        for row in sheet.findAll('Row'):
            row_as_list = []
            for cell in row.findAll('Cell'):
                try:
                    row_as_list.append(cell.Data.text)
                except:
                    row_as_list.append(np.nan)
            sheet_as_list.append(row_as_list)
        workbook.append(sheet_as_list)
    df = pd.DataFrame(workbook[0])
    df.columns = df.iloc[0]
    df = df.drop(df.index[0])
    name = ''
    if ('FormulaName' in df):
        name += df['FormulaName'].iloc[0] + ' '
        df = df.drop(['FormulaName'], axis=1)
    if ('FormulaExperimentID' in df):
        name += df['FormulaExperimentID'].iloc[0]
        df = df.drop(['FormulaExperimentID'], axis=1)
    name = name.lstrip().rstrip()
    return df, name


def make_ipc(x):
    try:
        x = int(x)
    except:
        pass
    return '0' * (max(0, 8 - len(str(x)))) + str(x)


def solvent_simple(solvent):
    if "/" in solvent:
        return False
    else:
        return True

### Some style data conditional ###
def line_odd(color):
    styles=[]
    styles.append(
        {
            'if': {'row_index': 'odd'},
            'backgroundColor': color
        }
    )
    return styles

def data_bars_for_AOX(df, column, aox_in_direct,missing):
    n_bins = 100
    bounds = [i * (1.0 / n_bins) for i in range(n_bins + 1)]
    ranges = [
        ((df[column].max() - df[column].min()) * i) + df[column].min()
        for i in bounds
    ]
    styles = []
    for i in range(1, len(bounds)):
        min_bound = ranges[i - 1]
        max_bound = ranges[i]
        max_bound_percentage = bounds[i] * 100
        styles.append({
            'if': {
                'filter_query': (
                        '{{{column}}} >= {min_bound}' +
                        (' && {{{column}}} < {max_bound}' if (i < len(bounds) - 1) else '')
                ).format(column=column, min_bound=min_bound, max_bound=max_bound),
                'column_id': column
            },
            'background': (
                """
                    linear-gradient(90deg,
                    #0074D9 0%,
                    #0074D9 {max_bound_percentage}%,
                    #262B3D {max_bound_percentage}%,
                    #262B3D 100%)
                """.format(max_bound_percentage=max_bound_percentage)
            ),
            'paddingBottom': 2,
            'paddingTop': 2
        })

    for value in aox_in_direct:
        styles.append(
            {
                'if': {
                    'column_id': 'Name',
                    'filter_query': '{IPC} eq ' + str(value)
                },
                'backgroundColor': '#3D9970',
                'color': 'white',
            }
        )

    styles.append({'if': {
        'column_id': 'AOX',
        'filter_query': '{AOX} != ""'
    },
        'backgroundColor': '#d97b09',
        'color': 'white', })

    for value in missing:
        styles.append(
            {
                'if': {
                    'filter_query': '{IPC} eq ' + str(value)
                },
                'backgroundColor': '#858385',
                'color': 'white',
            }
        )
    return styles


def data_bars(df, column, function,missing):
    n_bins = 100
    bounds = [i * (1.0 / n_bins) for i in range(n_bins + 1)]
    ranges = [
        ((df[column].max() - df[column].min()) * i) + df[column].min()
        for i in bounds
    ]
    styles = []
    for i in range(1, len(bounds)):
        min_bound = ranges[i - 1]
        max_bound = ranges[i]
        max_bound_percentage = bounds[i] * 100
        styles.append({
            'if': {
                'filter_query': (
                        '{{{column}}} >= {min_bound}' +
                        (' && {{{column}}} < {max_bound}' if (i < len(bounds) - 1) else '')
                ).format(column=column, min_bound=min_bound, max_bound=max_bound),
                'column_id': column
            },
            'background': (
                """
                    linear-gradient(90deg,
                    #0074D9 0%,
                    #0074D9 {max_bound_percentage}%,
                    #262B3D {max_bound_percentage}%,
                    #262B3D 100%)
                """.format(max_bound_percentage=max_bound_percentage)
            ),
            'paddingBottom': 2,
            'paddingTop': 2
        })

    if function == 'Solvent':
        styles.append({'if': {
            'column_id': 'Name',
            'filter_query': '{Solvent} contains {dosage}'
        },
            'backgroundColor': '#3D9970',
            'color': 'white', })

        styles.append({'if': {
            'column_id': 'Solvent',
            'filter_query': '{Solvent} != ""'
        },
            'backgroundColor': '#d97b09',
            'color': 'white', })

    if function == 'risk assessment':
        # styles.append({
        #     'if':{
        #     'column_id':'Dilution',
        #     },
        #     'backgroundColor': '#262B3D',
        #     'color': 'white',
        # })
        styles.append({
            'if': {
                'column_id': 'Risk Assessment',
                'filter_query': '{Risk Assessment} != ""'
            },
            'backgroundColor': '#d97b09',
            'color': 'white',
        })
        styles.append({
            'if': {
                'column_id': 'Dilution',
            },
            'backgroundColor': '#232838',
            'color': 'white',
        })

    if function == 'nat/synt':
        styles.append({
            'if': {
                'column_id': 'Name',
                'filter_query': '{Nat/Synth} eq "Natural"'
            },
            'backgroundColor': '#3D9970',
            'color': 'white',
        })

    if function == "warning":
        styles.append({
            'if': {
                'column_id': 'COLORATION ',
                'filter_query': '{COLORATION } != ""'
            },
            'backgroundColor': '#d97b09',
            'color': 'white',
        })
        styles.append({
            'if': {
                'column_id': 'DYES ',
                'filter_query': '{DYES } != ""'
            },
            'backgroundColor': '#d97b09',
            'color': 'white',
        })
        styles.append({
            'if': {
                'column_id': 'ODOR ',
                'filter_query': '{ODOR } != ""'
            },
            'backgroundColor': '#d97b09',
            'color': 'white',
        })
        styles.append({
            'if': {
                'column_id': 'METHANOL WARNING',
                'filter_query': '{METHANOL WARNING} != ""'
            },
            'backgroundColor': '#d97b09',
            'color': 'white',
        })
        styles.append({
            'if': {
                'column_id': 'REACTIVITY WITH HEAVY METALS',
                'filter_query': '{REACTIVITY WITH HEAVY METALS} != ""'
            },
            'backgroundColor': '#d97b09',
            'color': 'white',
        })
        styles.append({
            'if': {
                'column_id': 'CAN GENERATE PARTICLES',
                'filter_query': '{CAN GENERATE PARTICLES} != ""'
            },
            'backgroundColor': '#d97b09',
            'color': 'white',
        })
        styles.append({
            'if': {
                'column_id': 'SOLUBILITY',
                'filter_query': '{SOLUBILITY} != ""'
            },
            'backgroundColor': '#d97b09',
            'color': 'white',
        })
        styles.append({
            'if': {
                'column_id': '% GC',
                'filter_query': '{% GC} != ""'
            },
            'backgroundColor': '#d97b09',
            'color': 'white',
        })

    if function == 'identification':
        styles.append({
            'if': {
                'column_id': 'Name',
                'filter_query': '{Identification} eq "indole"'
            },
            'backgroundColor': '#D2CC18',
            'color': 'white',
        })
        styles.append({
            'if': {
                'column_id': 'Name',
                'filter_query': '{Identification} eq "linalyl acetate"'
            },
            'backgroundColor': '#39C214',
            'color': 'white',
        })
        styles.append({
            'if': {
                'column_id': 'Name',
                'filter_query': '{Identification} eq "methyl anthranilate"'
            },
            'backgroundColor': '#DD0F0C',
            'color': 'white',
        })
        styles.append({
            'if': {
                'column_id': 'Name',
                'filter_query': '{Identification} eq "vanillin"'
            },
            'backgroundColor': '#19C9EC',
            'color': 'white',
        })

    for value in missing:
        styles.append(
            {
                'if': {
                    'filter_query': '{IPC} eq ' + str(value)
                },
                'backgroundColor': '#858385',
                'color': 'white',
            }
        )
    return styles


def data_comparator(ref, col):
    styles = []
    styles.append(
        {
            'if': {
                'column_id': ref,
                'filter_query': f'{{{ref}}} != ""'
            },
            'backgroundColor': '#E84509',
            'color': 'white',
        }

    )
    styles.append(
        {
            'if': {
                'column_id': 'Risk Assessment',
                'filter_query': '{Risk Assessment} != ""'
            },
            'backgroundColor': '#DA0918',
            'color': 'white',
        }
    )
    styles.append(
        {
            'if': {
                'column_id': 'AOX',
                'filter_query': '{AOX} != ""'
            },
            'backgroundColor': '#DA0918',
            'color': 'white',
        }

    )
    for i in col:
        styles.append(
            {
                'if': {
                    'column_id': i,
                    'filter_query': f'{{{i}}} != ""'
                },
                'backgroundColor': '#E84509',
                'color': 'white',
            }
        )
    return styles


def data_schiff_base(class1_indirect, class1_direct, class2_indirect, class2_direct, class3_indirect, class3_direct,
                     methyl_indirect, methyl_direct,missing):
    styles = []
    for value in class1_indirect:
        styles.append(
            {
                'if': {
                    'column_id': 'Name',
                    'filter_query': '{IPC} eq ' + str(value)
                },
                'backgroundColor': '#DC800C',
                'color': 'white',
            }
        )
        styles.append(
            {
                'if': {
                    'column_id': 'IPC',
                    'filter_query': '{IPC} eq ' + str(value)
                },
                'backgroundColor': '#1f2132',
                'color': '#1f2132',
            }
        )
    for value in class1_direct:
        styles.append(
            {
                'if': {
                    'filter_query': '{IPC} eq ' + str(value)
                },
                'backgroundColor': '#DC800C',
                'color': 'white',
            }
        )
    for value in class2_indirect:
        styles.append(
            {
                'if': {
                    'column_id': 'Name',
                    'filter_query': '{IPC} eq ' + str(value)
                },
                'backgroundColor': '#D3BE11',
                'color': 'white',
            }
        )
        styles.append(
            {
                'if': {
                    'column_id': 'IPC',
                    'filter_query': '{IPC} eq ' + str(value)
                },
                'backgroundColor': '#1f2132',
                'color': '1f2132',
            }
        )
    for value in class2_direct:
        styles.append(
            {
                'if': {
                    'filter_query': '{IPC} eq ' + str(value)
                },
                'backgroundColor': '#D3BE11',
                'color': 'white',
            }
        )
    for value in class3_indirect:
        styles.append(
            {
                'if': {
                    'column_id': 'Name',
                    'filter_query': '{IPC} eq ' + str(value)
                },
                'backgroundColor': '#11D34C',
                'color': 'white',
            }
        )
        styles.append(
            {
                'if': {
                    'column_id': 'IPC',
                    'filter_query': '{IPC} eq ' + str(value)
                },
                'backgroundColor': '#1f2132',
                'color': '1f2132',
            }
        )
    for value in class3_direct:
        styles.append(
            {
                'if': {
                    'filter_query': '{IPC} eq ' + str(value)
                },
                'backgroundColor': '#11D34C',
                'color': 'white',
            }
        )
    for value in methyl_indirect:
        styles.append(
            {
                'if': {
                    'filter_query': '{IPC} eq ' + str(value)
                },
                'backgroundColor': '#1175D3',
                'color': 'white',
            }
        )
        styles.append(
            {
                'if': {
                    'column_id': 'IPC',
                    'filter_query': '{IPC} eq ' + str(value)
                },
                'backgroundColor': '#1f2132',
                'color': '1f2132',
            }
        )
    for value in methyl_direct:
        styles.append(
            {
                'if': {
                    'filter_query': '{IPC} eq ' + str(value)
                },
                'backgroundColor': '#D311AA',
                'color': 'white',
            }
        )
    for value in missing:
        styles.append(
            {
                'if': {
                    'filter_query': '{IPC} eq ' + str(value)
                },
                'backgroundColor': '#858385',
                'color': 'white',
            }
        )
    return styles


def data_ester_class(indirect_ester_class1, indirect_ester_class2, indirect_ester_class3, direct_ester_class1,
                     direct_ester_class2, direct_ester_class3,missing):
    styles = []
    for value in indirect_ester_class1:
        styles.append(
            {
                'if': {
                    'column_id': 'Name',
                    'filter_query': '{Name} eq "' + str(value) + '"'
                },
                'backgroundColor': '#D01C1C',
                'color': 'white',
            }
        )
    for value in indirect_ester_class2:
        styles.append(
            {
                'if': {
                    'column_id': 'Name',
                    'filter_query': '{Name} eq "' + str(value) + '"'
                },
                'backgroundColor': '#D09C1C',
                'color': 'white',
            }
        )
    for value in indirect_ester_class3:
        styles.append(
            {
                'if': {
                    'column_id': 'Name',
                    'filter_query': '{Name} eq "' + str(value) + '"'
                },
                'backgroundColor': '#34C00B',
                'color': 'white',
            }
        )
    for value in direct_ester_class1:
        styles.append(
            {
                'if': {
                    'filter_query': '{IPC} eq ' + str(value)
                },
                'backgroundColor': '#D01C1C',
                'color': 'white',
            }
        )
    for value in direct_ester_class2:
        styles.append(
            {
                'if': {
                    'filter_query': '{IPC} eq ' + str(value)
                },
                'backgroundColor': '#D09C1C',
                'color': 'white',
            }
        )
    for value in direct_ester_class3:
        styles.append(
            {
                'if': {
                    'filter_query': '{IPC} eq ' + str(value)
                },
                'backgroundColor': '#34C00B',
                'color': 'white',
            }
        )
    for value in missing:
        styles.append(
            {
                'if': {
                    'filter_query': '{IPC} eq ' + str(value)
                },
                'backgroundColor': '#858385',
                'color': 'white',
            }
        )
    return styles
##################################

#####################################################################################################################################
######################################################### Some variables ############################################################
#####################################################################################################################################

rename_dict_for_data_upload = {
    'INGREDIENT': 'BOMItemName',
    'IPC': 'BOMItemNumber',
    'PARTS': 'Parts',
    "INGREDIENTS": 'BOMItemName',
    "INGREDIENT NAME": 'BOMItemName',
    "DOSAGES": 'Parts',
    "dosage": 'Parts',

}

comparator_type = ['OIL', 'EDT', 'Solvent',
                   'SGI', 'OSIFF', 'Main Odour', 'Sec Odour', 'Push', 'Diff', 'BP (°C)',
                   'log P', 'MW (g/mol)', 'SVP (mmHg)', 'SVP (µg/L)',
                   'Water Sol (mg/L @20°C)', 'Mvol (nm3)', 'CyD Suitability Score',
                   'Physico Chemical parameters Score', 'Criteria',
                   'Absorbing Capsules Suitability', 'Nat/Synth', 'MNC (USD/kg)', 'AOX', 'RM color', 'Risk Assessment']

list_aox = ['BHA', 'BHT', 'TBHQ', 'TOCOPHEROL ALPHA', 'TOCOPHEROLS NATURAL', 'STABILIFF', 'TINOGARD TT',
            'BUTYL PHENOL,2,6,DITERT', 'A-5237', 'CITRIC ACID']


#####################################################################################################################################
######################################################### Anlysis functions #########################################################
#####################################################################################################################################


def treatment_risk_assessment(df_data, df_data_2, df_ing, df_RMs_warning, df_solvent_nat_synt, df_explosion_nat,
                              df_explosion_synt, function):
    """
    FUNCTION : Risk Assessment and Comparator (action type : risk assessment)

    Input :
    - df_data : formula data for datatable "Non exploded formula risk assessment"
    - df_data_2 : formula data for datatable "Exploded formula risk assessment"
    - df_ing : database of raw materials (MDBEXPERF)
    - df_RMs_warning : database of raw materials risk assessment
    - df_solvent_nat_synt : database of indirect solvant in raw materials
    - df_explosion_nat :  database of natural raw materials explosion
    - df_explosion_synt : database of synthetic raw materials explosion
    - function : "risk_assessment" for risk assessment function and "comparator_risk_assessment" for comparator function (type risk assessment)

    Output :
    - df_data for risk assessment and comparator functions
    - df_data_2 for risk Assessment
    - df_data_warning for risk assessment
    """
    #If function risk assessment is called :
    if function == "risk_assessment":
        #we create 2 sames dataframes, df_data for non exploded datatable and df_data_warning for warning flags summary datatable
        df_data = df_data.join(df_RMs_warning.set_index('IPC'))
        df_data_warning = df_data.copy()

        #Theses columns are all the risk assessment
        columns_risk = ['COLORATION ', 'DYES ', 'ODOR ', 'METHANOL WARNING', 'REACTIVITY WITH HEAVY METALS',
                        'CAN GENERATE PARTICLES', 'SOLUBILITY', '% GC']

        #df_data_warning treatment_AOX
        #We create a new column named "PURE QUANTITY %"
        #The calcul of the PURE QUANTITY is dosage * dilution / 100
        df_data_warning = df_data_warning.reindex(columns=[*df_data_warning.columns.tolist(), *['PURE QUANTITY %']])
        df_data_warning['PURE QUANTITY %'] = df_data_warning[['PURE QUANTITY %', 'dosage', 'Dilution']].apply(
            lambda x: round(x[1] * x[2] / 100, 3), axis=1)
        df_data_warning = df_data_warning.rename_axis('IPC').reset_index()
        df_data_warning["dosage"] = round(df_data_warning["dosage"], 3)
        df_data_warning[columns_risk] = df_data_warning[columns_risk].replace(np.nan, "")
        df_data_warning = df_data_warning.sort_values(by='dosage', ascending=False)
        df_data_warning = df_data_warning.drop(['Neat IPC', 'Dilution', 'Solvent', '% GC'], axis=1)
        df_data_warning = df_data_warning[(df_data_warning['COLORATION '] != "") | (df_data_warning['DYES '] != "") | (
                    df_data_warning['ODOR '] != "") | (df_data_warning['METHANOL WARNING'] != "") | (
                                                      df_data_warning['REACTIVITY WITH HEAVY METALS'] != "") | (
                                                      df_data_warning['CAN GENERATE PARTICLES'] != "") | (
                                                      df_data_warning['SOLUBILITY'] != "")]

        #df_data treatment
        #In df_data, we resume all the risk assessment in one column name Risk Assessment
        df_data[columns_risk] = df_data[columns_risk].replace(np.nan, "")
        df_data = df_data.reindex(columns=[*df_data.columns.tolist(), *['Risk Assessment', 'PURE QUANTITY %']])
        df_data['PURE QUANTITY %'] = df_data[['PURE QUANTITY %', 'dosage', 'Dilution']].apply(
            lambda x: round(x[1] * x[2] / 100, 3), axis=1)
        df_data['Risk Assessment'] = df_data[columns_risk].apply(
            lambda x: str('\n').join([str(f) + ' ==> ' + str(x[f]) for f in df_data[columns_risk] if x[f] != ""]),
            axis=1)
        df_data = df_data.rename_axis('IPC').reset_index()
        df_data = df_data.drop(
            ['Neat IPC', 'COLORATION ', 'DYES ', 'ODOR ', 'METHANOL WARNING', 'REACTIVITY WITH HEAVY METALS',
             'CAN GENERATE PARTICLES', 'SOLUBILITY', '% GC'], axis=1)
        df_data["dosage"] = round(df_data["dosage"], 3)

        new_order = [0, 2, 3, 4, 5, 1, 6]
        df_data = df_data[df_data.columns[new_order]]
        df_data = df_data.sort_values(by='dosage', ascending=False)

        #df_data_2 treatment
        #df_data_2 is for exploded formula datatable
        #We search for each molecule what is the component of raw materials
        columns = ['dosage', 'Dilution', 'Solvent','Name']


        df_data_2_solvant = df_data[columns]
        df_data_2_solvant = df_data_2_solvant.rename(columns={'Name': 'NAME DECOMPO'})
        df_data_2_solvant = df_data_2_solvant.set_index('Solvent').join(df_solvent_nat_synt.set_index('Solvent'))

        df_data_2_solvant = df_data_2_solvant.drop(["SGI", "OSIFF", "Decomposition Solvent"], axis=1)
        df_data_2_solvant = df_data_2_solvant.reindex(
            columns=[*df_data_2_solvant.columns.tolist(), *['PURE QUANTITY %']])
        df_data_2_solvant['PURE QUANTITY %'] = df_data_2_solvant[['PURE QUANTITY %', 'dosage', 'Dilution']].apply(
            lambda x: round(x[1] * (100 - x[2]) / 100, 3), axis=1)
        df_data_2_solvant = df_data_2_solvant[~df_data_2_solvant["Name"].isna()]
        df_data_2_solvant = df_data_2_solvant.drop(['Name', 'dosage', 'Dilution'], axis=1)

        myList2 = list(df_data_2_solvant.columns)
        myList2[2] = 'dosage'
        df_data_2_solvant.columns = myList2

        df_data_2_solvant_nat = df_data_2_solvant.set_index('IPC').join(df_explosion_nat.set_index('IPC'))
        df_data_2_solvant_synt = df_data_2_solvant.set_index('IPC').join(df_explosion_synt.set_index('IPC'))
        df_data_2_solvant_nat = df_data_2_solvant_nat[~df_data_2_solvant_nat["NAME"].isna()]
        df_data_2_solvant_synt = df_data_2_solvant_synt[~df_data_2_solvant_synt["NAME"].isna()]

        myList3 = list(df_data_2_solvant_nat.columns)
        myList3[3] = 'COMPONENT'
        df_data_2_solvant_nat.columns = myList3

        df_data_2_solvant_nat = pd.DataFrame(df_data_2_solvant_nat.groupby(["NAME DECOMPO", "COMPONENT"])["dosage"].sum())
        df_data_2_solvant_synt = pd.DataFrame(df_data_2_solvant_synt.groupby(["NAME DECOMPO", "COMPONENT"])["dosage"].sum())

        df_data_2_solvant_nat = df_data_2_solvant_nat.reset_index(["NAME DECOMPO", "COMPONENT"])

        df_data_2_solvant_synt = df_data_2_solvant_synt.reset_index(["NAME DECOMPO", "COMPONENT"])
        df_data_2_solvant_nat = df_data_2_solvant_nat.rename(columns={'NAME DECOMPO': 'NAME'})
        df_data_2_solvant_synt = df_data_2_solvant_synt.rename(columns={'NAME DECOMPO': 'NAME'})
        df_data_2_solvant_synt["GS Area%"] = 100
        df_data_2_solvant_synt = df_data_2_solvant_synt[
            ~(df_data_2_solvant_synt.index.isin(df_data_2_solvant_nat.index))]

        #explosion Natural
        df_data_2_nat = df_data_2.join(df_explosion_nat.set_index('IPC'))
        df_data_2_nat = df_data_2_nat[~df_data_2_nat["NAME"].isna()]
        df_data_2_nat = df_data_2_nat.drop(['Name'], axis=1)
        df_data_2_nat = df_data_2_nat.join(df_ing.set_index('IPC'))
        cols = ['dosage', 'NAME', 'Peak Name', 'GS Area%', 'Name', 'Dilution (%)']
        df_data_2_nat = df_data_2_nat[cols]
        df_data_2_nat = df_data_2_nat.reindex(columns=[*df_data_2_nat.columns.tolist(), *['PURE QUANTITY %']])
        df_data_2_nat['PURE QUANTITY %'] = df_data_2_nat[['PURE QUANTITY %', 'dosage', 'Dilution (%)']].apply(
            lambda x: round(x[1] * x[2] / 100, 3), axis=1)
        df_data_2_nat = df_data_2_nat.drop(['Name', 'dosage', 'Dilution (%)'], axis=1)

        myList5 = list(df_data_2_nat.columns)
        myList5[3] = 'dosage'
        df_data_2_nat.columns = myList5

        #explosion Synthetic
        df_data_2_synt = df_data_2.join(df_explosion_synt.set_index('IPC'))
        df_data_2_synt = df_data_2_synt[~df_data_2_synt["NAME"].isna()]
        df_data_2_synt = df_data_2_synt.drop(['Name'], axis=1)
        df_data_2_synt = df_data_2_synt.join(df_ing.set_index('IPC'))
        cols = ['dosage', 'NAME', 'Name', 'Dilution (%)', 'COMPONENT']
        df_data_2_synt = df_data_2_synt[cols]
        df_data_2_synt = df_data_2_synt.reindex(columns=[*df_data_2_synt.columns.tolist(), *['PURE QUANTITY %']])
        df_data_2_synt['PURE QUANTITY %'] = df_data_2_synt[['PURE QUANTITY %', 'dosage', 'Dilution (%)']].apply(
            lambda x: round(x[1] * x[2] / 100, 3), axis=1)
        df_data_2_synt = df_data_2_synt.drop(['Name', 'dosage', 'Dilution (%)'], axis=1)

        df_data_2_synt = df_data_2_synt[~(df_data_2_synt.index.isin(df_data_2_nat.index))]

        myList6 = list(df_data_2_synt.columns)
        myList6[2] = 'dosage'
        df_data_2_synt.columns = myList6

        df_data_2_synt["GS Area%"] = 100

        myList = list(df_data_2_nat.columns)
        myList[1] = 'COMPONENT'
        df_data_2_nat.columns = myList

        #We concatenate
        df_data_2 = pd.concat([df_data_2_nat, df_data_2_synt, df_data_2_solvant_nat, df_data_2_solvant_synt],
                              sort=False)

        df_data_2["Decomposition"] = df_data_2["NAME"] + " " + round(
            (df_data_2["dosage"] * df_data_2["GS Area%"] / 100).astype(np.double), 3).astype(str) + " % ;"
        df_data_2["Quantity"] = round((df_data_2["dosage"] * df_data_2["GS Area%"] / 100).astype(np.double), 3)

        df_data_2 = df_data_2.drop(['dosage', 'NAME', "GS Area%"], axis=1)

        new_order_ns = [0, 2, 1]
        df_data_2 = df_data_2[df_data_2.columns[new_order_ns]]

        myList4 = list(df_data_2.columns)
        myList4[0] = 'Molecule Name'
        df_data_2.columns = myList4

        df_data_2 = pd.DataFrame(df_data_2.groupby("Molecule Name")["Decomposition"].sum()).join(
            df_data_2.groupby("Molecule Name")["Quantity"].sum())
        df_data_2 = df_data_2.reset_index(["Molecule Name"])

        df_data_2 = df_data_2.sort_values(by='Quantity', ascending=False)
        df_data_2["Quantity"] = round(df_data_2["Quantity"], 3)

        df_data=df_data.rename(columns={'dosage': 'Quantity %'})
        df_data_warning=df_data_warning.rename(columns={'dosage': 'Quantity %'})

        #df_data for Non epxloded formula
        #df_data_2 for Exploded formula
        #df_data_warning for warning flag summary
        return df_data, df_data_2, df_data_warning

#if function comparator with criteria risk assessment is called :
    if function == "comparator_risk_assessment":

        #We need only one dataframe for return : df_data
        columns_risk = ['COLORATION ', 'DYES ', 'ODOR ', 'METHANOL WARNING', 'REACTIVITY WITH HEAVY METALS',
                        'CAN GENERATE PARTICLES', 'SOLUBILITY', '% GC']
        df_data = df_data.set_index('IPC').join(df_RMs_warning.set_index('IPC'))

        #We replace NaN by "" in risk assessment columns
        df_data[columns_risk] = df_data[columns_risk].replace(np.nan, "")

        #We calculate the Pure quantity of each raw materials
        df_data = df_data.reindex(columns=[*df_data.columns.tolist(), *['Risk Assessment', 'PURE QUANTITY %']])
        df_data['PURE QUANTITY %'] = df_data[['PURE QUANTITY %', 'dosage', 'Dilution']].apply(
            lambda x: round(x[1] * x[2] / 100, 3), axis=1)

        #We set all the risks assessment by raw materials
        df_data['Risk Assessment'] = df_data[columns_risk].apply(
            lambda x: str('\n').join([str(f) + ' ==> ' + str(x[f]) for f in df_data[columns_risk] if x[f] != ""]),
            axis=1)
        df_data = df_data.rename_axis('IPC').reset_index()

        #Then we drop all the risk columns
        df_data = df_data.drop(
            ['Neat IPC', 'COLORATION ', 'DYES ', 'ODOR ', 'METHANOL WARNING', 'REACTIVITY WITH HEAVY METALS',
             'CAN GENERATE PARTICLES', 'SOLUBILITY', '% GC'], axis=1)
        df_data["dosage"] = round(df_data["dosage"], 3)

        #We reorder the columns
        new_order = [0, 2, 3, 4, 5, 1, 6]
        df_data = df_data[df_data.columns[new_order]]
        df_data = df_data.sort_values(by='dosage', ascending=False)

        df_data = df_data[['IPC', 'Risk Assessment', 'Name']]

        #df_data : return dataframe to be treated
        return df_data, [], []

def treatment_AOX(df_aox_data, df_aox, df_output, df_data, function):
    """
    FUNCTION : Anti oxidant origin

    Input :
    - df_aox_data : formula data for simple analyse
    - df_aox : aox database
    - df_output / df_data : formula data for comparator analyse
    - function : "AOX" for simple analyse or "comparator" for comparator analyse

    Output :
    - df_aox_data : data result for aox analyse and comparator analyse
    - df_direct / df_direct_bis / aox_in_direct : data results for aox analyse
    """
    #aox_in_direct is setting at null in case if this variable return nothing
    aox_in_direct = []

    #if function antioxidant origin is called :
    if function == "AOX":

        #df_aox_data is the data for datatable "part of antioxidant in raw Materials"
        df_aox_data = df_aox_data.join(df_aox.set_index('IPC'))

        #If raw material is not content is the database of df_aox, we set it at "Not Found"
        df_aox_data['Name'] = df_aox_data['Name'].fillna(df_aox_data.index.to_series()).fillna('Not Found')
        df_aox_data = df_aox_data.fillna("")

        #We drop the column of data we don't need and we re index the column IPC
        df_aox_data = df_aox_data.drop(['Dilution', 'Solvant'], axis=1)
        df_aox_data = df_aox_data.rename_axis('IPC').reset_index()

        #We set a new order of the columns
        new_order = [2, 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        df_aox_data = df_aox_data[df_aox_data.columns[new_order]]

        #We treat only the aox columns, so we remove Name, IPC and dosage from the list
        aox_columns = list(set(df_aox_data) - set(['Name', 'IPC', 'dosage']))

        #We treat the dosage of AOX for each raw material
        #e set sr = dosage /100
        sr = df_aox_data['dosage'] / 100

        #We multiply the quandity of AOX by sr
        df_aox_data[aox_columns] = df_aox_data[aox_columns].apply(pd.to_numeric, errors='coerce')
        df_aox_data[aox_columns] = df_aox_data[aox_columns].mul(sr, fill_value=None, axis=0)
        df_aox_data[aox_columns] = df_aox_data[aox_columns].apply(pd.to_numeric, errors='coerce')
        df_aox_data[aox_columns] = df_aox_data[aox_columns].replace(np.nan, "")

        #We are looking if some AOX are in direct in raw material, it means that the dosage of the raw material is equal to the quantity of AOX
        df_aox_data['AOX in direct'] = df_aox_data[aox_columns].apply(
            lambda x: ";".join([str(round(x[f], 4)) for f in df_aox_data[aox_columns] if x[f] != ""]), axis=1)

        #We put all the direct AOX in the variable aox_in_direct
        #Then we can color the line in the datatable which the AOX is in direct, by recognize them by their IPC
        aox_in_direct = list(
            df_aox_data[df_aox_data['AOX in direct'].astype(str) == df_aox_data['dosage'].astype(str)]['IPC'])

        #We fill the column AOX
        df_aox_data['AOX'] = df_aox_data[aox_columns].apply(
            lambda x: ";".join([str(f) + ' : ' + str(round(x[f], 8)) for f in df_aox_data[aox_columns] if x[f] != ""]),
            axis=1)

        #We we round the dosage with 4 numbers after comma
        df_aox_data['dosage'] = round(df_aox_data['dosage'], 4)

        #We set 2 dataframes
        #df_direct for anti oxidant direct or indirect
        #df_direct_bis for diagram in anti oxidant origin analyse
        columns_direct = ['AOX', '% DIRECT', '% INDIRECT', '% TOTAL']
        df_direct = pd.DataFrame(columns=columns_direct)
        df_direct_bis = pd.DataFrame(columns=columns_direct)

        #We treat the dataframe df_direct and df_direct_bis
        sr_2 = df_aox_data['dosage']
        in_aox = []

        #We browse all the AOX in column
        for column in list_aox:
            direct = 0
            indirect = 0
            for i, part_aox in enumerate(df_aox_data[column]):
                if part_aox != "":
                    if round(part_aox, 4) == round(sr_2[i], 4):
                        direct = direct + part_aox
                    else:
                        indirect = indirect + part_aox
            # df_direct = df_direct.append(
            #     {'AOX': column, '% DIRECT': "{:.3E}".format(Decimal(direct)) if direct != 0 else 0,
            #      '% INDIRECT': "{:.3E}".format(Decimal(indirect)) if indirect != 0 else 0,
            #      '% TOTAL': "{:.3E}".format(Decimal(direct + indirect)) if direct + indirect != 0 else 0},
            #     ignore_index=True)
            df_direct = df_direct.append(
                {'AOX': column, '% DIRECT':round(direct,6) if direct != 0 else 0,
                 '% INDIRECT': round(indirect,6) if indirect != 0 else 0,
                 '% TOTAL': round(direct + indirect,6) if (direct + indirect) != 0 else 0},
                ignore_index=True)
            df_direct_bis = df_direct_bis.append(
                {'AOX': column, '% DIRECT': direct, '% INDIRECT': indirect, '% TOTAL': direct + indirect},
                ignore_index=True)

        df_aox_data = df_aox_data.sort_values(by='dosage', ascending=False)
        df_aox_data=df_aox_data.rename(columns={'dosage': 'Quantity %'})
        #df_aox_data for Part of Antioxidant in Raw Materials DataTable
        #df_direct for Antioxidant direct or indirect DataTable
        #df_direct_bis for aox diagram
        #aox_in_direct for canditional styling for datatable
        return df_aox_data, df_direct, df_direct_bis, aox_in_direct

    #if function comparator with criteria AOX is called :
    elif function == "comparator_AOX":

        #df_output : the data we recover from "def datatable_comparator" in app.py
        #df_output regroup the reference formula data and all the comparation formulas data with IPC
        df_output = df_output.replace(np.nan, "")

        #df_aox_data is same as df_output but with dosage
        df_aox_data = pd.DataFrame(df_data, columns=['dosage', 'IPC'])

        #We join df_aox_data with the aox databale
        df_aox_data = df_aox_data.set_index('IPC').join(df_aox.set_index('IPC'))

        #We treat the not found value
        df_aox_data['Name'] = df_aox_data['Name'].fillna(df_aox_data.index.to_series()).fillna('Not Found')
        df_aox_data = df_aox_data.fillna("")

        #We drop the columns we don't need
        df_aox_data = df_aox_data.drop(['Dilution', 'Solvant'], axis=1)

        #We reindex the IPC column to display it in the datatable
        df_aox_data = df_aox_data.rename_axis('IPC').reset_index()

        #We reorder the columns of df_aox_data
        new_order = [2, 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        df_aox_data = df_aox_data[df_aox_data.columns[new_order]]

        #We set a variable with only AOX columns
        aox_columns = list(set(df_aox_data) - set(['Name', 'IPC', 'dosage']))

        #We replace nan value by ""
        df_aox_data[aox_columns] = df_aox_data[aox_columns].apply(pd.to_numeric, errors='coerce')
        df_aox_data[aox_columns] = df_aox_data[aox_columns].replace(np.nan, "")

        #We display in AOX columns all the AOX content in the raw material
        df_aox_data['AOX'] = df_aox_data[aox_columns].apply(
            lambda x: ";".join([str(f) for f in df_aox_data[aox_columns] if x[f] != ""]), axis=1)
        df_aox_data = df_aox_data[['IPC', 'AOX', 'Name']]

        #We join the df_output with df_aox_data to display the raw material columns, all the formula reference and comparation columns and the AOX column
        df_output = df_output.set_index('IPC').join(df_aox_data.set_index('IPC'))

        #We reorder the columns of df_output
        cols = df_output.columns.tolist()
        cols = cols[-1:] + cols[:-1]

        df_output = df_output[cols]

        #df_output : we return the datatable df_output
        return df_output, [], [], aox_in_direct


def treatment_nat_synt(df_nat_synt, df_solvent_nat_synt, value):
    """
    FUNCTION : Natural Synthetic decomposition

    Input :
    - df_nat_synt : formula data
    - df_solvent_nat_synt : database of solvent natural or synthethic
    - value : solvent as a group or not

    Output :
    - df_nat_synt :datatable result of natural and synthetic decomposition of raw materials
    - natural : % of natural in the formula
    - synthetic : % of synthetic in the formula
    - solvant : % of solvant in the formula
    - NF : % of not found raw materials in the formula
    """

    #if solvent as group is on :
    if value == True:

        #We set the columns of df_nat_synt
        column_datatable = ['Name', 'IPC', 'parts', 'dosage', 'Nat/Synth']

        #we reindex the IPC column
        df_nat_synt = df_nat_synt.rename_axis('IPC').reset_index()

        #We reorder the columns of df_nat_synt DataFrame
        new_order_ns = [1, 0, 2, 3, 4, 5, 6]
        df_nat_synt = df_nat_synt[df_nat_synt.columns[new_order_ns]]

        #We stock the quantity of natural, synthetic, solvant and not found raw materials
        natural = round(df_nat_synt[['dosage', 'Nat/Synth', 'Dilution (%)', 'Solvent']].apply(
            lambda x: x[0] * x[2] / 100 if (x[1] == 'Natural' and not (x[2] == 100 and x[3] != 0)) else 0,
            axis=1).sum(), 3)
        synthetic = round(df_nat_synt[['dosage', 'Nat/Synth', 'Dilution (%)', 'Solvent']].apply(
            lambda x: x[0] * x[2] / 100 if (x[1] == 'Synthetic' and not (x[2] == 100 and x[3] != 0)) else 0,
            axis=1).sum(), 3)
        solvant_indirect = df_nat_synt[['dosage', 'Dilution (%)']].apply(lambda x: x[0] * (100 - x[1]) / 100,
                                                                         axis=1).sum()
        solvant_direct = df_nat_synt[['dosage', 'Dilution (%)', 'Solvent']].apply(
            lambda x: x[0] * x[1] / 100 if (x[1] == 100 and x[2] != 0) else 0, axis=1).sum()
        solvant = round(solvant_indirect + solvant_direct, 3)
        NF = round(100 - natural - synthetic - solvant, 3)

        #We round the column dosage by 4 number after comma
        df_nat_synt['dosage'] = round(df_nat_synt['dosage'], 4)

        #We sort the df_nat_synt dataframe by column
        df_nat_synt = df_nat_synt.sort_values(by='dosage', ascending=False)
        df_nat_synt=df_nat_synt.rename(columns={'dosage': 'Quantity %'})

        #df_nat_synt is datatable result
        #Natural / synthethic / solvant / NF : Natural synthetic decomposition result
        return df_nat_synt, natural, synthetic, solvant, NF

    #if solvent as group is off :
    elif value == False:

        #We set the columns of df_nat_synt with solvant
        columns_solvent = ['parts', 'dosage', 'Dilution (%)', 'Solvent']

        #we reindex the IPC column
        df_nat_synt = df_nat_synt.rename_axis('IPC').reset_index()

        #We reorder the columns of df_nat_synt DataFrame
        new_order_ns = [1, 0, 2, 3, 4, 5, 6]
        df_nat_synt = df_nat_synt[df_nat_synt.columns[new_order_ns]]

        #We join the df_solvent (df_nat_synt) with the solvant DataBase
        df_solvent = df_nat_synt[columns_solvent]
        df_solvent = df_solvent.set_index('Solvent').join(df_solvent_nat_synt.set_index('Solvent'))
        df_solvent = df_solvent.dropna(axis=0, how='any')

        #We calculate the dosage of natural solvant
        solvent_nat = round(df_solvent[['dosage', 'Decomposition Solvent', 'Dilution (%)']].apply(
            lambda x: x[0] * (100 - x[2]) / 100 if (x[1] == 'Natural' and x[2] != 100) else (
                x[0] * x[2] / 100 if (x[1] == 'Natural' and x[2] == 100) else 0), axis=1).sum(), 3)

        #We calculate the dosage of synthetic solvant
        solvent_synt = round(df_solvent[['dosage', 'Decomposition Solvent', 'Dilution (%)']].apply(
            lambda x: x[0] * (100 - x[2]) / 100 if (x[1] == 'Synthetic' and x[2] != 100) else (
                x[0] * x[2] / 100 if (x[1] == 'Synthetic' and x[2] == 100) else 0), axis=1).sum(), 3)

        #We calculate the dosage of natural raw materials
        natural = round(df_nat_synt[['dosage', 'Nat/Synth', 'Dilution (%)', 'Solvent']].apply(
            lambda x: x[0] * x[2] / 100 if (x[1] == 'Natural' and not (x[2] == 100 and x[3] != 0)) else 0,
            axis=1).sum(), 3)

        #We calculate the dosage of synthetic raw materials
        synthetic = round(df_nat_synt[['dosage', 'Nat/Synth', 'Dilution (%)', 'Solvent']].apply(
            lambda x: x[0] * x[2] / 100 if (x[1] == 'Synthetic' and not (x[2] == 100 and x[3] != 0)) else 0,
            axis=1).sum(), 3)

        #We regroup the quantity of dosage of natural/synthethoc and solvant natural/synthethic raw material
        natural = round(natural + solvent_nat, 3)
        synthetic = round(synthetic + solvent_synt, 3)

        #We calculate the Not found value
        NF = round(100 - natural - synthetic, 3)

        #We round the column dosage by 4 number after comma
        df_nat_synt['dosage'] = round(df_nat_synt['dosage'], 4)

        #We sort the df_nat_synt dataframe by column
        df_nat_synt = df_nat_synt.sort_values(by='dosage', ascending=False)
        df_nat_synt=df_nat_synt.rename(columns={'dosage': 'Quantity %'})

        #df_nat_synt is datatable result
        #Natural / synthethic / NF : Natural synthetic decomposition result
        return df_nat_synt, natural, synthetic, 0, NF


def treatment_solvent(df_data_dt, df_ing):
    """
    FUNCTION : Solvent Decomposition

    Input :
    - df_data_dt : formula data
    - df_ing : solvent database of raw material

    Output :
    - df_data_dt : formula data result with solvent decomposition
    - df_solv_direct : solvent data in direct
    - df_solv_direct_bis : solvent data in direct (to display another type of datatable)
    - df_solvent_data : data to display the diagram of function solvent decomposition
    """
    #Setting columns for df_data_dt
    solvent_columns = ['Name', 'IPC', 'dosage', 'Dilution (%)', 'Solvent']

    #Setting columns for datatable solvent direct or indirect
    solvent_direct = ['SOLVENT', '% DIRECT', '% INDIRECT', '% TOTAL']

    #Setting 2 dataframes :
    #df_solv_direct for datatable solvent direct or indirect
    #df_solv_direct_bis for solvent diagram
    df_solv_direct = pd.DataFrame(columns=solvent_direct)
    df_solv_direct_bis = pd.DataFrame(columns=solvent_direct)

    #join data formula with database df_warning
    df_data_dt = df_data_dt.join(df_ing.set_index('IPC'))

    #We reindex IPC column
    df_data_dt = df_data_dt.rename_axis('IPC').reset_index()

    #We keep only columns in variable solvent_columns
    df_data_dt = df_data_dt[solvent_columns]

    #Creating 2 columns : Pure quantity % and solvent quantity % and then calculate them
    df_data_dt = df_data_dt.reindex(columns=[*df_data_dt.columns.tolist(), *['PURE QUANTITY %', 'SOLVENT QUANTITY %']])
    df_data_dt['PURE QUANTITY %'] = df_data_dt[['PURE QUANTITY %', 'dosage', 'Dilution (%)']].apply(
        lambda x: round(x[1] * x[2] / 100, 3), axis=1)
    df_data_dt['SOLVENT QUANTITY %'] = df_data_dt[['SOLVENT QUANTITY %', 'dosage', 'Dilution (%)']].apply(
        lambda x: round(x[1] * (100 - x[2]) / 100, 3), axis=1)
    # df_data_dt=df_data_dt.drop('Dilution (%)',axis=1)

    #Creating dataframe for solvant
    df_solvant = pd.DataFrame(columns=solvent_direct)

    #Recover the list of solvant from database and then remove the duplicates
    df_solvant['SOLVENT'] = df_ing['Solvent']
    df_solvant['SOLVENT'] = df_solvant['SOLVENT'].drop_duplicates(keep='first')
    df_solvant = df_solvant.dropna(axis=0, how='all')
    df_solvant = df_solvant[df_solvant.SOLVENT != 0]

    #Creating df_solvent_data to display all the solvent columns ind DataFrame
    df_solvent_data = pd.DataFrame(columns=['Name'])
    a = 0

    #treatment of df_solvent_data, df_solv_direct and df_solv_direct_bis
    for j, column in enumerate(df_solvant['SOLVENT']):
        direct = 0
        indirect = 0
        for i, solv in df_data_dt.iterrows():
            if solv['Solvent'] == column and solv['Dilution (%)'] == 100:
                direct = direct + solv['PURE QUANTITY %']
                if column not in df_solvent_data.columns:
                    df_solvent_data = df_solvent_data.reindex(columns=[*df_solvent_data.columns.tolist(), *[column]])
                df_solvent_data.loc[a, 'Name'] = solv['Name']
                df_solvent_data.loc[a, column] = direct
                a = a + 1
            elif solv['Solvent'] == column and solv['SOLVENT QUANTITY %'] != 0:
                indirect = indirect + solv['SOLVENT QUANTITY %']
                if column not in df_solvent_data.columns:
                    df_solvent_data = df_solvent_data.reindex(columns=[*df_solvent_data.columns.tolist(), *[column]])
                df_solvent_data.loc[a, 'Name'] = solv['Name']
                df_solvent_data.loc[a, column] = indirect
                a = a + 1
        # df_solv_direct = df_solv_direct.append(
        #     {'SOLVENT': column, '% DIRECT': "{:.3E}".format(Decimal(direct)) if direct != 0 else 0,
        #      '% INDIRECT': "{:.3E}".format(Decimal(indirect)) if indirect != 0 else 0,
        #      '% TOTAL': "{:.3E}".format(Decimal(direct + indirect)) if direct + indirect != 0 else 0},
        #     ignore_index=True)
        df_solv_direct = df_solv_direct.append(
            {'SOLVENT': column, '% DIRECT':round(direct,6) if direct != 0 else 0,
             '% INDIRECT': round(indirect,6) if indirect != 0 else 0,
             '% TOTAL': round(direct + indirect,6) if direct + indirect != 0 else 0},
            ignore_index=True)
        df_solv_direct_bis = df_solv_direct_bis.append(
            {'SOLVENT': column, '% DIRECT': direct, '% INDIRECT': indirect, '% TOTAL': direct + indirect},
            ignore_index=True)

    #Remove line when % TOTAL = 0
    df_solv_direct = df_solv_direct.drop(df_solv_direct.loc[df_solv_direct['% TOTAL'] == 0].index)

    #Sort df_data_dt by dosage descending
    df_data_dt = df_data_dt.sort_values(by='dosage', ascending=False)

    #Replace 0 by "" in column solvent
    df_data_dt['Solvent'] = df_data_dt['Solvent'].replace(0, "")

    #round the dosage by 4 number after comma
    df_data_dt['dosage'] = round(df_data_dt['dosage'], 4)
    df_data_dt=df_data_dt.rename(columns={'dosage': 'Quantity %'})

    #df_data_dt : datatable Parts of solvent in RM
    #df_solv_direct : datatable solvent in direct or indirect
    #df_solv_direct_bis, df_solvent_data : data for solvent diagram
    return df_data_dt, df_solv_direct, df_solv_direct_bis, df_solvent_data


def treatment_RMs_tracking(df_data, df_ing, df_indole, df_linalyl, df_methyl, df_vanillin, value):
    """
    FUNCTION : RMs tracking Problematic

    Input :
    - df_data : formula data
    - df_ing : database of raw materials
    - df_indole / df_linalyl / df_methyl / df_vanillin : database of raw materials tracking
    - value : boolean to display all the raw material of formula or not

    Output :
    - df_data_identification : DataFrame result to display datatable with raw material tracking
    """
    #We set new order for df_data_indole, df_data_linalyl, df_data_methyl and df_data_vanillin
    new_order = [2, 0, 1, 3, 4]

    #Looking if there is indole in our formula
    df_data_indole = df_data.join(df_indole.set_index('IPC'))

    #We drop naN value
    df_data_indole = df_data_indole.dropna(axis=0)
    if df_data_indole.empty == False:
        #We create 2 columns and we complete them
        df_data_indole = df_data_indole.reindex(
            columns=[*df_data_indole.columns.tolist(), *['Dosage in raw material', 'Identification']])
        df_data_indole['Dosage in raw material'] = df_data_indole[['Dosage in raw material', 'dosage', '% GC']].apply(
            lambda x: round(x[1] * x[2] / 100, 4), axis=1)
        df_data_indole = df_data_indole.drop(["NAME", "% GC"], axis=1)
        df_data_indole = df_data_indole.rename_axis('IPC').reset_index()
        df_data_indole['Identification'] = 'indole'
        df_data_indole = df_data_indole[df_data_indole.columns[new_order]]
    else:
        df_data_indole = df_data_indole.drop(["NAME", "% GC"], axis=1)

    #Looking if there is linalyl in our formula
    df_data_linalyl = df_data.join(df_linalyl.set_index('IPC'))

    #We drop naN value
    df_data_linalyl = df_data_linalyl.dropna(axis=0)
    if df_data_linalyl.empty == False:
        #We create 2 columns and we complete them
        df_data_linalyl = df_data_linalyl.reindex(
            columns=[*df_data_linalyl.columns.tolist(), *['Dosage in raw material', 'Identification']])
        df_data_linalyl['Dosage in raw material'] = df_data_linalyl[['Dosage in raw material', 'dosage', '% GC']].apply(
            lambda x: round(x[1] * x[2] / 100, 4), axis=1)
        df_data_linalyl = df_data_linalyl.drop(["NAME", "% GC"], axis=1)
        df_data_linalyl = df_data_linalyl.rename_axis('IPC').reset_index()
        df_data_linalyl['Identification'] = 'linalyl acetate'
        df_data_linalyl = df_data_linalyl[df_data_linalyl.columns[new_order]]
    else:
        df_data_linalyl = df_data_linalyl.drop(["NAME", "% GC"], axis=1)

    #Looking if there is methyl anthranilate in our formula
    df_data_methyl = df_data.join(df_methyl.set_index('IPC'))

    #We drop naN value
    df_data_methyl = df_data_methyl.dropna(axis=0)
    if df_data_methyl.empty == False:
        #We create 2 columns and we complete them
        df_data_methyl = df_data_methyl.reindex(
            columns=[*df_data_methyl.columns.tolist(), *['Dosage in raw material', 'Identification']])
        df_data_methyl['Dosage in raw material'] = df_data_methyl[['Dosage in raw material', 'dosage', '% GC']].apply(
            lambda x: round(x[1] * x[2] / 100, 4), axis=1)
        df_data_methyl = df_data_methyl.drop(["NAME", "% GC"], axis=1)
        df_data_methyl = df_data_methyl.rename_axis('IPC').reset_index()
        df_data_methyl['Identification'] = 'methyl anthranilate'
        df_data_methyl = df_data_methyl[df_data_methyl.columns[new_order]]
    else:
        df_data_methyl = df_data_methyl.drop(["NAME", "% GC"], axis=1)

    #Looking if there is vanillin in our formula
    df_data_vanillin = df_data.join(df_vanillin.set_index('IPC'))

    #We drop naN value
    df_data_vanillin = df_data_vanillin.dropna(axis=0)

    if df_data_vanillin.empty == False:
        #We create 2 columns and we complete them
        df_data_vanillin = df_data_vanillin.reindex(
            columns=[*df_data_vanillin.columns.tolist(), *['Dosage in raw material', 'Identification']])
        df_data_vanillin['Dosage in raw material'] = df_data_vanillin[
            ['Dosage in raw material', 'dosage', '% GC']].apply(lambda x: round(x[1] * x[2] / 100, 4), axis=1)
        df_data_vanillin = df_data_vanillin.drop(["NAME", "% GC"], axis=1)
        df_data_vanillin = df_data_vanillin.rename_axis('IPC').reset_index()
        df_data_vanillin['Identification'] = 'vanillin'
        df_data_vanillin = df_data_vanillin[df_data_vanillin.columns[new_order]]
    else:
        df_data_vanillin = df_data_vanillin.drop(["NAME", "% GC"], axis=1)

    #if we display in the datatable only raw materials tracking
    if value == False:
        #Concatenation
        df_data_identification = pd.concat([df_data_indole, df_data_linalyl, df_data_methyl, df_data_vanillin],
                                           sort=False, ignore_index=True)
        df_data_identification = df_data_identification.reindex(
            columns=[*df_data_identification.columns.tolist(), *['Dosage in raw material', 'Identification','IPC']])
    #if we display in the datatable all the formula raw materials
    elif value == True:
        #We recover the data formula
        df_data = pd.DataFrame(df_data, columns=["dosage"])
        df_data = df_data.join(df_ing[['Name', 'IPC']].set_index('IPC'))

        #We reindex IPC column
        df_data = df_data.rename_axis('IPC').reset_index()

        #We create 2 columns for df_data dataframe
        df_data = df_data.reindex(columns=[*df_data.columns.tolist(), *['Dosage in raw material', 'Identification']])

        #We set the new new_order_ns
        df_data = df_data[df_data.columns[new_order]]

        #Concatenation
        df_data_identification = pd.concat([df_data_indole, df_data_linalyl, df_data_methyl, df_data_vanillin, df_data],
                                           sort=False, ignore_index=True)

    df_data_identification=df_data_identification.rename(columns={'dosage': 'Quantity %'})

    #df_data_identification : result datatable for RMs tracking function
    return df_data_identification

def treatment_P20_P80(df_data_dt, df_p80, df_p20_p80,df_methyl_direct):
    """
    FUNCTION : P20 / P80
    Notice that this function cna be executed only if there is/are some methyl anthranilate in the formula

    Input :
    - df_data_dt : formula data
    - df_p80 : formula data that we are going to use to list p80 data
    - df_p20_p80 : database of p20 p80

    Output :
    - df_p20 : p20 data formula
    - df_p80 : p80 data formula
    - P20_somme : p20 sum dosage
    - P80_somme : p80 sum dosage
    - P20_somme_2 : p20 sum dosage not completed at 20%
    - P80_somme_2 : p80 sum when p20 sum not completed at 20%
    - perc :  percentage of p20 dosage sum
    - condition : if p20 is completed or not
    - methyl : if methyl is present or not in the formula
    """
    #somme : sum of all the raw materials dosage
    somme = df_data_dt['parts'].sum()

    #split the sum of dosage in 20 % - 80 %
    P20_somme = round(somme / 5, 4)
    P80_somme = round(somme - P20_somme, 4)

    #We create a copy of our data formula in df_p20
    df_p20 = df_data_dt.copy()

    #We join df_p20 with p20 p80 database
    df_p20 = df_p20.join(df_p20_p80.set_index('IPC'))

    #Dropping the columns we don't need
    df_p20 = df_p20.drop(columns='methyl or indole')

    #Dropping nan values
    df_p20 = df_p20.dropna()

    #Sorting value by column numero (order for prioritate p20 p80 raw materials)
    df_p20 = df_p20.sort_values(by='numero')

    #Initialize some variables
    list_p20 = []
    sum = 0
    last_p20 = 0

    #Set boolean
    #methyl : false for no methyl in formula / true for methyl in formula
    methyl = False

    #condition : false for p20 not completed / true for p20 fully completed
    condition = True

    #Looking if methyl is present in the formula
    for i, row in df_p20.iterrows():
        #if i == '00133186' or i == '00132867' or i == '00132116' or i == '13319600' or i == '00133190' or i == '00133188' or i == '00133158' or i == '00132117' or i == '00135689':
        if i in pd.unique(df_methyl_direct['IPC']).tolist():
            methyl = True

    #if methyl is present in formula, we do the analyse
    if methyl == True:
        condition = False

        #Looking for p20 raw materials in formula and place it to list_p20
        for i, row in df_p20.iterrows():

            if i == '00094006' or i == '00094008' or i == '00091687':
                sum = sum + row.parts * 0.2
                df_p20.loc[i, 'parts'] = df_p20.loc[i, 'parts'] * 0.2
                df_p80.loc[i, 'parts'] = df_p20.loc[i, 'parts'] * 4
                list_p20.append(i)
            else:
                sum = sum + row.parts

            if sum < P20_somme:
                if i != '00094006' and i != '00094008' and i != '00091687':
                    list_p20.append(i)


            elif sum > P20_somme:
                condition = True
                if i != '00094006' and i != '00094008' and i != '00091687':
                    list_p20.append(i)
                last_p20 = sum - P20_somme
                break

        #Setting p80 formula without p20 data formula
        df_p80 = df_p80.drop(list_p20)

        #Setting p20 formula
        df_p20=df_p20.rename_axis('IPC').reset_index()
        df_p20=df_p20[df_p20['IPC'].isin(list_p20)]
        df_p20=df_p20.set_index('IPC')


        #Calculate in order to have 20% part of formula in p20
        df_p20.iloc[df_p20.shape[0] - 1, 0] = round(df_p20.iloc[df_p20.shape[0] - 1, 0] - last_p20,4)
        df_p80.loc[list(df_p20.index)[-1], 'parts'] = round(last_p20,4)
        df_p80.loc[list(df_p20.index)[-1], 'Name'] = df_p20['Name'][-1]

        #Not necessary now but need it to color the raw materials target in the color orange
        orange = []
        df_orange = df_p20.loc[(df_p20['numero'] == 1) | (df_p20['numero'] == 2) | (df_p20['numero'] == 3) | (
                    df_p20['numero'] == 4), 'Name']

        #Dropping some columns
        df_p20 = df_p20.drop(columns='numero')

        #Setting the dataframe with only 2 columns
        df_p20 = df_p20[['Name', 'parts']]
        df_p80 = df_p80[['Name', 'parts']]

        df_p20=df_p20.rename_axis('IPC').reset_index()
        df_p80=df_p80.rename_axis('IPC').reset_index()
        #if p20 not completed
        if condition == False:
            P20_somme_2 = round(df_p20['parts'].sum(), 4)
            P80_somme_2 = round(somme - P20_somme_2, 4)
            perc = round(P20_somme_2 / somme * 100, 0)

            #df_p20 : using for p20 array
            #df_p80 : using for p80 array
            #P20_somme_2 : sum of parts in p20 not fully completed
            #P80_somme_2 : sum of parts in p80
            #perc : percentage of p20 parts in formula
            #condition and methyl : return boolean
            return df_p20, df_p80, 0, 0, P20_somme_2, P80_somme_2, perc, condition, methyl

        #if p20 completed
        elif condition == True:

            #df_p20 : using for p20 array
            #df_p80 : using for p80 array
            #P20_somme : sum of parts in p20
            #P80_somme : sum of parts in p80
            #condition and methyl : return boolean
            return df_p20, df_p80, P20_somme, P80_somme, 0, 0, 0, condition, methyl

    #if no methyl in the formula
    elif methyl == False:

        #return empty results
        return pd.DataFrame(), pd.DataFrame(), 0, 0, 0, 0, 0, False, methyl


def treatment_RMs_color(df_data, value, function):
    """
    FUNCTION : Raw materials initial color and fundamentals
    This function can be called by 2 analysis : RMs initial color and fundamentals or Comparator with criteria Rms colors

    Input :
    - df_data : data formula for RMs initial color analyse and data formulas (reference and comparations) for comparator analyse
    - value : Columns we want to display or not
    - function : RMs_color for RMs initial color analyse and comparator_RMs_color for comparator analyse

    Output :
    - df_data : data result formula
    - custom : style for displaying color in category neit oil color in the datatable result
    """
#Creating 3 new columns for neit oil category
    df_data = df_data.reindex(columns=[*df_data.columns.tolist(), *['From', 'To', 'PURE RAW MATERIAL QUANTITY']])

    #Setting the right syntax to display the color in datatable cells
    df_data['From'] = df_data[['From', 'R1', 'G1', 'B1']].apply(
        lambda x: "RGB(" + str(round(x[1], 2)) + "," + str(round(x[2], 2)) + "," + str(round(x[3], 2)) + ")", axis=1)
    df_data['To'] = df_data[['To', 'R2', 'G2', 'B2']].apply(
        lambda x: "RGB(" + str(round(x[1], 2)) + "," + str(round(x[2], 2)) + "," + str(round(x[3], 2)) + ")", axis=1)

    #initializing the style of cells in columns from and to
    custom = []

    #Filling of custom variable style
    for r in df_data['From']:
        custom.append(
            {'if': {'column_id': 'From', 'filter_query': '{From} eq "' + str(r) + '"'}, 'backgroundColor': str(r),
             'color': str(r), })
    for r in df_data['To']:
        custom.append({'if': {'column_id': 'To', 'filter_query': '{To} eq "' + str(r) + '"'}, 'backgroundColor': str(r),
                       'color': str(r), })

    custom.append({'if': {'column_id': 'From', 'filter_query': '{From} eq "RGB(255.0,255.0,255.0)"'},
                   'backgroundColor': '#262B3D', 'color': '#262B3D', })
    custom.append(
        {'if': {'column_id': 'To', 'filter_query': '{To} eq "RGB(255.0,255.0,255.0)"'}, 'backgroundColor': '#262B3D',
         'color': '#262B3D', })

    custom.append(
        {'if': {'column_id': 'From', 'filter_query': '{From} eq "RGB(nan,nan,nan)"'}, 'backgroundColor': '#262B3D',
         'color': '#262B3D', })
    custom.append(
        {'if': {'column_id': 'To', 'filter_query': '{To} eq "RGB(nan,nan,nan)"'}, 'backgroundColor': '#262B3D',
         'color': '#262B3D', })

    #If RMs initial color analyse is colled :
    if function == "RMs_color":

        #round the PURE RAW MATERIAL QUANTITY and the dosage
        df_data['PURE RAW MATERIAL QUANTITY'] = df_data[['PURE RAW MATERIAL QUANTITY', 'dosage', 'Dilution']].apply(
            lambda x: round(x[1] * x[2] / 100, 4), axis=1)
        df_data['dosage'] = round(df_data['dosage'], 4)

        #Droping columns we don't need
        df_data = df_data.drop(['Dilution', 'Solvent', 'R1', 'G1', 'B1', 'R2', 'G2', 'B2'], axis=1)

        #reindex IPC column
        df_data = df_data.rename_axis('IPC').reset_index()

        #Setting new order
        new_order = [2, 0, 1, 15, 13, 14, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        df_data = df_data[df_data.columns[new_order]]

        #Displaying the category of columns we want to display
        if 0 not in value:
            df_data = df_data.drop(['From', 'To', 'RM color', 'Absolute Y/N', 'Aspect'], axis=1)
        if 1 not in value:
            df_data = df_data.drop(['EDT color', 'Concentration tested in EDT (%)', '2w60°C', '4w50°C', '16hST'],
                                   axis=1)
        if 2 not in value:
            df_data = df_data.drop(['Limit concentration to see a color impact (%)'], axis=1)
        if 3 not in value:
            df_data = df_data.drop(['Replacement'], axis=1)

        df_data=df_data.rename(columns={'dosage': 'Quantity %'})

    #If comparator function with criteria RMs color is called :
    elif function == "comparator_RMs_color":

        #Droping columns we don't need
        df_data = df_data.drop(['dosage', 'Dilution', 'Solvent', 'R1', 'G1', 'B1', 'R2', 'G2', 'B2'], axis=1)

        #reindex IPC columns
        df_data = df_data.rename_axis('IPC').reset_index()

        #Setting new order
        new_order = [0, 12, 13, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 1]
        df_data = df_data[df_data.columns[new_order]]

    #return
    #df_data : data formula results
    #custom : styling of columns from and to of datatable result
    return df_data, custom

def treatment_schiff_base(df_data_final, df_ing, df_explosion_nat, df_BDS_natural, df_BDS, df_methyl_indirect,
                          df_methyl_direct):
    """
    FUNCTION : Schiff Base

    Input :
    - df_data_final : data formula
    - df_ing : Raw materials general DataBase
    - df_explosion_nat : explosion natural DataBase
    - df_BDS_natural : aldehyde natural DataBase
    - df_BDS : aldehyde database
    - df_methyl_indirect : methyl indirect database
    - df_methyl_direct : methyl direct database

    Output :
    - df_data_final : data formula result
    - indirect_aldehydes_class1 : list of raw materials indirect aldehyde of class 1
    - direct_aldehydes_class1 : list of raw materials direct aldehyde of class 1
    - indirect_aldehydes_class2 : list of raw materials indirect aldehyde of class 2
    - direct_aldehydes_class2 : list of raw materials direct aldehyde of class 2
    - indirect_aldehydes_class3 : list of raw materials indirect aldehyde of class 3
    - direct_aldehydes_class3 : list of raw materials direct aldehyde of class 3
    - IPC_indirect_methyl : list of raw materials IPC of indirect methyl
    - IPC_direct_methyl : list of raw materials IPC of direct methyl
    - total_aldehyde : sum of aldehyde in formula (class 1 and 2)
    - total_methyl : sum of methyl direct and indirect in formula
    - primary_aldehyde : sum of aldehyde class 1 in formula
    - secondary_aldehyde : sum of aldehyde class 2 in formula
    - tertiary_aldehyde : sum of aldehyde class 3 in formula
    - direct_methyl : sum of direct methyl in formula
    - indirect_methyl : sum of indirect methyl in formula
    """
    #We join the data formula with the general daatabase to recover the IPC, Name and Dilution columns
    df_data_final = df_data_final.join(df_ing[['IPC', 'Name', 'Dilution (%)']].set_index('IPC'))

    #We set a new dataframe of data formula with only dosage and dilution columns with index IPC
    df_data = df_data_final[['dosage', 'Dilution (%)']]

    #-----------------Treatment of indirect aldehyde-------------------
    #Exploding the natural raw materials by joining with explosion natural database
    df_explosion_natural = df_data.join(df_explosion_nat.set_index('IPC'))

    #Droping NaN line
    df_explosion_natural = df_explosion_natural.dropna(axis=0)

    #Droping column Name
    df_explosion_natural = df_explosion_natural.drop('Name', axis=1)

    #Reindexing IPC
    df_explosion_natural = df_explosion_natural.rename_axis('IPC').reset_index()

    #Rename some columns
    df_explosion_natural = df_explosion_natural.rename(columns={'Peak Name': 'Name', 'NAME': 'NAME MP'})

    #Looking for indirect aldehyde if data df_explosion_natural by joining with database aldehyde by criteria 'Name'
    df_indirect_aldehydes = df_explosion_natural.set_index("Name").join(df_BDS_natural.set_index('NAME'))

    #Droping NaN line
    df_indirect_aldehydes = df_indirect_aldehydes.dropna(axis=0)

    #reindex Name
    df_indirect_aldehydes = df_indirect_aldehydes.rename_axis('Name').reset_index()

    #Listing indirect aldehyde with different class
    indirect_aldehydes_class1 = df_indirect_aldehydes.loc[df_indirect_aldehydes['ALDEHYDE CLASS'] == 1]
    indirect_aldehydes_class1 = indirect_aldehydes_class1['IPC ald']

    indirect_aldehydes_class2 = df_indirect_aldehydes.loc[df_indirect_aldehydes['ALDEHYDE CLASS'] == 2]
    indirect_aldehydes_class2 = indirect_aldehydes_class2['IPC ald']

    indirect_aldehydes_class3 = df_indirect_aldehydes.loc[df_indirect_aldehydes['ALDEHYDE CLASS'] == 3]
    indirect_aldehydes_class3 = indirect_aldehydes_class3['IPC ald']

    #-----------------Treatment of direct aldehyde-------------------
    #Looking for direct aldehydes by joining formula data with aldehyde database with index IPC
    df_direct_aldehydes = df_data.join(df_BDS.set_index('IPC'))

    #Droping NaN line
    df_direct_aldehydes = df_direct_aldehydes.dropna(axis=0)

    #Reindexing IPC
    df_direct_aldehydes = df_direct_aldehydes.rename_axis('IPC').reset_index()

    #Listing direct aldehyde with different class
    direct_aldehydes_class1 = df_direct_aldehydes.loc[df_direct_aldehydes['ALDEHYDE CLASS'] == 1]
    direct_aldehydes_class1 = direct_aldehydes_class1['IPC']

    direct_aldehydes_class2 = df_direct_aldehydes.loc[df_direct_aldehydes['ALDEHYDE CLASS'] == 2]
    direct_aldehydes_class2 = direct_aldehydes_class2['IPC']

    direct_aldehydes_class3 = df_direct_aldehydes.loc[df_direct_aldehydes['ALDEHYDE CLASS'] == 3]
    direct_aldehydes_class3 = direct_aldehydes_class3['IPC']

    #-----------------Treatment of indirect methyl-------------------
    #Looking for direct methyl anthranilate in data formula with methyl direct database with index IPC
    df_indirect_methyl = df_data.join(df_methyl_indirect.set_index('IPC'))

    #Droping NaN line
    df_indirect_methyl = df_indirect_methyl.dropna(axis=0)

    #Reindexing IPC
    df_indirect_methyl = df_indirect_methyl.rename_axis('IPC').reset_index()

    #Listing indirect methyl anthranilate
    IPC_indirect_methyl = df_indirect_methyl['IPC']

    #-----------------Treatment of direct methyl----------------------
    #Looking for indirect methyl anthranilate in data formula with methyl indirect database with index IPC
    df_direct_methyl = df_data.join(df_methyl_direct.set_index('IPC'))

    #Droping NaN line
    df_direct_methyl = df_direct_methyl.dropna(axis=0)

    #Reindexing IPC
    df_direct_methyl = df_direct_methyl.rename_axis('IPC').reset_index()

    #Listing direct methyl anthranilate
    IPC_direct_methyl = df_direct_methyl['IPC']

    #Creating new columns usefull for the results
    df_data_final = df_data_final.reindex(columns=[*df_data_final.columns.tolist(),
                                                   *['Pure raw material quantity %', 'Indirect Aldehydes',
                                                     'Indirect Methyl Anthranilate']])

    #Calculating the Pure raw material quantity in percentage
    df_data_final['Pure raw material quantity %'] = df_data_final[
        ['Pure raw material quantity %', 'dosage', 'Dilution (%)']].apply(lambda x: round(x[1] * x[2] / 100, 3), axis=1)

    #Reindexing IPC
    df_data_final = df_data_final.rename_axis('IPC').reset_index()

    #browsing all the data raw materials in the formula
    for i, value in enumerate(df_data_final['IPC']):
        #Check all the indirect aldehyde in formula data and adding line of decomposition of indirect aldehyde
        for j, value_check in enumerate(df_indirect_aldehydes['IPC']):
            if value == value_check:
                df_data_final = df_data_final.append(pd.DataFrame(
                    {"IPC": df_indirect_aldehydes.loc[j, 'IPC ald'], "Name": df_indirect_aldehydes.loc[j, 'Name'],
                     'Indirect Aldehydes': round(
                         df_indirect_aldehydes.loc[j, 'dosage'] * df_indirect_aldehydes.loc[j, 'GS Area%'] / 100, 6)},
                    index=[i]))

        #Check all the indirect methyl in formula data and adding line of decomposition of indirect methyl
        for j, value_check in enumerate(df_indirect_methyl['IPC']):
            if value == value_check:
                df_data_final.loc[i, 'Indirect Methyl Anthranilate'] = round(
                    df_indirect_methyl.loc[j, 'dosage'] * df_indirect_methyl.loc[j, '% GC'] / 100, 6)

    #Reindexing all index in df_data_final
    df_data_final = df_data_final.reset_index()

    #Sorting the data by adding the decomposition of indirect aldehyde and methyl after the raw material target
    df_data_final['col_sort'] = np.arange(len(df_data_final))
    df_data_final = df_data_final.sort_values(by=['index', 'col_sort']).drop(['index', 'col_sort'], axis=1)

    #Round the dosage with 4 number after comma
    df_data_final['dosage'] = round(df_data_final['dosage'], 4)

    #Calculating all the totals of aldehyde by different class
    df_total_indirect_aldehyde = df_indirect_aldehydes[['dosage', 'GS Area%', 'ALDEHYDE CLASS']]
    df_total_indirect_aldehyde = df_total_indirect_aldehyde.reindex(
        columns=[*df_total_indirect_aldehyde.columns.tolist(), *['new dosage']])
    # df_total_indirect_aldehyde['new dosage']=df_total_indirect_aldehyde[['new dosage','dosage','GS Area%','ALDEHYDE CLASS']].apply(lambda x: round(x[1]*x[2]/100 if float(x[2])!=3 else 0,3),axis=1)
    df_total_indirect_aldehyde['new dosage'] = df_total_indirect_aldehyde[['new dosage', 'dosage', 'GS Area%']].apply(
        lambda x: round(x[1] * x[2] / 100, 3), axis=1)

    primary_aldehyde = round(
        df_total_indirect_aldehyde[df_total_indirect_aldehyde['ALDEHYDE CLASS'] == 1]['new dosage'].sum() +
        df_direct_aldehydes[df_direct_aldehydes['ALDEHYDE CLASS'] == 1]['dosage'].sum(), 4)
    secondary_aldehyde = round(
        df_total_indirect_aldehyde[df_total_indirect_aldehyde['ALDEHYDE CLASS'] == 2]['new dosage'].sum() +
        df_direct_aldehydes[df_direct_aldehydes['ALDEHYDE CLASS'] == 2]['dosage'].sum(), 4)
    tertiary_aldehyde = round(
        df_total_indirect_aldehyde[df_total_indirect_aldehyde['ALDEHYDE CLASS'] == 3]['new dosage'].sum() +
        df_direct_aldehydes[df_direct_aldehydes['ALDEHYDE CLASS'] == 3]['dosage'].sum(), 4)
    total_aldehyde = round(
        df_total_indirect_aldehyde[df_total_indirect_aldehyde['ALDEHYDE CLASS'] != 3]['new dosage'].sum() +
        df_direct_aldehydes[df_direct_aldehydes['ALDEHYDE CLASS'] != 3]['dosage'].sum(), 4)

    #Calculating all the totals of direct and indirect methyl anthranilate
    direct_methyl = round(df_direct_methyl['dosage'].sum(), 4)
    indirect_methyl = round(df_data_final['Indirect Methyl Anthranilate'].sum(), 4)
    total_methyl = round(df_data_final['Indirect Methyl Anthranilate'].sum() + df_direct_methyl['dosage'].sum(), 4)
    # new_order=[1,4,6,5,2,3]
    # df_data_final = df_data_final[df_data_final.columns[new_order]]

    df_data_final=df_data_final.rename(columns={'dosage': 'Quantity %'})

    #return the data formula result (df_data_final) and all the totals of aldehyde and methyl in different category
    return df_data_final, indirect_aldehydes_class1, direct_aldehydes_class1, indirect_aldehydes_class2, direct_aldehydes_class2, indirect_aldehydes_class3, direct_aldehydes_class3, IPC_indirect_methyl, IPC_direct_methyl, total_aldehyde, total_methyl, primary_aldehyde, secondary_aldehyde, tertiary_aldehyde, direct_methyl, indirect_methyl



def treatment_comparator(df_data, df_output, df_ing, df_aox, df_RMs_color, df_RMs_warning, value_ref, value_type,
                         value_edt, value_color, value_comparator, formulas, formulas2):
    """
    FUNCTION : Comparator

    Input :
    - df_data : data formulas
    - df_output : data formulas with columns IPC, formula reference and formulas comparation
    - df_ing : general raw materials database
    - df_aox : antioxidant origin database
    - df_RMs_color : Raw materials color  database
    - df_RMs_warning : risk assessment of raw material database
    - value_ref : reference formula chosen by the user
    - value_type : criteria of comparation chosen by the user
    - value_edt : dosage for edt criteria
    - value_color : category of datatable for Rms color criteria
    - value_comparator : comparation formulas chosen by the user
    - formulas : data formula reference
    - formulas2 : data formulas comparation

    Output :
    - df_output : dataframe formulas result
    - custom : style of datatable displaying
    """
        #if criteria is OIL :
    if value_type == "OIL":

        #Replacing NaN value by ""
        df_output = df_output.replace(np.nan, "")

        #Looking for raw materials data by joining df_ing database by IPC
        df_output = df_output.set_index('IPC').join(df_ing[['IPC', 'Name']].set_index('IPC'))

        #Reordering columns of df_output
        cols = df_output.columns.tolist()
        cols = cols[-1:] + cols[:-1]
        df_output = df_output[cols]

        #round OIL dosage
        df_output = df_output.applymap(lambda x: round(x, 4) if isinstance(x, float) == True else x)

        #Setting custom style to null
        custom = []

        #Reindexing IPC
        df_output=df_output.rename_axis('IPC').reset_index()

        return df_output, custom

    #if criteria is EDT :
    if value_type == 'EDT':

        #Setting columns display for columns_formulas and columns
        columns_formulas = [formulas[value_ref]] + list(pd.Series(formulas2).iloc[value_comparator])
        columns = ['IPC'] + [formulas[value_ref]] + list(pd.Series(formulas2).iloc[value_comparator])

        #Setting list col
        col = list(pd.Series(formulas2).iloc[value_comparator])

        #Redefining df_output by import df_data (formulas data) with variable columns (IPC, formula reference and formulas comparation name)
        df_output = pd.DataFrame(df_data, columns=columns)

        #Replacing NaN value by ""
        df_output = df_output.replace(np.nan, "")

        #Looking for raw materials data by joining df_ing database by IPC
        df_output = df_output.set_index('IPC').join(df_ing[['IPC', 'Name']].set_index('IPC'))

        #Reordering columns of df_output
        cols = df_output.columns.tolist()
        cols = cols[-1:] + cols[:-1]
        df_output = df_output[cols]

        #Setting EDT dosage with variable value_edt[i] which is edt dosage for each formula
        for i, col in enumerate(columns_formulas):
            df_output[col] = df_output[col].apply(
                lambda x: round(x / 100 * float(value_edt[i]), 4) if isinstance(x, float) == True else x)

        ##Setting custom style to null
        custom = []

        #Reindexing IPC
        df_output=df_output.rename_axis('IPC').reset_index()

        return df_output, custom

    #if criteria is AOX :
    if value_type == 'AOX':
        #Setting custom style to null
        custom = []

        #Calling treatment_AOX procedure
        df_output, df_direct, df_direct_bis,aox_in_direct = treatment_AOX([], df_aox, df_output, df_data, "comparator_AOX")

        #Setting list for col to reorder columns datatable
        col = [formulas[value_ref]]+list(pd.Series(formulas2).iloc[value_comparator])

        #Round dosage of data formulas
        df_output[col] = round(df_output[col], 4)

        #Reindexing IPC
        df_output=df_output.rename_axis('IPC').reset_index()

        return df_output, custom

    #if criteria is RM color :
    elif value_type == "RM color":

        #Replacing NaN value by ""
        df_output = df_output.replace(np.nan, "")

        #Stocking df_data dict to df_data dataframe with columns dosage and IPC
        df_data = pd.DataFrame(df_data, columns=['dosage', 'IPC'])

        #Looking for RMs color data of raw materials in df_data by joining RMs color database with index IPC
        df_data = df_data.set_index('IPC').join(df_RMs_color.set_index('IPC'))

        #Droping the column we don't need
        df_data = df_data.drop('Synthetic/Natural', axis=1)

        #Calling treatment_RMs_color procedure
        df_data, custom = treatment_RMs_color(df_data, [], "comparator_RMs_color")

        #Joining df_output with raw materials of formula reference and formulass comparation with df_data which got the result data of RM color
        df_output = df_output.set_index('IPC').join(df_data.set_index('IPC'))

        #Reordering df_output
        cols = df_output.columns.tolist()
        cols = cols[-1:] + cols[:-1]
        df_output = df_output[cols]

        #Setting col : list of formula reference and formulas comparation
        col = [formulas[value_ref]]+list(pd.Series(formulas2).iloc[value_comparator])

        #Round the dosage of formula reference and formulas comparation
        df_output[col] = round(df_output[col], 4)

        #Dispalying columns in function of category (value_color) chosen by the user
        if [0] not in value_color:
            df_output = df_output.drop(['From', 'To', 'RM color', 'Absolute Y/N', 'Aspect'], axis=1)
        if [1] not in value_color:
            df_output = df_output.drop(['EDT color', 'Concentration tested in EDT (%)', '2w60°C', '4w50°C', '16hST'],
                                       axis=1)
        if [2] not in value_color:
            df_output = df_output.drop(['Limit concentration to see a color impact (%)'], axis=1)
        if [3] not in value_color:
            df_output = df_output.drop(['Replacement'], axis=1)

        #Reindexing IPC
        df_output=df_output.rename_axis('IPC').reset_index()

        return df_output, custom

    #if criteria is Risk Assessment :
    elif value_type == "Risk Assessment":
        #Setting custom to null
        custom = []

        #Replacing NaN value by ""
        df_output = df_output.replace(np.nan, "")

        #Stocking df_data dict to df_data dataframe with columns dosage and IPC
        df_data = pd.DataFrame(df_data, columns=['dosage', 'IPC'])

        #Calling treatment_risk_assessment procedure
        df_data, df_data_2, df_data_warning = treatment_risk_assessment(df_data, [], df_ing, df_RMs_warning, [], [], [],
                                                                        "comparator_risk_assessment")

        #Redefining df_output by import df_data (formulas data) with variable columns (IPC, formula reference and formulas comparation name)
        df_output = df_output.set_index('IPC').join(df_data.set_index('IPC'))

        #Reordering df_output
        cols = df_output.columns.tolist()
        cols = cols[-1:] + cols[:-1]
        df_output = df_output[cols]

        #Round the dosage of raw materials
        col = [formulas[value_ref]]+list(pd.Series(formulas2).iloc[value_comparator])
        df_output[col] = round(df_output[col], 4)

        #Reindexing IPC
        df_output=df_output.rename_axis('IPC').reset_index()

        return df_output, custom

    #if criteria is in column of df_ing database
    for valeur in df_ing.columns:
        if value_type == valeur:

            #Setting custom to null
            custom = []

            #Replacing NaN value by ""
            df_output = df_output.replace(np.nan, "")

            #Looking for raw materials with criteria value
            df_output = df_output.set_index('IPC').join(df_ing[['IPC', valeur, 'Name']].set_index('IPC'))

            #Reordering df_output
            cols = df_output.columns.tolist()
            cols = cols[-1:] + cols[:-1]
            df_output = df_output[cols]

            #Round the dosage of raw materials
            col = [formulas[value_ref]]+list(pd.Series(formulas2).iloc[value_comparator])
            df_output[col] = round(df_output[col], 4)

            #Reindexing IPC
            df_output=df_output.rename_axis('IPC').reset_index()

            return df_output, custom

def treatment_data_display(df_data_dt, df_ing):
    """
    FUNCTION : Data display

    Input :
    - df_data_dt : formula data dataframe
    - df_ing : raw materials database

    Output :
    - df_data_dt : formula data result dataframe
    """

    #Looking for raw materials in database df_ing
    df_data_dt = df_data_dt.join(df_ing.set_index('IPC'))

    #Reindex IPC
    df_data_dt = df_data_dt.rename_axis('IPC').reset_index()

    #Round dosage of data
    df_data_dt['dosage'] = round(df_data_dt['dosage'], 3)

    #Setting new column : PURE QUANTITY %
    df_data_dt = df_data_dt.reindex(columns=[*df_data_dt.columns.tolist(), *['PURE QUANTITY %']])

    #Calculating the data of column PURE QUANTITY %
    df_data_dt['PURE QUANTITY %'] = df_data_dt[['PURE QUANTITY %', 'dosage', 'Dilution (%)']].apply(
        lambda x: round(x[1] * x[2] / 100, 3), axis=1)

    df_data_dt=df_data_dt.rename(columns={'dosage': 'Quantity %'})

    return df_data_dt


def treatment_hydro_alcoholic(df_data, df_methyl, df_methyl_direct, value, value1, value2, value3, value4, value5,
                              value6, value7, value8, value9, value10, value11, value12):
    """
    FUNCTION : hydrolyse solution alcoholic

    Input :
    - df_data : data formula
    - df_methyl : methyl anthranilate database
    - df_methyl_direct : methyl anthranilate in direct database
    - value : Variable in form (alcoholic solution criteria)
    - value1 : Variable in form (Client / Project)
    - value2 : Variable in form (Formula name)
    - value3 : Variable in form (Fragrance % in EDT)
    - value4 : Variable in form (Ethanol % in EDT)
    - value5 : Variable in form (Maturation Time)
    - value6 : Variable in form (Maceration Time)
    - value7 : Variable in form (Date)
    - value8 : Variable in form (Additive)
    - value9 : Variable in form (BHT)
    - value10 : Variable in form (Covabsorb)
    - value11 : Variable in form (Parsol 1789)
    - value12 : Variable in form (Tinogard Q)

    No Output
    We generate a docx file
    """
    if value == 'Alcoholic solution':

        #Coding to make docx file
        document = Document()

        document.add_picture('assets/Annotation.png', width=Inches(7))
        p1 = document.add_paragraph()
        p1.alignment = WD_ALIGN_PARAGRAPH.CENTER
        p1.paragraph_format.space_before = Pt(3)
        p1.paragraph_format.space_after = Pt(1.5)
        client = p1.add_run(str(value1))
        client_font = client.font
        client.bold = True
        client.italic = True
        client_font.size = Pt(13)

        p1 = document.add_paragraph()
        p1.alignment = WD_ALIGN_PARAGRAPH.CENTER
        p1.paragraph_format.space_before = Pt(2)
        p1.paragraph_format.space_after = Pt(3)
        formula = p1.add_run(str(value2))
        formula.bold = True
        formula.italic = True

        p1 = document.add_paragraph()
        p1.alignment = WD_ALIGN_PARAGRAPH.CENTER
        p1.paragraph_format.space_before = Pt(2)
        p1.paragraph_format.space_after = Pt(3)
        p1.add_run('Fragrance :    ')
        p1.add_run(str(value3))
        p1.add_run('    %w/w')
        p1 = document.add_paragraph()
        p1.alignment = WD_ALIGN_PARAGRAPH.CENTER
        p1.paragraph_format.space_before = Pt(2)
        p1.paragraph_format.space_after = Pt(5)
        p1.add_run('Alcohol :    ')
        p1.add_run(str(value4))
        p1.add_run('    %w/w ')

        if value9 == None:
            value9 = 0
        if value10 == None:
            value10 = 0
        if value11 == None:
            value11 = 0
        if value12 == None:
            value12 = 0

        additive = [['BHT', value9], ['Covabsorb', value10], ['Parsol 1789', value11], ['Tinogard Q', value12]]
        records = (
            ('formula name', 'Parfum/Fragrance', str(value3)),
            ('No Denatured Alcohol ', 'Alcohol', str(float(value4) - float(value9))),
            ('Deionized Water ', 'Aqua/Water',
             str(100 - float(value3) - float(value4) - float(value10) - float(value11) - float(value12)))
        )

        a = 0

        for i, j in additive:
            if j != 0:
                a = a + 1
                records2 = list(records)
                records2.append(("", i, j))
                records = tuple(records2)

        table = document.add_table(rows=1, cols=3)
        hdr_cells = table.rows[0].cells
        hdr_cells[0].paragraphs[0].add_run('Raw material').bold = True
        hdr_cells[1].paragraphs[0].add_run('INCI').bold = True
        hdr_cells[2].paragraphs[0].add_run('% w/w').bold = True
        for raw, inci, ww in records:
            row_cells = table.add_row().cells
            row_cells[0].text = raw
            row_cells[1].text = inci
            row_cells[2].text = str(ww)
        table.style = 'TableGrid'

        if a != 0:
            if a != 1:
                for i in range(1, a):
                    c = table.cell(3 + i, 0)
                    d = table.cell(4 + i, 0)
                    E = c.merge(d)
                E.text = 'Additive'
            else:
                table.cell(4, 0).text = 'Additive'

        table2 = document.add_table(rows=1, cols=3)
        hdr_cells2 = table2.rows[0].cells
        a = table2.cell(0, 0)
        b = table2.cell(0, 1)

        hdr_cells2[2].paragraphs[0].add_run('100').bold = True
        A = a.merge(b)
        A.text = 'TOTAL'
        Abold = A.paragraphs[0].runs[0]
        Abold.font.bold = True
        table2.style = 'TableGrid'

        p = document.add_paragraph()
        plus = p.add_run('*The concentrate density is considered to being equal at 1.')
        plus.italic = True
        plus_font = plus.font
        plus_font.size = Pt(9)

        p = document.add_paragraph()
        material = p.add_run('Material:')
        material.bold = True
        material_font = material.font
        material_font.color.rgb = RGBColor(0, 102, 204)
        p.paragraph_format.space_before = Pt(1)
        p.paragraph_format.space_after = Pt(1)
        p = document.add_paragraph()
        p.add_run('- Glass jar')
        p.paragraph_format.space_before = Pt(1)
        p.paragraph_format.space_after = Pt(1)
        p = document.add_paragraph()
        p.add_run('- Press-filter (supplier Amafilter group / reference = PFC 10 – 0,5-1,5µm)')
        p.paragraph_format.space_before = Pt(1)
        p.paragraph_format.space_after = Pt(1)

        p = document.add_paragraph()
        p = document.add_paragraph()
        manufacture = p.add_run('Manufacturing Process:')
        manufacture.bold = True
        manufacture_font = manufacture.font
        manufacture_font.color.rgb = RGBColor(0, 102, 204)
        p.paragraph_format.space_before = Pt(1)
        p.paragraph_format.space_after = Pt(1)
        p = document.add_paragraph()
        p.add_run('Mix perfume concentrate and alcohol. Then add distilled water under agitation. ')
        p.paragraph_format.space_before = Pt(1)
        p.paragraph_format.space_after = Pt(1)
        p = document.add_paragraph()
        p.paragraph_format.space_before = Pt(1)
        p.paragraph_format.space_after = Pt(1)
        p.add_run('Before filtration on press filter, the solution must be cooled down to -2°C / 5°C during 2 hours. ')

        p = document.add_paragraph()
        p = document.add_paragraph()
        maturation = p.add_run('Maturation time:   ')
        maturation.bold = True
        maturation_font = maturation.font
        maturation_font.color.rgb = RGBColor(0, 102, 204)
        p.add_run(value5)
        p.add_run(' weeks')
        p.paragraph_format.space_before = Pt(1)
        p.paragraph_format.space_after = Pt(1)
        p = document.add_paragraph()
        maceration = p.add_run('Maceration time:   ')
        maceration.bold = True
        maceration_font = maceration.font
        maceration_font.color.rgb = RGBColor(0, 102, 204)
        p.add_run(value6)
        p.add_run(' weeks')
        p.paragraph_format.space_before = Pt(1)
        p.paragraph_format.space_after = Pt(1)

        p = document.add_paragraph()
        p = document.add_paragraph()
        p.add_run('Neuilly-sur-seine,')
        p.paragraph_format.space_before = Pt(1)
        p.paragraph_format.space_after = Pt(1)
        p = document.add_paragraph()
        p.add_run('DATE')
        p.add_run('          ')
        p.add_run(value7)
        p.paragraph_format.space_before = Pt(1)
        p.paragraph_format.space_after = Pt(10)

        p2 = document.add_paragraph()
        text = p2.add_run(
            'This formula is furnished only for your guidance. We urge you to make your own careful tests of it. We make no warranties to you or any third party as to patent infringement. We will not accept responsibility of any kind in connection with the use of formula.')
        p2.alignment = WD_ALIGN_PARAGRAPH.CENTER
        p2_font = text.font
        p2.bold = True
        p2_font.size = Pt(9)
        p2_font.color.rgb = RGBColor(128, 128, 128)

        my_image = document.add_picture('assets/iff.png', width=Inches(0.5))
        last_paragraph = document.paragraphs[-1]
        last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER

        document.add_page_break()
        desktop = os.path.normpath(os.path.expanduser("~/demo.docx"))
        document.save(desktop)

        open_file(desktop)
        raise PreventUpdate

    if value == 'Alcoholic solution P20/P80':
        # check si methyl present
        df_data = pd.DataFrame(df_data, columns=['dosage'])
        df_data = df_data.join(df_methyl.set_index('IPC'))
        df_data = df_data.dropna(axis=0)

        if df_data.empty == False:

            document = Document()

            document.add_picture('assets/Annotation.png', width=Inches(7))
            p1 = document.add_paragraph()
            p1.alignment = WD_ALIGN_PARAGRAPH.CENTER
            p1.paragraph_format.space_before = Pt(3)
            p1.paragraph_format.space_after = Pt(1.5)
            client = p1.add_run(str(value1))
            client_font = client.font
            client.bold = True
            client.italic = True
            client_font.size = Pt(13)

            p1 = document.add_paragraph()
            p1.alignment = WD_ALIGN_PARAGRAPH.CENTER
            p1.paragraph_format.space_before = Pt(2)
            p1.paragraph_format.space_after = Pt(3)
            formula = p1.add_run(str(value2))
            formula.bold = True
            formula.italic = True

            p1 = document.add_paragraph()
            p1.alignment = WD_ALIGN_PARAGRAPH.CENTER
            p1.paragraph_format.space_before = Pt(2)
            p1.paragraph_format.space_after = Pt(3)
            p1.add_run('Fragrance :    ')
            p1.add_run(str(value3))
            p1.add_run('    %w/w')
            p1 = document.add_paragraph()
            p1.alignment = WD_ALIGN_PARAGRAPH.CENTER
            p1.paragraph_format.space_before = Pt(2)
            p1.paragraph_format.space_after = Pt(5)
            p1.add_run('Alcohol :    ')
            p1.add_run(str(value4))
            p1.add_run('    %w/w ')

            if value9 == None:
                value9 = 0
            if value10 == None:
                value10 = 0
            if value11 == None:
                value11 = 0
            if value12 == None:
                value12 = 0

            additive = [['BHT', value9], ['Covabsorb', value10], ['Parsol 1789', value11], ['Tinogard Q', value12]]

            records = (
                ('formula name   P20*', '', str(float(value3) / 5)),
                ('formula name   P80*', '', str(float(value3) * (4 / 5))),
                ('No Denatured Alcohol ', 'Alcohol', str(float(value4) - float(value9))),
                ('Deionized Water ', 'Aqua/Water',
                 str(100 - float(value3) - float(value4) - float(value10) - float(value11) - float(value12)))
            )

            a = 0

            for i, j in additive:
                if j != 0:
                    a = a + 1
                    records2 = list(records)
                    records2.append(("", i, j))
                    records = tuple(records2)

            table = document.add_table(rows=1, cols=3)
            hdr_cells = table.rows[0].cells
            hdr_cells[0].paragraphs[0].add_run('Raw material').bold = True
            hdr_cells[1].paragraphs[0].add_run('INCI').bold = True
            hdr_cells[2].paragraphs[0].add_run('% w/w').bold = True
            for raw, inci, ww in records:
                row_cells = table.add_row().cells
                row_cells[0].text = raw
                row_cells[1].text = inci
                row_cells[2].text = str(ww)
            c = table.cell(1, 1)
            d = table.cell(2, 1)
            E = c.merge(d)
            E.text = 'Parfum/Fragrance'
            table.style = 'TableGrid'

            if a != 0:
                if a != 1:
                    for i in range(1, a):
                        g = table.cell(4 + i, 0)
                        h = table.cell(5 + i, 0)
                        I = g.merge(h)
                    I.text = 'Additive'
                else:
                    table.cell(5, 0).text = 'Additive'

            table2 = document.add_table(rows=1, cols=3)
            hdr_cells2 = table2.rows[0].cells
            a = table2.cell(0, 0)
            b = table2.cell(0, 1)

            hdr_cells2[2].paragraphs[0].add_run('100').bold = True
            A = a.merge(b)
            A.text = 'TOTAL'
            Abold = A.paragraphs[0].runs[0]
            Abold.font.bold = True
            table2.style = 'TableGrid'

            p = document.add_paragraph()
            plus = p.add_run('*The concentrate density is considered to being equal at 1.')
            plus.italic = True
            plus_font = plus.font
            plus_font.size = Pt(9)

            p = document.add_paragraph()
            material = p.add_run('Material:')
            material.bold = True
            material_font = material.font
            material_font.color.rgb = RGBColor(0, 102, 204)
            p.paragraph_format.space_before = Pt(1)
            p.paragraph_format.space_after = Pt(1)
            p = document.add_paragraph()
            p.add_run('- Glass jar')
            p.paragraph_format.space_before = Pt(1)
            p.paragraph_format.space_after = Pt(1)
            p = document.add_paragraph()
            p.add_run('- Press-filter (supplier Amafilter group / reference = PFC 10 – 0,5-1,5µm)')
            p.paragraph_format.space_before = Pt(1)
            p.paragraph_format.space_after = Pt(1)

            p = document.add_paragraph()
            p = document.add_paragraph()
            manufacture = p.add_run('Manufacturing Process:')
            manufacture.bold = True
            manufacture_font = manufacture.font
            manufacture_font.color.rgb = RGBColor(0, 102, 204)
            p.paragraph_format.space_before = Pt(1)
            p.paragraph_format.space_after = Pt(1)
            p = document.add_paragraph()
            p.add_run(
                'Mix p80 perfume concentrate and alcohol. Then add distilled water. Once the quite homogeneous mixture, add the p20 perfume concentrate under agitation. ')
            p.paragraph_format.space_before = Pt(1)
            p.paragraph_format.space_after = Pt(1)
            p = document.add_paragraph()
            p.paragraph_format.space_before = Pt(1)
            p.paragraph_format.space_after = Pt(1)
            p.add_run(
                'Before filtration on press filter, the solution must be cooled down to -2°C / 5°C during 2 hours.')

            p = document.add_paragraph()
            p = document.add_paragraph()
            maturation = p.add_run('Maturation time:   ')
            maturation.bold = True
            maturation_font = maturation.font
            maturation_font.color.rgb = RGBColor(0, 102, 204)

            p.add_run(value5)
            p.add_run(' weeks')
            p.paragraph_format.space_before = Pt(1)
            p.paragraph_format.space_after = Pt(1)
            p = document.add_paragraph()
            maceration = p.add_run('Maceration time:   ')
            maceration.bold = True
            maceration_font = maceration.font
            maceration_font.color.rgb = RGBColor(0, 102, 204)
            p.add_run(value6)
            p.add_run(' weeks')
            p.paragraph_format.space_before = Pt(1)
            p.paragraph_format.space_after = Pt(1)

            p = document.add_paragraph()
            p = document.add_paragraph()
            p.add_run('Neuilly-sur-seine,')
            p.paragraph_format.space_before = Pt(1)
            p.paragraph_format.space_after = Pt(1)
            p = document.add_paragraph()
            p.add_run('DATE')
            p.add_run('          ')
            p.add_run(value7)
            p.paragraph_format.space_before = Pt(1)
            p.paragraph_format.space_after = Pt(10)

            p2 = document.add_paragraph()
            text = p2.add_run(
                'This formula is furnished only for your guidance. We urge you to make your own careful tests of it. We make no warranties to you or any third party as to patent infringement. We will not accept responsibility of any kind in connection with the use of formula.')
            p2.alignment = WD_ALIGN_PARAGRAPH.CENTER
            p2_font = text.font
            p2.bold = True
            p2_font.size = Pt(9)
            p2_font.color.rgb = RGBColor(128, 128, 128)

            my_image = document.add_picture('assets/iff.png', width=Inches(0.5))
            last_paragraph = document.paragraphs[-1]
            last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER

            document.add_page_break()

            desktop = os.path.normpath(os.path.expanduser("~/Desktop"))
            document.save('demo.docx')

            open_file('demo.docx')
            raise PreventUpdate

        else:

            return {'display': 'block'}, True

    if value == 'Alcoholic solution SMA':

        #Checking if methyl anthranilate is present in the formula
        df_data = pd.DataFrame(df_data, columns=['dosage'])
        df_data = df_data.join(df_methyl_direct.set_index('IPC'))
        df_data = df_data.dropna(axis=0)
        if df_data.empty == False:

            #Creating column PURE QUANTITY %
            df_data = df_data.reindex(columns=[*df_data.columns.tolist(), *['PURE QUANTITY %']])

            #Calculating PURE QUANTITY %
            df_data['PURE QUANTITY %'] = df_data[['PURE QUANTITY %', 'dosage', '% GC']].apply(
                lambda x: round(x[1] * x[2] / 100, 3), axis=1)

            #Suming methyl anthranilate in concentrate and solution
            methyl_in_concentrate = df_data['PURE QUANTITY %'].sum()
            methyl_in_solution = (methyl_in_concentrate * float(value3) / 100)

            #Setting some parameters in function of methyl anthranilate quantitiy in solution
            if methyl_in_solution < 0.01:
                alcohol = 100
                alcool = 1
            elif methyl_in_solution < 0.1:
                alcohol = 10
                alcool = 10
            elif methyl_in_solution >= 0.1:
                alcohol = 1
                alcool = 100

            methyl = round(methyl_in_solution * alcohol, 4)

            #Coding to make docx file
            document = Document()

            document.add_picture('assets/Annotation.png', width=Inches(7))
            p1 = document.add_paragraph()
            p1.alignment = WD_ALIGN_PARAGRAPH.CENTER
            p1.paragraph_format.space_before = Pt(3)
            p1.paragraph_format.space_after = Pt(1.5)
            client = p1.add_run(str(value1))
            client_font = client.font
            client.bold = True
            client.italic = True
            client_font.size = Pt(13)

            p1 = document.add_paragraph()
            p1.alignment = WD_ALIGN_PARAGRAPH.CENTER
            p1.paragraph_format.space_before = Pt(2)
            p1.paragraph_format.space_after = Pt(3)
            formula = p1.add_run(str(value2))
            formula.bold = True
            formula.italic = True

            p1 = document.add_paragraph()
            p1.alignment = WD_ALIGN_PARAGRAPH.CENTER
            p1.paragraph_format.space_before = Pt(2)
            p1.paragraph_format.space_after = Pt(3)
            p1.add_run('Fragrance :    ')
            p1.add_run(str(value3))
            p1.add_run('    %w/w')
            p1 = document.add_paragraph()
            p1.alignment = WD_ALIGN_PARAGRAPH.CENTER
            p1.paragraph_format.space_before = Pt(2)
            p1.paragraph_format.space_after = Pt(5)
            p1.add_run('Alcohol :    ')
            p1.add_run(str(value4))
            p1.add_run('    %w/w ')

            if value9 == None:
                value9 = 0
            if value10 == None:
                value10 = 0
            if value11 == None:
                value11 = 0
            if value12 == None:
                value12 = 0

            additive = [['BHT', value9], ['Covabsorb', value10], ['Parsol 1789', value11], ['Tinogard Q', value12]]

            records = (
                (str(value2) + '   SMA*', 'Parfum/Fragrance', str(float(value3) - methyl_in_solution)),
                ('No Denatured Alcohol ', 'Alcohol',
                 str((float(value4) - float(value9)) - (methyl - methyl_in_solution))),
                ('Methyl  Anthranilate @ ' + str(alcool) + '% Alcohol**', 'Methyl  Anthranilate', str(methyl)),
                ('Deionized Water', 'Aqua/Water',
                 str(100 - float(value3) - float(value4) - float(value10) - float(value11) - float(value12)))
            )

            a = 0

            for i, j in additive:
                if j != 0:
                    a = a + 1
                    records2 = list(records)
                    records2.append(("", i, j))
                    records = tuple(records2)

            table = document.add_table(rows=1, cols=3)
            hdr_cells = table.rows[0].cells
            hdr_cells[0].paragraphs[0].add_run('Raw material').bold = True
            hdr_cells[1].paragraphs[0].add_run('INCI').bold = True
            hdr_cells[2].paragraphs[0].add_run('% w/w').bold = True
            for raw, inci, ww in records:
                row_cells = table.add_row().cells
                row_cells[0].text = raw
                row_cells[1].text = inci
                row_cells[2].text = str(ww)

            table.style = 'TableGrid'

            if a != 0:
                if a != 1:
                    for i in range(1, a):
                        g = table.cell(4 + i, 0)
                        h = table.cell(5 + i, 0)
                        I = g.merge(h)
                    I.text = 'Additive'
                else:
                    table.cell(5, 0).text = 'Additive'

            table2 = document.add_table(rows=1, cols=3)
            hdr_cells2 = table2.rows[0].cells
            a = table2.cell(0, 0)
            b = table2.cell(0, 1)

            hdr_cells2[2].paragraphs[0].add_run('100').bold = True
            A = a.merge(b)
            A.text = 'TOTAL'
            Abold = A.paragraphs[0].runs[0]
            Abold.font.bold = True
            table2.style = 'TableGrid'

            p = document.add_paragraph()
            plus = p.add_run('*The concentrate density is considered to being equal at 1.')
            plus.italic = True
            plus_font = plus.font
            plus_font.size = Pt(9)
            p.paragraph_format.space_before = Pt(1)
            p.paragraph_format.space_after = Pt(1)
            p = document.add_paragraph()
            plus2 = p.add_run(
                '**Quantity of methyl anthranilate in concentrate (%) : ' + str(round(methyl_in_concentrate, 4)))

            plus2.italic = True
            plus2_font = plus2.font
            plus2_font.size = Pt(9)
            p.paragraph_format.space_before = Pt(1)
            p.paragraph_format.space_after = Pt(1)
            p = document.add_paragraph()
            plus3 = p.add_run(
                '***Global quantity of methyl anthranilate in solution (%) : ' + str(round(methyl_in_solution, 4)))
            plus3.italic = True
            plus3_font = plus3.font
            plus3_font.size = Pt(9)
            p.paragraph_format.space_before = Pt(1)
            p.paragraph_format.space_after = Pt(1)

            p = document.add_paragraph()
            material = p.add_run('Material:')
            material.bold = True
            material_font = material.font
            material_font.color.rgb = RGBColor(0, 102, 204)
            p.paragraph_format.space_before = Pt(1)
            p.paragraph_format.space_after = Pt(1)
            p = document.add_paragraph()
            p.add_run('- Glass jar')
            p.paragraph_format.space_before = Pt(1)
            p.paragraph_format.space_after = Pt(1)
            p = document.add_paragraph()
            p.add_run('- Press-filter (supplier Amafilter group / reference = PFC 10 – 0,5-1,5µm)')
            p.paragraph_format.space_before = Pt(1)
            p.paragraph_format.space_after = Pt(1)

            p = document.add_paragraph()
            p = document.add_paragraph()
            manufacture = p.add_run('Manufacturing Process:')
            manufacture.bold = True
            manufacture_font = manufacture.font
            manufacture_font.color.rgb = RGBColor(0, 102, 204)
            p.paragraph_format.space_before = Pt(1)
            p.paragraph_format.space_after = Pt(1)
            p = document.add_paragraph()
            p.add_run(
                'Mix perfume concentrate and alcohol. Then add distilled water. Once the quite homogeneous mixture, add the methyl anthranilate solution under agitation. ')
            p.paragraph_format.space_before = Pt(1)
            p.paragraph_format.space_after = Pt(1)
            p = document.add_paragraph()
            p.paragraph_format.space_before = Pt(1)
            p.paragraph_format.space_after = Pt(1)
            p.add_run(
                'Before filtration on press filter, the solution must be cooled down to -2°C / 5°C during 2 hours.')

            p = document.add_paragraph()
            p = document.add_paragraph()
            maturation = p.add_run('Maturation time:   ')
            maturation.bold = True
            maturation_font = maturation.font
            maturation_font.color.rgb = RGBColor(0, 102, 204)
            p.add_run(value5)
            p.add_run(' weeks')
            p.paragraph_format.space_before = Pt(1)
            p.paragraph_format.space_after = Pt(1)
            p = document.add_paragraph()
            maceration = p.add_run('Maceration time:   ')
            maceration.bold = True
            maceration_font = maceration.font
            maceration_font.color.rgb = RGBColor(0, 102, 204)
            p.add_run(value6)
            p.add_run(' weeks')
            p.paragraph_format.space_before = Pt(1)
            p.paragraph_format.space_after = Pt(1)

            p = document.add_paragraph()
            p = document.add_paragraph()
            p.add_run('Neuilly-sur-seine,')
            p.paragraph_format.space_before = Pt(1)
            p.paragraph_format.space_after = Pt(1)
            p = document.add_paragraph()
            p.add_run('DATE')
            p.add_run('          ')
            p.add_run(value7)
            p.paragraph_format.space_before = Pt(1)
            p.paragraph_format.space_after = Pt(10)

            p2 = document.add_paragraph()
            text = p2.add_run(
                'This formula is furnished only for your guidance. We urge you to make your own careful tests of it. We make no warranties to you or any third party as to patent infringement. We will not accept responsibility of any kind in connection with the use of formula.')
            p2.alignment = WD_ALIGN_PARAGRAPH.CENTER
            p2_font = text.font
            p2.bold = True
            p2_font.size = Pt(9)
            p2_font.color.rgb = RGBColor(128, 128, 128)

            my_image = document.add_picture('assets/iff.png', width=Inches(0.5))
            last_paragraph = document.paragraphs[-1]
            last_paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER

            document.add_page_break()

            desktop = os.path.normpath(os.path.expanduser("~/Desktop"))
            document.save('demo.docx')

            open_file('demo.docx')
            raise PreventUpdate

        else:

            return {'display': 'block'}, True


def treatment_List_Ester(df_data, df_list_ester):
    """
    FUNCTION : Ester hydrolyse

    Input :
    - df_data : data formula
    - df_list_ester : ester DataBase

    Output :
    - df_list : list of raw material with ester
    """

    #Joining data formula with ester database
    df_list = df_data.join(df_list_ester.set_index('IPC'))

    #Droping columns Name
    df_list = df_list.drop(columns=['NAME'], axis=1).dropna(axis=0)

    return df_list


def treatment_Ester_Classes(df_data, df_ing, df_ester, df_explosion_nat, edt_value):
    """
    FUNCTION : Ester hydrolyse, button : ESTER classes

    Input :
    - df_data : data formula
    - df_ing : raw materials database
    - df_ester : ester database
    - df_explosion_nat : explosion natural database
    - edt_value : % EDT value

    Output :
    - df_ester_data : dataframe result
    - indirect_ester_class1 : sum
    - indirect_ester_class2 : sum
    - indirect_ester_class3 : sum
    - direct_ester_class1 : sum
    - direct_ester_class2 : sum
    - direct_ester_class3 : sum
    - ester_I : sum
    - ester_II : sum
    - ester_III : sum
    - ester_I_II : sum
    """

    #Joining data formula with raw materials database
    df_data = df_data.join(df_ing[['IPC', 'Neat IPC', 'Name', 'Dilution (%)']].set_index('IPC'))

    #Setting df_data_final and df_data
    df_data_final = df_data[["Name", "Dilution (%)", "dosage"]]
    df_data = df_data[['dosage', 'Dilution (%)', 'Neat IPC']]

    #treatment indirect ester
    #Joining data formula with explosion natural database
    df_explosion_natural = df_data.join(df_explosion_nat.set_index('IPC'))

    #Droping NaN line
    df_explosion_natural = df_explosion_natural.dropna(axis=0)

    #Reindexing IPC
    df_explosion_natural = df_explosion_natural.rename_axis('IPC').reset_index()

    #Droping columns Name
    df_explosion_natural = df_explosion_natural.drop(columns=['Name'], axis=1)

    #Rename columns
    df_explosion_natural = df_explosion_natural.rename(
        columns={'Peak Name': 'Name', 'NAME': 'NAME MP', 'IPC': 'IPC MP'})

    #Setting characters in lowercase
    df_ester["NAME"] = df_ester["NAME"].str.lower()

    #Joining data with natural explosion with ester database
    df_indirect_ester = df_explosion_natural.set_index("Name").join(df_ester.set_index('NAME'))

    #Droping columns
    df_indirect_ester = df_indirect_ester.drop(columns=['Name', 'Type'], axis=1)

    #Droping NaN values
    df_indirect_ester = df_indirect_ester.dropna(axis=0)

    #Reindexing Name column
    df_indirect_ester = df_indirect_ester.rename_axis('Name').reset_index()

    #Setting sum of indirect ester class
    indirect_ester_class1 = df_indirect_ester.loc[df_indirect_ester['Class'] == 1]
    indirect_ester_class1 = indirect_ester_class1['Name']

    indirect_ester_class2 = df_indirect_ester.loc[df_indirect_ester['Class'] == 2]
    indirect_ester_class2 = indirect_ester_class2['Name']

    indirect_ester_class3 = df_indirect_ester.loc[df_indirect_ester['Class'] == 3]
    indirect_ester_class3 = indirect_ester_class3['Name']

    #treatment direct ester
    #Joining data formula with ester database
    df_direct_ester = df_data.rename_axis('IPC').reset_index().set_index('Neat IPC').join(df_ester.set_index('IPC'))

    #Droping useless column
    df_direct_ester = df_direct_ester.drop(columns=['Type'], axis=1)

    #Droping NaN values
    df_direct_ester = df_direct_ester.dropna(axis=0)

    #Reindexing neat IPC column
    df_direct_ester = df_direct_ester.rename_axis('Neat IPC').reset_index()

    #Setting sum of direct ester classes
    direct_ester_class1 = df_direct_ester.loc[df_direct_ester['Class'] == 1]
    direct_ester_class1 = direct_ester_class1['IPC']

    direct_ester_class2 = df_direct_ester.loc[df_direct_ester['Class'] == 2]
    direct_ester_class2 = direct_ester_class2['IPC']

    direct_ester_class3 = df_direct_ester.loc[df_direct_ester['Class'] == 3]
    direct_ester_class3 = direct_ester_class3['IPC']

    #treatment dataframe table result
    #Creating columns Quantity in EDT
    df_data_final = df_data_final.reindex(columns=[*df_data_final.columns.tolist(), *['Quantity in EDT']])

    #Reindexing IPC column
    df_data_final = df_data_final.rename_axis('IPC').reset_index()

    #Adding in dataframe the ester raw materials
    a = 0
    for i, value in enumerate(df_data_final['IPC']):
        for j, value_check in enumerate(df_indirect_ester['IPC MP']):
            if value == value_check:
                df_data_final = df_data_final.append(pd.DataFrame({"Name": df_indirect_ester.loc[j, 'Name'],
                                                                   'dosage': round(df_indirect_ester.loc[j, 'dosage'] *
                                                                                   df_indirect_ester.loc[
                                                                                       j, 'GS Area%'] / 100, 6)},
                                                                  index=[i]))

    df_data_final['Quantity in EDT'] = df_data_final[['Quantity in EDT', 'dosage']].apply(
        lambda x: round(x[1] * float(edt_value) / 100, 4), axis=1)
    df_data_final = df_data_final.reset_index()
    df_data_final['col_sort'] = np.arange(len(df_data_final))
    df_data_final = df_data_final.sort_values(by=['index', 'col_sort']).drop(['index', 'col_sort'], axis=1)
    df_data_final['dosage'] = round(df_data_final['dosage'], 4)
    df_data_final = df_data_final.drop("Dilution (%)", axis=1).rename(columns={'dosage': '% Quantity',"Quantity in EDT":'% in EDT'})

    #Seting totals sum
    df_total_indirect_ester = df_indirect_ester[['dosage', 'GS Area%', 'Class']]
    df_total_indirect_ester = df_total_indirect_ester.reindex(
        columns=[*df_total_indirect_ester.columns.tolist(), *['new dosage']])
    # df_total_indirect_aldehyde['new dosage']=df_total_indirect_aldehyde[['new dosage','dosage','GS Area%','ALDEHYDE CLASS']].apply(lambda x: round(x[1]*x[2]/100 if float(x[2])!=3 else 0,3),axis=1)
    df_total_indirect_ester['new dosage'] = df_total_indirect_ester[['new dosage', 'dosage', 'GS Area%']].apply(
        lambda x: round(x[1] * x[2] / 100, 3), axis=1)

    #Calculating totals sum
    ester_I = round(df_total_indirect_ester[df_total_indirect_ester['Class'] == 1]['new dosage'].sum() +
                    df_direct_ester[df_direct_ester['Class'] == 1]['dosage'].sum(), 4)
    ester_II = round(df_total_indirect_ester[df_total_indirect_ester['Class'] == 2]['new dosage'].sum() +
                     df_direct_ester[df_direct_ester['Class'] == 2]['dosage'].sum(), 4)
    ester_III = round(df_total_indirect_ester[df_total_indirect_ester['Class'] == 3]['new dosage'].sum() +
                      df_direct_ester[df_direct_ester['Class'] == 3]['dosage'].sum(), 4)
    ester_I_II = round(df_total_indirect_ester[df_total_indirect_ester['Class'] != 3]['new dosage'].sum() +
                       df_direct_ester[df_direct_ester['Class'] != 3]['dosage'].sum(), 4)

    return df_data_final, indirect_ester_class1, indirect_ester_class2, indirect_ester_class3, direct_ester_class1, direct_ester_class2, direct_ester_class3, ester_I, ester_II, ester_III, ester_I_II


def treatment_ester_calculate(df_data, df_ing, df_ester_half_life, list_ester, edt_value, temp_value, time_value,
                              ph_value):
    """
    FUNCTION : Ester hydrolyse, button calculate

    Input :
    - df_data : data formula
    - df_ing :  raw materials database
    - df_ester_half_life : ester half life database
    - list_ester : ester component
    - edt_value : EDT value
    - temp_value : temperature value
    - time_value : time value
    - ph_value : pH value

    Output :
    - residual : float
    - deteriorate : float
    """

    #Joining data formula with raw materials database
    df_data = df_data.join(df_ing[['IPC', 'Name']].set_index('IPC'))

    #Setting ester dataframe
    ester = pd.DataFrame(list_ester, columns=['Name'], index=['a'])

    #Setting temp dataframe
    temp = pd.DataFrame(temp_value, columns=['Temperature (°C)'], index=['a'])

    #Joining ester dataframe with data formula
    ester = ester.set_index('Name').join(df_data.rename_axis('IPC').reset_index().set_index('Name'))

    #Joining ester dataframe with ester half life dataframe
    ester = ester.set_index('IPC').join(df_ester_half_life.set_index('IPC'))

    #Joining temp dataframe with ester dataframe by temperature
    ester = temp.set_index('Temperature (°C)').join(ester.set_index('Temperature (°C)'))

    #reindeing temperature column
    ester = ester.rename_axis('Temperature (°C)').reset_index()

    #Calculating t_half
    t_half = ester.loc[:, "pH " + str(ph_value)]
    if t_half.iloc[0]==">365":
        t_half=10000
    #Calculating residual ester and deteriorate ester
    residual_ester = (ester["dosage"] * (ester["Dilution"] / 100) * (float(edt_value) / 100)) * math.exp(
        -math.log(2) / t_half * float(time_value))
    deteriorate_ester = (ester["dosage"] * (ester["Dilution"] / 100) * (float(edt_value) / 100)) - residual_ester
    return residual_ester, deteriorate_ester
